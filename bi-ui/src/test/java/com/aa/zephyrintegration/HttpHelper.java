package com.aa.zephyrintegration;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.aa.automation.BaseClass;
import com.aa.automation.SqlConnection;
import com.sun.jersey.core.util.Base64;


@SuppressWarnings("deprecation")
public class HttpHelper {
	
	int statusCode = 0;
	String statusResponse = null;
	String jiraurl = null;
	
	public HttpResult getJiraId(String uri) throws Exception
	{
		String token = new String(Base64.encode(BaseClass.GetPropertyName("passWord")));
		jiraurl = BaseClass.GetPropertyName("Jiraloginurl");
		String baseuri = jiraurl + "/rest/api/2/project/"+BaseClass.GetPropertyName("ProjectName");
		if(uri!=null)
		{
			baseuri+="/"+uri+"";
		}
		@SuppressWarnings({ "resource" })
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet request= new HttpGet(baseuri);
		request.addHeader("Authorization", "Basic " + token);
		HttpResponse response = httpClient.execute(request);
		statusCode = response.getStatusLine().getStatusCode();
		statusResponse = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
		return new HttpResult(statusCode , statusResponse);
	}

	public HttpResult getZephyrId(String versionId, String projectId,String jwtToken, String uri) throws IOException
	{
		String baseuri = BaseClass.GetPropertyName("JIRA_URL")+"/rest/api/1.0/"+uri;

		@SuppressWarnings({ "resource" })
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet request = new HttpGet(baseuri);
		request.addHeader("content-type", "text/plain");
		request.addHeader("Authorization", jwtToken);
		request.addHeader("zapiAccessKey", BaseClass.GetPropertyName("accessKey"));
		HttpResponse response = httpClient.execute(request);
		statusCode = response.getStatusLine().getStatusCode();
		statusResponse = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
		return new HttpResult(statusCode , statusResponse);
	}

	public HttpResult getZephyrIdForExecutionId(String versionId, String projectId,String jwtToken, String uri, int offset, int size) throws IOException
	{
		String baseuri = BaseClass.GetPropertyName("JIRA_URL")+"/rest/api/1.0/"+uri+"&offset=" + offset +"&size=" + size;
		@SuppressWarnings({ "resource" })
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet request = new HttpGet(baseuri);
		request.addHeader("content-type", "text/plain");
		request.addHeader("Authorization", jwtToken);
		request.addHeader("zapiAccessKey", BaseClass.GetPropertyName("accessKey"));
		HttpResponse response = httpClient.execute(request);
		statusCode = response.getStatusLine().getStatusCode();
		statusResponse = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
		return new HttpResult(statusCode , statusResponse);
	}

	public HttpResult getZephyrIdForTestCaseId(String versionId, String projectId,String jwtToken, String uri, int offset, int size) throws IOException
	{
		String baseuri = BaseClass.GetPropertyName("JIRA_URL")+"/rest/api/1.0/"+uri+"&offset=" + offset +"&size=" + size;
		@SuppressWarnings({ "resource" })
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet request = new HttpGet(baseuri);
		request.addHeader("content-type", "text/plain");
		request.addHeader("Authorization", jwtToken);
		request.addHeader("zapiAccessKey", BaseClass.GetPropertyName("accessKey"));
		HttpResponse response = httpClient.execute(request);
		statusCode = response.getStatusLine().getStatusCode();
		statusResponse = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
		return new HttpResult(statusCode , statusResponse);
	}


	public HttpResult updateZephyrStatus(String uri, String payload, String jwtToken) throws Exception
	{
		String baseuri = BaseClass.GetPropertyName("JIRA_URL")+"/rest/api/1.0/"+uri;
		@SuppressWarnings("resource")
		HttpClient httpClient = new DefaultHttpClient();
		HttpPut request = new HttpPut(baseuri);
		StringEntity params = new StringEntity(payload,"UTF-8");
		params.setContentType("application/json");
		request.addHeader("content-type", "application/json");
		request.addHeader("Authorization", jwtToken);
		request.addHeader("zapiAccessKey", BaseClass.GetPropertyName("accessKey"));
		request.setEntity(params);
		HttpResponse response = httpClient.execute(request);
		statusCode = response.getStatusLine().getStatusCode();
		return new HttpResult(statusCode , null);
	}

	public HttpResult getBotInsightProductDataJson(String taskname) throws Exception
	{
		try{
			String token = new String(Base64.encode(BaseClass.GetPropertyName("controlRoomAdminCredentials")));
			String URL = BaseClass.GetPropertyName("URL");
			String baseuri = URL + "analytics/data/api/gettaskvariableprofile/"+taskname+"";

			@SuppressWarnings({ "resource" })
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet request= new HttpGet(baseuri);
			request.addHeader("Authorization", "Basic " + token);
			HttpResponse response = httpClient.execute(request);
			statusCode = response.getStatusLine().getStatusCode();
			statusResponse = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
		}
		catch(Exception e)
		{
			System.out.println("getBotInsightProductDataJson() has error :" + e.getMessage());
		}
		return new HttpResult(statusCode , statusResponse);
	}

	public void deleteStandardDashboard(String taskname) throws Exception
	{
		//Add IsStandard = y
		String zoomSourcequery = "select zoomdatasourceid from [BotInsight].[dwa].[dashboard] where isstandard='Y' and taskid";
		String zoomDashboardquery = "select zoomdatadashboardid from [BotInsight].[dwa].[dashboard] where isstandard='Y' and taskid";

		String URL = BaseClass.GetPropertyName("URL");

		SqlConnection sql = new SqlConnection();
		String selectQuery[]={zoomSourcequery,zoomDashboardquery};
		String zoomDataSourceId = sql.getZoomDataIdsForStandardDashboard(taskname,selectQuery[0]);
		String zoomDataDashboardId = sql.getZoomDataIdsForStandardDashboard(taskname,selectQuery[1]);

		String zoomDataId[]={zoomDataDashboardId,zoomDataSourceId};
		String requestURI[]={"zoomdata/api/bookmarks/","zoomdata/api/sources/"};

		for (int i=0;i<selectQuery.length;i++)
		{
			String zoomDataURI = URL + requestURI[i] + zoomDataId[i];
			String token = new String(Base64.encode(BaseClass.GetPropertyName("zoomDevCredentials")));
			@SuppressWarnings({ "resource" })
			HttpClient httpClient = new DefaultHttpClient();
			HttpDelete request = new HttpDelete(zoomDataURI);
			request.addHeader("Authorization", "Basic " + token);
			HttpResponse response = httpClient.execute(request);

			//	String statusResponse = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
			int statusCode = response.getStatusLine().getStatusCode();

			try
			{
				if (statusCode >= 200 && statusCode < 300) {
					sql.deleteDashboardDetails(taskname);
				}
				else
				{
					//System.out.println("deleteStandardDashboard from Zoom API call StatusCode" + statusResponse +" "+statusCode);
					System.out.println("deleteStandardDashboard from Zoom API call StatusCode" + statusCode);
				}
			}
			catch(Exception e)
			{
				System.out.println("deleteStandardDashboard from Zoom Error" + e.getMessage());
			}
		}
	}

	public void deleteSavedAsAndPublishedDashboard(String dashboardname, String zoomCredentials) throws Exception
	{
		//IsStandard = No, env = dev or Prod
		String zoomSourcequery = "select zoomdatasourceid from [BotInsight].[dwa].[dashboard] where isstandard='N' and dashboardname";
		String zoomDashboardquery = "select zoomdatadashboardid from [BotInsight].[dwa].[dashboard] where isstandard='N' and dashboardname";

		String URL = BaseClass.GetPropertyName("URL");

		SqlConnection sql = new SqlConnection();
		String selectQuery[]={zoomSourcequery,zoomDashboardquery};
		String zoomDataSourceId = sql.getZoomDataIdsForSavedDashboard(dashboardname,selectQuery[0]);
		String zoomDataDashboardId = sql.getZoomDataIdsForSavedDashboard(dashboardname,selectQuery[1]);

		String zoomDataId[]={zoomDataDashboardId,zoomDataSourceId};
		String requestURI[]={"zoomdata/api/bookmarks/","zoomdata/api/sources/"};

		for (int i=0;i<selectQuery.length;i++)
		{
			String zoomDataURI = URL + requestURI[i] + zoomDataId[i];
			String token = new String(Base64.encode(zoomCredentials));
			@SuppressWarnings({ "resource" })
			HttpClient httpClient = new DefaultHttpClient();
			HttpDelete request = new HttpDelete(zoomDataURI);
			request.addHeader("Authorization", "Basic " + token);
			HttpResponse response = httpClient.execute(request);
			//String statusResponse = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
			statusCode = response.getStatusLine().getStatusCode();
			try
			{
				if (statusCode >= 200 && statusCode < 300) {
					sql.deleteSavedDashboardFromSQL(zoomDataSourceId);
				}
				else
				{
					System.out.println("deleteSavedAndPublishedDashboard from Zoom API call StatusCode" + statusCode);
				}
			}
			catch(Exception e)
			{
				System.out.println("deleteSavedAndPublishedDashboard from Zoom Error" +e.getMessage());
			}
		}
	}

	public static void main(String args[]) throws Exception
	{
		HttpHelper test = new HttpHelper();
		//String arr[] = {"17022017","AN7_1","Blank_Variables","ExitLoop_at_Counter10","OnlyNumericDataType","OnlyStringDataType","ReadFromText_with_Header","ReadFromText_without_Header","TelecomOrderEntry","ParentTask","ChildTask"};
		String arr[]={"17022017"};
		for (int i=0; i<arr.length; i++)
		{
			test.deleteStandardDashboard(arr[i]);
			test.deleteSavedAsAndPublishedDashboard(arr[i], BaseClass.GetPropertyName("zoomDevCredentials"));
		}
		//test.deleteSavedAsAndPublishedDashboard(Baseclass.GetPropertyname("Dashboard"),Baseclass.GetPropertyname("zoomDevCredentials"));
		//test.deleteSavedAsAndPublishedDashboard(Baseclass.GetPropertyname("Publisheddb"),Baseclass.GetPropertyname("zoomProdCredentials"));
	}


}

