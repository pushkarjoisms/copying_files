package com.aa.common;

public enum EndPoints {
  AUTHENTICATION  ("/v1/authentication"),
  USERMANAGEMENT  ("/v1/usermanagement");

  private final String endpoint;

  EndPoints(String endpoint) {
    this.endpoint = endpoint;
  }

  public String getEndpoint() {
    return this.endpoint;
  }
}
