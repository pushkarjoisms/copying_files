package com.thed.zephyr;

import javax.swing.JOptionPane;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class ZAPI {

		
	private final String JIRA_URL;

    private final String AUTH_TOKEN;

    public static enum REQUEST_METHOD {
        GET,
        PUT,
        POST
    }

    /**
     * @param jiraUrl - URL of your jira instance
     * @param authToken - Base64Encripted string
     */
    
    public static void main(String[] args)
    {
    	ZAPI zapi = new ZAPI("","");
    	zapi.sendPut(null, "");
    }
    
    public ZAPI(String jiraUrl, String authToken) {
        this.JIRA_URL = "https://prod-play.zephyr4jiracloud.com/connect/public/rest/api/1.0/execution/0001487585888971-242ac112-0001?projectId=11000&issueId=19980";
        this.AUTH_TOKEN = "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwYWxhay5zaGFybWEiLCJxc2giOiJlNGVjZGIzNTRhM2MwY2RkODg1OTAyMTE2M2VmMWZjYWFlMjM2MGNjNjBjMDNmNzEyOTg1MTdhZDVhNjBhMGZiIiwiaXNzIjoiYW1seVlUb3lNV1ZqTlRRd1pDMW1aVGN6TFRRd09USXRZamd4TWkwNE5qaGtaalkyTjJGbVpHSWdjR0ZzWVdzdWMyaGhjbTFoSUZWVFJWSmZSRVZHUVZWTVZGOU9RVTFGIiwiZXhwIjoxNDg3ODc3MDYzLCJpYXQiOjE0ODc3NDEwNjN9.lNjaHucbcSSLSK9pGWK3SxAD8jBurO1kY1VZ512r4SM";
    }

    /**
     * Send the requests against the provided URL using the request method provided.
     * @param requestMethod {@link REQUEST_METHOD}
     * @param endPoint - endpoint
     * @param jsonRequestBody - payload
     * @return {@link String}
     */
    @SuppressWarnings("deprecation")
    public void sendPut(String data, String url) {
        int responseCode = -1;
        String payload = "{\"status\":{\"id\":1},\"id\":\"0001487585888971-242ac112-0001\",\"projectId\":11000,\"issueId\":19980,\"cycleId\":-1,\"versionId\":-1}";
		HttpClient httpClient = new DefaultHttpClient();
        try {
        	StringBuilder responseSB = new StringBuilder();
        	
            HttpPut request = new HttpPut(JIRA_URL);
            StringEntity params =new StringEntity(payload,"UTF-8");
            params.setContentType("application/json");
            //params.setContentType(contentType);
            request.addHeader("content-type", "application/json");
            request.addHeader("Authorization", AUTH_TOKEN);
            request.addHeader("zapiAccessKey", "amlyYToyMWVjNTQwZC1mZTczLTQwOTItYjgxMi04NjhkZjY2N2FmZGIgcGFsYWsuc2hhcm1hIFVTRVJfREVGQVVMVF9OQU1F");
            request.setEntity(params);
            //JOptionPane.showMessageDialog(null, payload);
            HttpResponse response = httpClient.execute(request);
            responseCode = response.getStatusLine().getStatusCode();
            //JOptionPane.showMessageDialog(null, responseCode);
            String theString = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
             JOptionPane.showMessageDialog(null, theString);
             System.out.println(theString);
        }
        catch (Exception ex) {
        	System.out.println("data:" + payload);
        	JOptionPane.showMessageDialog(null, ex);
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
    }
}
