package com.aa.zephyrintegration;

import java.io.IOException;

import org.apache.http.ParseException;
import org.json.JSONArray; 
import org.json.JSONObject;

public class ExecutionId {
	
	int totalCount;
	public String get(long issueId, String cycleId,String projectId, String versionId) throws Exception
	{
		HttpResult response;
		String result ="";
		int offset = 1;
		int size = 50;
		do{
		response = jwtToken(cycleId, projectId, versionId, offset, size);
		result = getExecutionId(issueId, response); 
		if( result != null & result.trim()!="" | offset > totalCount)
		{
			break;
			
		}
		offset = offset + 49;
	}while( result == null | result.trim()=="");

	return result;
}

	private String getExecutionId(long issueId, HttpResult response)throws IOException {
		if (response.getStatusCode() >= 200 && response.getStatusCode() < 300) {
			try { 
			} catch (ParseException e) { 
				e.printStackTrace(); 
			} 
			JSONObject allIssues = new JSONObject(response.getResponse()); 
			totalCount = allIssues.getInt("totalCount");
			JSONArray IssuesArray = allIssues.getJSONArray("searchObjectList");
			
			//String strObj = IssuesArray.toString();
			//LogTextToFile l1 = new LogTextToFile();
			//l1.test("D:\\nagesh1.txt", strObj);
			
			if (IssuesArray.length() == 0) { 
				return ""; 
			} 
			for (int j = 0; j <= IssuesArray.length() - 1; j++) { 
				JSONObject jobj = IssuesArray.getJSONObject(j); 
				JSONObject jobj2 = jobj.getJSONObject("execution"); 
				String executionId = jobj2.getString("id"); 
				long IssueId1 = jobj2.getLong("issueId"); 

				if(IssueId1==issueId)
				{
					System.out.println("ExecutionID = "+executionId+"\n");
					return executionId;
				}
			} 
		}
		return "";
	}

	private HttpResult jwtToken(String cycleId, String projectId,String versionId,int offset, int size) throws Exception, IOException {
		String jwtToken = new JwtTokenGenerator().generateTokenToGetExecutionId(projectId, versionId, cycleId, offset, size);
		HttpHelper request = new HttpHelper();
		String uri = "executions/search/cycle/"+cycleId+"?versionId="+versionId+"&projectId="+projectId+"";
		HttpResult response = request.getZephyrIdForExecutionId(versionId, projectId, jwtToken, uri, offset, size);
		return response;
	}
}