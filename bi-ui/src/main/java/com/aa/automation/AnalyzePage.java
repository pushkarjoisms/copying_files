package com.aa.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AnalyzePage {

	WebDriver driver;
	WebDriverWait wait;
	BaseClass baseClass;
	WebElement element;	

	By Analyze = By.xpath("//div[@class='analyze']");
	By configure = By.xpath("//*[@class='configure']");
	By dashboard = By.xpath("//li[text()='Dashboard']");
	By SearchDashoard = By.id("search");
	By searchIcon = By.xpath("//div[@class='searchBtn searchIcon']");
	By Delete = By.xpath("(//img[contains(@src, 'assets/images/icons/delete.png')])[1]");
	By DeleteDashboard = By.xpath("//span[text()='Okay']");
	By NoPublishedDashboard = By.xpath("//*[@id='search']/following::ul[1]");
	By compareLink = By.xpath("//img[@src='assets/images/icons/compare.png']");
	By secondaryDashboardSearchBox = By.xpath("//search-input[@dashboard-name='vm.dashboardNameSecondary']//input[@id='search']");
	By seacondaryDashboardloadig = By.xpath("//div[@class='dashboardContainer'][2]/dashboard[1]");
	By searchTask = By.id("search");
	By publishLink = By.xpath("//img[contains(@src, 'assets/images/icons/publish.png')]");
	By enterPublishDashboardName = By.xpath("//input[@placeholder='Please name your dashboard']");
	
	By expandWidgetButton = By.xpath("//*[contains(@id, 'cid-c')]/div[3]/div[1]/div[2]/span[4]/i");
	By minimizeWidget = By.xpath("//*[@name='fullscreen_min']");
	By expandWidgetButton1 = By.xpath("//*[contains(text(),'the total Discount')]//following::div[@class='header-control full-screen-button']");
	By minimizeWidgetButton = By.xpath("//*[contains(text(),'the total Discount')]//following::div[@class='exitFullscreen']/i");
	
	By okay = By.xpath("//*[text()='Okay']");
	By defaultUItext= By.xpath("//span[text()='Please search your dashboard to start!']");
	By Usericon = By.xpath("//img[contains(@src, 'assets/images/user.png')]");
	By bookmarkLink = By.xpath("//img[contains(@src, 'assets/images/icons/bookmark.png')]");
	By BotInsightTag = By.xpath("//*[text()='Bot Insight']");
	By shareToEmailLink = By.xpath("//img[@src='assets/images/icons/share.png']");
	By toolbarBoomark = By.xpath("//img[contains(@src, 'assets/images/toolbar_bookmark.png')]");
	By expanWidgetOnDashboard = By.xpath("//img[src=''assets/images/expand.png']");
	By iFrame = By.xpath("//iframe[@id='dashboardContent']");

	public AnalyzePage(WebDriver driver, WebDriverWait wait) throws Exception
	{
		this.driver = driver;
		wait = new WebDriverWait(driver,60);
		this.wait = wait;
		baseClass = new BaseClass(driver,wait);		
	}

	public void clickOnAnalyze()throws Exception
	{
		element = driver.findElement(Analyze);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}

	public Boolean checkConfigureSectionAvailability()throws Exception
	{
		boolean flag = false;
		try	{
			flag = driver.findElement(configure).isDisplayed();	
			}catch(Exception e)	{
				System.out.println("Error in checkConfigureSectionAvailability() Method: "+e.getStackTrace());
			return flag;
		}
		return flag;
	}
	
	public Boolean checkAnalyzeSectionAvailability()throws Exception
	{
		boolean flag = false;
		try	{
			flag = driver.findElement(Analyze).isDisplayed();	
			}catch(Exception e)	{
				System.out.println("Error in checkAnalyzeSectionAvailability() Method: "+e.getStackTrace());
			return flag;
		}
		return flag;
	}

	public void clickOnConfigure()
	{
		element = driver.findElement(configure);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}

	public void selectDashboard()
	{
		element = driver.findElement(dashboard);
		baseClass.clickOnWebElement(element);
	}
	
	public void publishDashboard()
	{
		driver.findElement(configure).click();
		driver.findElement(dashboard).click();
	}
	
	public void searchPublishedDashboard(String Publisheddb) throws Exception
	{	
		try{
			element = driver.findElement(SearchDashoard);
			baseClass.enterDataIntoEditbox(element, Publisheddb);
			element = driver.findElement(searchIcon);
			baseClass.clickOnWebElement(element);
			baseClass.waitForThreadToLoad(3);
			baseClass.refreshThePage();
			baseClass.waitForThreadToLoad(5);
			}catch(Exception e){
				System.out.println("Error in searchPublishedDashboard() Method: "+e.getStackTrace());
		}
	}

	public void deletePublishedDashboard(String dashboard) throws Exception
	{				
		boolean flag = false;
		try{
			baseClass.waitForThreadToLoad(2);
			flag = driver.findElement(iFrame).isDisplayed();
			System.out.println("Delete Published Dashboard: "+dashboard);
			element = driver.findElement(Delete);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			baseClass.waitForThreadToLoad(2);
			element = driver.findElement(DeleteDashboard);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			}catch(Exception e){
				System.out.println("Published Dashboard has been Deleted: "+dashboard);
		}
	}
	
	public void searchAndDeletePublishedDashboard(String PublishedDb) throws Exception
	{				
		boolean flag = false;
		try{
			element = driver.findElement(SearchDashoard);
			baseClass.enterDataIntoEditbox(element, PublishedDb);
			element = driver.findElement(searchIcon);
			baseClass.clickOnWebElement(element);
			//baseClass.waitForThreadToLoad(2);
			//baseClass.refreshThePage();
			baseClass.waitForThreadToLoad(5);
			flag = driver.findElement(iFrame).isDisplayed();
			System.out.println("Delete Published Dashboard: "+PublishedDb);
			element = driver.findElement(Delete);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			baseClass.waitForThreadToLoad(2);
			element = driver.findElement(DeleteDashboard);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			}catch(Exception e){
				System.out.println("Published Dashboard has been Deleted: "+PublishedDb);
		}
	}

	public String verifyPublishedDashboardIsDeleted(String Publisheddb) throws Exception
	{
		element = driver.findElement(NoPublishedDashboard);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}

	public void clickOnCompareButton()throws Exception
	{
		element = driver.findElement(compareLink);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}

	public String secondaryDashboardSearchBox()
	{
		element = driver.findElement(secondaryDashboardSearchBox);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTagNameFromWebElement(element);		
	}

	public void searchAndSelectDashboard(String dashboard) throws Exception
	{
		try{
			baseClass.waitForThreadToLoad(2);
			element = driver.findElement(searchTask);
			baseClass.waitForElementToBePresent(element);
			baseClass.enterDataIntoEditbox(element, dashboard);
			element = driver.findElement(searchIcon);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(iFrame);
			baseClass.waitForThreadToLoad(10);
			}catch(Exception e){
				System.out.println("Error in searchAndSelectDashboard() Method: "+e.getStackTrace());
		}
		
	}

	public void publishDashboard(String Publisheddb) throws Exception
	{
		try{
		element = driver.findElement(publishLink);
		baseClass.clickOnWebElement(element);
		element = driver.findElement(enterPublishDashboardName);
		baseClass.enterDataIntoEditbox(element, Publisheddb);
		element = driver.findElement(okay);
		baseClass.clickOnWebElement(element);
		baseClass.waitForThreadToLoad(5);
		}catch(Exception e){
			System.out.println("Error in publishDashboard() Method: "+e.getStackTrace());
		}
	}

	public boolean selectFirstWidgetInIframe(String text)throws Exception
	{
		try{
			element = driver.findElement(By.xpath("//div[contains(text(),'"+text+"')]"));
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			}catch(Exception e)	{
				System.out.println("Error in selectFirstWidgetInIframe() Method: "+e.getStackTrace());
			return false;
		}
		return true;
	}

	public void expandWidget() throws Exception
	{
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(expandWidgetButton1);
		baseClass.mouseHoverEventToElemnt(element);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}

	public String minimizeWidget() throws Exception
	{
		String result = driver.findElement(minimizeWidget).getCssValue("display");
		element = driver.findElement(minimizeWidget);
		baseClass.clickOnWebElement(element);
		return result;
	}

	public Boolean DefaultAnalyzeUiLinks()throws Exception
	{
		try	{
			driver.findElement(Usericon).getClass().getName();
			driver.findElement(bookmarkLink).getClass().getName();
			} catch(Exception e){
				System.out.println("Error in DefaultAnalyzeUiLinks() Method: "+e.getStackTrace());
			return false;
		}
		return true;
	}

	public Boolean publishedDashboardUiLinks()throws Exception
	{
		try	{
			driver.findElement(compareLink).getClass().getName();
			driver.findElement(shareToEmailLink).getClass().getName();
			driver.findElement(bookmarkLink).getClass().getName();
			driver.findElement(toolbarBoomark).getClass().getName();
			System.out.println("Captured Links from Published Dashboard");
			} catch(Exception e){
				System.out.println("Error in publishedDashboardUiLinks() Method: "+e.getStackTrace());
			return false;
		}
		return true;
	}
}
