package com.aa.common;

public class AutomationException extends Exception {
  public AutomationException(String message) {
    super(message);
  }
}
