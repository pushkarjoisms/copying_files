package com.aa.common;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class HTTPUtility {
  private final static int TIMER = 2000;

  private static void timer() {
    try {
      Thread.sleep(TIMER);
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
  
  public static Response post(String uri, String payload) {
    Response response = given()
        .body(payload)
        .when()
        .post(uri)
        .then()
        .extract()
        .response();
    return response;
  }
  public static Response post(String uri, String payload, String token) {
    Response response = given()
        .header("X-Authorization", token)
        .body(payload)
        .when()
        .post(uri)
        .then()
        .extract()
        .response();
    return response;
  }

}
