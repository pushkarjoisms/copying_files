package com.aa.automation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aa.automation.LogoutPage;
import com.aa.automation.UsersPage;

public class LogoutTest {
	
	WebDriver driver;
	WebDriverWait wait;
	LogoutPage logoutPage;
	UsersPage usersPage;

	public LogoutTest(WebDriver driver, WebDriverWait wait) throws Exception
	{
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		this.wait = wait;
		logoutPage = new  LogoutPage(driver,wait);
		usersPage = new UsersPage(driver, wait);
	}

	public void logout_FromBotInsight_VerifySuccessfulLogout() throws Exception
	{
		System.out.println("Executing Method No: 63");
		
		logoutPage.logout();
	}
	
	public void logout_FromBotInsight_VerifyLogout() throws Exception
	{
		
		logoutPage.logout();
	}
	
	public void logoutFromControlRoom(){
		
		System.out.println("Executing Method No: 62");		
		
		logoutPage.clickOnHeaderBarTrayButton();
		logoutPage.clickOnLogOutButton();
	}
}
