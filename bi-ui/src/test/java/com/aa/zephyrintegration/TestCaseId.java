package com.aa.zephyrintegration;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.apache.http.ParseException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;

import com.aa.automation.BaseClass;

public class TestCaseId {
	WebDriver driver;
	BaseClass baseClass;
	
	int totalCount;
	public long get(String cycleId, String projectId, String versionId, String methodName) throws Exception
	{
		HttpResult response;
		long result =0;
		int offset = 1;
		int size = 50;
		do{
			response = jwtToken(cycleId, projectId, versionId, methodName,offset, size);
			result = getIssueId(methodName, response);
			if( result > 0 | offset > totalCount)
			{
				break;
				
			}
			offset = offset + 49;
		}while( result <= 0);

		return result;
	}

	private long getIssueId(String methodName, HttpResult response)	throws Exception {
		if (response.getStatusCode() >= 200 && response.getStatusCode() < 300) {
			try { 
			} catch (ParseException e) { 
				e.printStackTrace(); 
			} 
			JSONObject allIssues = new JSONObject(response.getResponse()); 
			totalCount = allIssues.getInt("totalCount");
			JSONArray IssuesArray = allIssues.getJSONArray("searchObjectList");
			baseClass = new BaseClass(driver);
			baseClass.logTextToFile("C:\\try.txt", IssuesArray.toString());

			if (IssuesArray.length() == 0) { 
				return 0; 
			} 
			for (int j = 0; j <= IssuesArray.length() - 1; j++) { 
				JSONObject jobj = IssuesArray.getJSONObject(j); 
				String IssueSummary = (String) jobj.get("issueLabel");
				JSONObject executionId = jobj.getJSONObject("execution"); 
				long IssueId = executionId.getLong("issueId"); 
				if(IssueSummary.compareTo(methodName)==0)
				{
					return IssueId;
				}				
			} 
		}
		return 0;
	}

	private HttpResult jwtToken(String cycleId, String projectId, String versionId, String methodName,int offset, int size) throws Exception, IOException {
		String jwtToken = new JwtTokenGenerator().generateTokenToGetIssueId(projectId, versionId, cycleId,offset, size);
		HttpHelper request = new HttpHelper();
		String uri = "executions/search/cycle/"+cycleId+"?projectId="+projectId+"&versionId="+versionId+"";
		HttpResult response = request.getZephyrIdForTestCaseId(versionId, projectId, jwtToken, uri, offset, size);
		return response;
	}

}
