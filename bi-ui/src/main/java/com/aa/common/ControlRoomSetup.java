package com.aa.common;

import org.apache.log4j.Logger;

import java.io.*;

public class ControlRoomSetup {
  final static Logger log = Logger.getLogger(ControlRoomSetup.class);

  /**
   * This needs to be called before the test is run on a freshly installed control room
   * It will do the following:
   *  copy the license to resource/licenses directory
   *  create control-room admin user
   *  create bot insights users; biadmin, botcreator, botrunner
   */
  static final public void setup() {
    UserCreation uc = new UserCreation();

    try {
      uc.createUser(Role.CRADMIN.getRole());
      uc.createUser("bitest", Role.BotInsight.getRole(), "DEVELOPMENT");
      uc.createUser("biadmin", Role.BotInsight.getRole(), "DEVELOPMENT");
      uc.createUser("botcreator", Role.Basic.getRole(), "DEVELOPMENT");
      uc.createUser("botrunner", Role.Basic.getRole(), "RUNTIME");
    } catch (AutomationException e) {
      log.info(e.getMessage());
    }
  }
}
