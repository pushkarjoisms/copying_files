package com.aa.automation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

	WebDriver driver;
	WebDriverWait wait;
	BaseClass baseClass;
	Actions actions;
	JavascriptExecutor executor;
	WebElement element;

	// Constructor
	public HomePage(WebDriver driver, WebDriverWait wait) throws Exception {

		this.driver = driver;
		wait = new WebDriverWait(driver,60);
		this.wait = wait;
		baseClass = new BaseClass(driver,wait);
		actions = new Actions(driver);
		executor = (JavascriptExecutor) driver;
	}	

	// License Expiry days Announcement message
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'announcement-main')]")
	private WebElement bannerAnnouncementBar;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'announcement-title')]")
	private WebElement licenseExpiryDaysBannerAnnouncementMessage;

	// Close icon for Announcement message
	@FindBy(how = How.XPATH, using = "//*[contains(@class, 'icon fa fa-close')]")
	private WebElement closeAnnouncementMessageIcon;

	// User Details and logout option
	@FindBy(how = How.XPATH, using = "//*[contains(@class, 'headerbar-tray-option-button headerbar-tray-option--labeled')]")
	private WebElement loggedInUserName;

	@FindBy(how = How.XPATH, using = "//button[@name='logout']")
	private WebElement logOutBtn;

	@FindBy(how = How.XPATH, using = "//button[@name='profileEdit']")
	private WebElement editProfileLink;

	@FindBy(how = How.XPATH, using = "//button[@name='profileChangePassword']")
	private WebElement changePasswordLink;

	// Announcement Show & Hide Details
	@FindBy(how = How.XPATH, using = "//span[. = 'Show details']")
	private WebElement showAnnouncementDetails;

	@FindBy(how = How.XPATH, using = "//span[. = 'Hide details']")
	private WebElement hideAnnouncementDetails;

	@FindBy(how = How.XPATH, using = "//div[@class = 'message-title']")
	private WebElement ticketAnnouncementMessageTitle;

	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'message-main-contents')]")
	private WebElement tickerAnnouncementContentSection;

	@FindBy(how = How.XPATH, using = "//div[@class = 'licenseexpiryannouncement']")
	private WebElement announcementMessageContent;

	@FindBy(how = How.XPATH, using = "//*[contains(text(), 'Install a new license')]")
	private WebElement installNewLicenceLinkinShowDetails;

	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'breadcrumbs')]")
	private WebElement breadCrumb;

	// Dashboards
	@FindBy(how = How.XPATH, using = "//div[. = 'Dashboards']")
	private WebElement dashBoards;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Home')]")
	private WebElement dashBoard_Home;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Bots')]")
	private WebElement dashBoard_Bots;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Devices')]")
	private WebElement dashBoard_Devices;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Audit')]")
	private WebElement dashBoard_Audit;

	// Activity
	@FindBy(how = How.XPATH, using = "//div[. = 'Activity']")
	private WebElement activity;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'In progress')]")
	private WebElement activity_InProgress;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Scheduled')]//*[@class='navigation-secondaryoption-title']")
	private WebElement activity_Scheduled;
	
	@FindBy(how = How.XPATH, using = "//div[. = 'Activity']//following-sibling::span")
	private WebElement ActivityIcon;

	// Bots
	@FindBy(how = How.XPATH, using = "//span[. = 'Bots']")
	private WebElement bots;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'My bots')]")
	private WebElement bots_MyBots;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Credentials')]")
	private WebElement bots_Credentials;

	// Devices
	@FindBy(how = How.XPATH, using = "//span[. = 'Devices']")
	private WebElement devices;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Bot runners')]")
	private WebElement devices_BotRunners;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Bot farm')]")
	private WebElement devices_BotFarm;

	// WorkLoad
	@FindBy(how = How.XPATH, using = "//span[. = 'Workload']")
	private WebElement workLoad;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Queues')]")
	private WebElement workLoad_Queues;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Work orders')]v")
	private WebElement workLoad_WorkOrders;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'SLA calculator')]")
	private WebElement workLoad_SLACalculator;

	// AuditLog
	@FindBy(how = How.XPATH, using = "//div[. = 'Audit log']")
	private WebElement auditLog;

	// Administration
	@FindBy(how = How.XPATH, using = "//div[. = 'Administration']")
	private WebElement administration;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Settings')]")
	private WebElement administration_Settings;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Users')]")
	private WebElement administration_Users;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Roles')]")
	private WebElement administration_Roles;

	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Licenses')]")
	private WebElement administration_Licenses;

	// Loggedin User status
	public boolean loggedInUserStatus() {

		boolean userStatus = false;

		try {
			if (loggedInUserName.isDisplayed()) {
				userStatus = true;
			}
		} catch (Exception e) {
			userStatus = false;
		}
		return userStatus;
	}

	// Access manage user section
	public void clickUserNameHeader() {
		loggedInUserName.click();
	}

	public void userLogOut() {

		baseClass.waitForElementToBePresent(logOutBtn);
		logOutBtn.click();
	}

	// Announcement Message for License Expiry days
	public void closeExpiryDaysAnnouncementMessage() {
		closeAnnouncementMessageIcon.click();
	}

	public String announcementBannerText() {

		String licenseExpiryAnnouncementMessageText = licenseExpiryDaysBannerAnnouncementMessage.getText();

		return licenseExpiryAnnouncementMessageText;
	}

	public boolean isAnnouncementBannerDisplayed() {

		boolean announcementBannerDisplayed = false;

		try {
			if (licenseExpiryDaysBannerAnnouncementMessage.isDisplayed()) {
				announcementBannerDisplayed = true;
			}
		} catch (Exception e) {
			announcementBannerDisplayed = false;
		}
		return announcementBannerDisplayed;
	}

	public boolean isLicenseExpiryDaysDisplayedInBannerLessThanThirtyDays() {

		boolean licenseExpiryDaysInBannerIsLessThanThrityDays;

		String licenseExpiryBannerMessage = licenseExpiryDaysBannerAnnouncementMessage.getText();
		System.out.println(licenseExpiryBannerMessage);
		String daysRemainingBanner = licenseExpiryBannerMessage.replaceAll("[^0-9]", "");
		System.out.println("Banner Days Remaining: " + daysRemainingBanner);

		int numberOfDaysRemainingForLicenseToExpireBanner = Integer.parseInt(daysRemainingBanner.trim());

		if (numberOfDaysRemainingForLicenseToExpireBanner <= 30) {
			licenseExpiryDaysInBannerIsLessThanThrityDays = true;
		} else {
			licenseExpiryDaysInBannerIsLessThanThrityDays = false;
		}
		return licenseExpiryDaysInBannerIsLessThanThrityDays;
	}

	public int getBannerLicenseExpiryDaysRemaining() {

		String getLicenseExpiryBannerMessageText = licenseExpiryDaysBannerAnnouncementMessage.getText();
		String getDaysRemainingBanner = getLicenseExpiryBannerMessageText.replaceAll("[^0-9]", "");
		int licenseExpiryDaysRemainingInBanner = Integer.parseInt(getDaysRemainingBanner);
		return licenseExpiryDaysRemainingInBanner;

	}

	// Edit Profile & Change Passowrd
	public void editUserProfile() {
		editProfileLink.click();
	}

	public void changeUserPassword() {
		changePasswordLink.click();
	}

	// Click show and hide detail

	public void clickShowAnnouncementDetails() throws InterruptedException {

		showAnnouncementDetails.click();
		driver.manage().timeouts().implicitlyWait(1500, TimeUnit.MILLISECONDS);
	}

	public boolean isLicenseExpiryDaysDisplayedInAnnouncementLessThanThirtyDays() {

		boolean licenseExpiryDaysInAnnouncementMessageIsLessThanThrityDays;

		baseClass.waitForElementToBePresent(ticketAnnouncementMessageTitle);

		String licenseExpiryAnnouncementMessage = ticketAnnouncementMessageTitle.getAttribute("textContent");
		System.out.println(licenseExpiryAnnouncementMessage);
		String daysRemainingAnnouncement = licenseExpiryAnnouncementMessage.replaceAll("[^0-9]", "");
		System.out.println("Announcement Days Remaining: " + daysRemainingAnnouncement);

		int numberOfDaysRemainingForLicenseToExpireAnnouncement = Integer.parseInt(daysRemainingAnnouncement.trim());

		if (numberOfDaysRemainingForLicenseToExpireAnnouncement <= 30) {

			licenseExpiryDaysInAnnouncementMessageIsLessThanThrityDays = true;

		} else {

			licenseExpiryDaysInAnnouncementMessageIsLessThanThrityDays = false;
		}

		return licenseExpiryDaysInAnnouncementMessageIsLessThanThrityDays;

	}

	public int getAnnouncementLicenseExpiryDaysRemaining() {

		String getLicenseExpiryAnnouncementMessageText = ticketAnnouncementMessageTitle.getAttribute("textContent");
		String getDaysRemainingAnnouncement = getLicenseExpiryAnnouncementMessageText.replaceAll("[^0-9]", "");
		int licenseExpiryDaysRemainingInAnnouncement = Integer.parseInt(getDaysRemainingAnnouncement);
		return licenseExpiryDaysRemainingInAnnouncement;

	}

	public void clickHideAnnouncementDetails() throws InterruptedException {

		hideAnnouncementDetails.click();
		driver.manage().timeouts().implicitlyWait(1500, TimeUnit.MILLISECONDS);
	}

	public boolean announcmentMessageIsDisplayed() {

		boolean isAnnouncementmessageDisplayed;

		if (ticketAnnouncementMessageTitle.isDisplayed()) {
			isAnnouncementmessageDisplayed = true;
		} else {
			isAnnouncementmessageDisplayed = false;
		}
		return isAnnouncementmessageDisplayed;
	}

	public boolean getAnnouncementDetailText() {

		boolean announcementDetailtextIsDisplayed;
		driver.manage().timeouts().implicitlyWait(1500, TimeUnit.MILLISECONDS);
		String announcementDetailText = announcementMessageContent.getText();
		String expectedAnnouncementDetailText = "At that time, users will no longer be able to log on. Install a new license under Administration > Licenses.\nIf you do not have a license, please contact your System administrator or Automation Anywhere Sales.";
		System.out.println(announcementDetailText);

		if (announcementDetailText.equalsIgnoreCase(expectedAnnouncementDetailText)) {
			announcementDetailtextIsDisplayed = true;
		} else {
			announcementDetailtextIsDisplayed = false;
		}
		return announcementDetailtextIsDisplayed;
	}

	

	public void clickInstallANewLicenseLinkUnderShowDetails() throws InterruptedException {

		baseClass.waitForElementToBePresent(installNewLicenceLinkinShowDetails);
		executor.executeScript("arguments[0].scrollIntoView();", installNewLicenceLinkinShowDetails);
		installNewLicenceLinkinShowDetails.click();
	}

	public boolean isAnnouncementBannerBackgroundColourDisplayedCorrectly() {

		boolean corretBannerBackgroundColourIsDisplayed;
		String rbgBannerBackgroundColour = bannerAnnouncementBar.getCssValue("background-color");
		String hexBannerBackgroundColour = Color.fromString(rbgBannerBackgroundColour).asHex();

		if (hexBannerBackgroundColour.equals("#133a65")) {
			corretBannerBackgroundColourIsDisplayed = true;
		} else {
			corretBannerBackgroundColourIsDisplayed = false;
		}
		return corretBannerBackgroundColourIsDisplayed;
	}

	public boolean isAnnouncementBannerTextColourDisplayedCorrectly() {

		boolean correctBannerTextColourIsDisplayed;

		String rbgBannerTextColour = licenseExpiryDaysBannerAnnouncementMessage.getCssValue("color").toString();
		String hexBannerTextColour = Color.fromString(rbgBannerTextColour).asHex();

		if (hexBannerTextColour.equals("#ffffff")) {
			correctBannerTextColourIsDisplayed = true;
		} else {
			correctBannerTextColourIsDisplayed = false;
		}
		return correctBannerTextColourIsDisplayed;
	}

	public boolean isTickerAnnouncementMessageBackgroundColourDisplayedCorrectly() {

		boolean corretTickerBackgroundColourIsDisplayed;

		String rbgTicketBackgroundColour = tickerAnnouncementContentSection.getCssValue("color");
		String hexTickerBackgroundColour = Color.fromString(rbgTicketBackgroundColour).asHex();

		if (hexTickerBackgroundColour.equals("#ffffff")) {
			corretTickerBackgroundColourIsDisplayed = true;
		} else {
			corretTickerBackgroundColourIsDisplayed = false;
		}

		return corretTickerBackgroundColourIsDisplayed;
	}

	public boolean istTicketMessageTitleTextColourIsDisplayedCorrectly() {

		boolean corretTickerTitleTextColourIsDisplayed;

		String rbgTicketTitleTextColour = ticketAnnouncementMessageTitle.getCssValue("color");
		String hexTickerTitletextColour = Color.fromString(rbgTicketTitleTextColour).asHex();

		if (hexTickerTitletextColour.equals("#133a65")) {
			corretTickerTitleTextColourIsDisplayed = true;
		} else {
			corretTickerTitleTextColourIsDisplayed = false;
		}

		return corretTickerTitleTextColourIsDisplayed;
	}

	public String breadCrumText() {
		baseClass.waitForElementToBePresent(breadCrumb);
		String breadCrumbValue = breadCrumb.getText();
		return breadCrumbValue;
	}

	// Dashboards
	public void accessDashboard() {
		dashBoards.click();
	}

	public void clickDashboardHome() {
		dashBoard_Home.click();
	}

	public void clickDashboardBots() {
		dashBoards.click();
	}

	public void clickDashboardDevices() {
		dashBoard_Home.click();
	}

	public void clickDashboardAudit() {
		dashBoard_Home.click();
	}

	// Activity
	public void accessActivityTab() {
		element = driver.findElement(By.xpath("//div[. = 'Activity']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void clickOnActivity() {
		baseClass.waitForElementToBePresent(ActivityIcon);
		String activityIconStatus=ActivityIcon.getAttribute("class");
		if(!activityIconStatus.contains("open")){
			baseClass.waitForElementToBeClickable(activity);
			activity.click();
		}	
	}

	public void clickActivityInProgress() {
		activity_InProgress.click();
	}

	public void clickOnScheduledLink() throws InterruptedException {
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[contains(@title, 'Scheduled')]//*[@class='navigation-secondaryoption-title']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoViewPageElementandClick(element);
	}
	
	public void clickOnUsersLink() throws Exception {
		element = driver.findElement(By.xpath("//*[contains(@title, 'Users')]/div[1]/div"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}
	
	public void clickOnHomeLink() throws Exception {
		element = driver.findElement(By.xpath("//*[contains(@title, 'Home')]/div"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void clickOnBotsLink() throws Exception {
		element = driver.findElement(By.xpath("//*[contains(@title, 'Bots')]/div[1]/div"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}
	
	public void clickOnDevicesLink() throws Exception {
		element = driver.findElement(By.xpath("//*[contains(@title, 'Devices')]/div[1]/div"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}
	
	public void clickOnAuditLink() throws Exception {
		element = driver.findElement(By.xpath("//*[contains(@title, 'Audit')]/div[1]/div"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}

	// BOTS
	public void accessBots() {
		bots.click();
	}

		
	public void clickBotsMyBots() {
		bots_MyBots.click();
	}

	public void clickBotsCredentials() {
		driver.manage().timeouts().implicitlyWait(1500, TimeUnit.MILLISECONDS);
		executor.executeScript("arguments[0].scrollIntoView();", bots_Credentials);
		bots_Credentials.click();
		driver.manage().timeouts().implicitlyWait(2500, TimeUnit.MILLISECONDS);

	}

	// Devices
	public void accessDevices() {
		devices.click();
	}

	public void clickDevicesBotRunner() {
		devices_BotRunners.click();
	}

	public void clickDevicesBotFarm() {
		devices_BotFarm.click();
	}

	// WorkLoad
	public void accessWorkLoad() {
		workLoad.click();
	}

	public void clickWorkLoadQueues() {
		workLoad_Queues.click();
	}

	public void clickWorkLoadworkOrders() {
		workLoad_WorkOrders.click();
	}

	public void clickAuditLog() {
		baseClass.waitForElementToBeClickable(auditLog);
		auditLog.click();
	}

	public void accessAdministrationTab(){
		element = driver.findElement(By.xpath("//div[. = 'Administration']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
		 }
	
	public void accessDashboardsTab(){
		element = driver.findElement(By.xpath("//span[. = 'Dashboards']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
		 }

	public void clickAdministrationSettings() {
		executor.executeScript("arguments[0].scrollIntoView();", administration_Settings);
		administration_Settings.click();
	}

	public void clickAdministrationUsers() {
		executor.executeScript("arguments[0].scrollIntoView();", administration);
		administration_Users.click();
	}

	public void clickOnRolesLink() throws Exception {		
		
		  element = driver.findElement(By.xpath("//*[contains(@title, 'Roles')]/div[1]/div"));
		  baseClass.waitForElementToBeClickable(element);
		  baseClass.scrollIntoPageElementandClick(element);
		 }

	public void clickAdministrationLicenses() {

		driver.manage().timeouts().implicitlyWait(1500, TimeUnit.MILLISECONDS);
		executor.executeScript("arguments[0].scrollIntoView();", administration_Licenses);
		administration_Licenses.click();
		driver.manage().timeouts().implicitlyWait(2500, TimeUnit.MILLISECONDS);
	}
	

}
