package com.aa.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

public class Utility {
  final static Logger log = Logger.getLogger(Utility.class);
  private volatile static Utility instance;

  public static Utility getInstance() {
    if (instance == null) {
      synchronized(Utility.class) {
        if (instance == null) {
          instance = new Utility();
        }
      }
    }
    return instance;
  }

  /**
   * Returns absolute path of a file
   * @param filename - name of the file
   * @return absolute path of a file
   */
  public String getFileLocation(String filename) {
    ClassLoader classLoader = getClass().getClassLoader();
    return classLoader.getResource(filename).getFile();
  }

  /**
   * Unmarshall the json based on POJO
   * @param clz - Pojo class
   * @param filename - json filename
   * @param <T> - class Pojo
   * @return Json object
   */
  public <T> T getMapper(Class<T> clz, String filename) {
    T value = null;
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

    try {
      value = mapper.readValue(new FileInputStream(new File(filename)), clz);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return value;
  }


  public String jsonObjectToString (Object clz) {
    ObjectMapper mapper = new ObjectMapper() ;
    try {
      return mapper.writeValueAsString(clz);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Copies the taskbots (src/test/resources/Taskbots to
   *   ../Automation Anywhere/My Tasks/Resources/Tasks
   */
  public void copyTaskBots() {
    String destpath = System.getProperty("user.home") + "\\Documents\\Automation Anywhere Files"
        + "\\Automation Anywhere\\My Tasks\\resources\\Tasks";

    File destFile = new File(destpath);
    if (!destFile.exists()) {
      destFile.mkdirs();
      log.info("Copy bot directory created") ;
      File srcFile = new File("src/test/resources/Taskbots");
      log.info(srcFile.getAbsoluteFile());

      try  {
        FileUtils.copyDirectory(srcFile, destFile);
      } catch (IOException io) {
        io.printStackTrace();
      }

    }
  }

  /**
   * If REMOTE env variable is set to true, it will read the biqa.properties file
   *   to get the Control Room URL; otherwise, it will read the registry
   * @return url
   */
  public String getHost() {
    String propertyHost = System.getenv("REMOTE");
    String url = null;

    if (propertyHost != null) {
      Properties properties = new Properties();
      properties = new Properties();
      InputStream inputStream = Utility.class.getClassLoader().getResourceAsStream("biqa.properties");
      try {
        properties.load(inputStream);
        url = properties.getProperty("URL");
      } catch (IOException e) {
        log.info("Unable to Load property from Properties file: " + e.getStackTrace());
      }
    } else {
      url = WindowsReqistry.readRegistry
        ("HKEY_LOCAL_MACHINE\\SYSTEM\\ControlSet001\\Control\\Session Manager\\Environment","AACRURL");
    }
    return url;
  }

  public Boolean isUserCreated() {
    final String USERNAME = "superadmin";
    final String PASSWORD = "W0nderland";
    Utility utility = Utility.getInstance();

    log.info("Checking if control-room user is created: " + USERNAME + ":" + PASSWORD);
    String authFilePath = utility.getFileLocation("json/users/authentication.json");
    Authentication auth = utility.getMapper(Authentication.class, authFilePath);
    auth.setUsername(USERNAME);
    auth.setPassword(PASSWORD);

    String payload = utility.jsonObjectToString(auth);
    String authURI = Utility.getInstance().getHost() + EndPoints.AUTHENTICATION.getEndpoint();
    Response response = HTTPUtility.post(authURI, payload);
    if (response.statusCode() ==  200) {
      return true;
    }
    return false;
  }

  /**
   * Description: This method is used for running command into windows command line.
   * @param cmd which contains exe path followed by command parameters
   */
  public void runCommand(String cmd) {
    Process proc = null;
    try {
      proc = Runtime.getRuntime().exec(cmd);
      proc.waitFor();
    } catch (Exception  e) {
      e.printStackTrace();
    }
  }

  public void closeChromeBrowser() {
    String CHROME_KILL = "taskkill /f /IM chrome.exe";
    this.runCommand(CHROME_KILL);
    log.info("Chrome browser is terminated");
  }

  public static void main(String[] args) {
    Utility util = Utility.getInstance();
    util.copyTaskBots();
  }
}
