package com.aa.automation;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aa.automation.BaseClass;
import com.aa.automation.DashboardPage;
import com.aa.automation.HomePage;
import com.aa.automation.OperationAnalyticsPage;

public class OperationAnalyticsTest {

	WebDriver driver;
	WebDriverWait wait;
	BaseClass baseClass;
	LoginTest loginTest;
	LogoutTest logoutTest;
	HomePage homePage;
	DashboardPage dashboardPage;
	OperationAnalyticsPage operationAnalyticsPage;


	public OperationAnalyticsTest(WebDriver driver,WebDriverWait wait) throws Exception
	{
		this.driver= driver;
		wait = new WebDriverWait(driver, 60);
		this.wait = wait;
		operationAnalyticsPage = new  OperationAnalyticsPage(driver,wait);
		loginTest = new LoginTest(driver, wait);
		logoutTest = new LogoutTest(driver, wait);
		baseClass = new BaseClass(driver, wait);
		homePage = new HomePage(driver, wait);
		dashboardPage = new DashboardPage(driver, wait);

	}

	public void OperationAnalytics_Dashboardsloading_Successfully() throws Exception {
		
		System.out.println("Executing Method No: 62");			
		
		baseClass.waitForThreadToLoad(2);
		logoutTest.logout_FromBotInsight_VerifyLogout();
		baseClass.waitForThreadToLoad(2);
		loginTest.loginToControlRoom_AdminCredentials();		
		homePage.accessDashboardsTab();
		
		//Home Dashboard		
		homePage.clickOnHomeLink();
		baseClass.waitForThreadToLoad(2);
		operationAnalyticsPage.SwitchToParentFrame();		
		String homePageTitle = operationAnalyticsPage.dashboardTitleText();		
		Assert.assertEquals(homePageTitle, "Home Dashboard");
		
		String[] widgetTexts1 = {"Home Users","Bot Run Status","Total Bot Schedules","Total Queues","Bot Velocity"};
		for(int i=0; i<widgetTexts1.length; i++){			
			operationAnalyticsPage.clickToFullScreen(widgetTexts1[i]);		
			Boolean leftPanel1 = operationAnalyticsPage.checkLeftPanelControls(widgetTexts1[i]);
			Assert.assertTrue(leftPanel1);
			/*String widgetLabelText1 = operationAnalyticsPage.getWidgetPrimaryLabelText(widgetTexts1[i]);
			Assert.assertNotNull(widgetLabelText1);
			String widgetLabelNumber1 = operationAnalyticsPage.getWidgetPrimaryLabelNumber(widgetTexts1[i]);
			Assert.assertNotNull(widgetLabelNumber1);*/
			Boolean bottomLabel1 = operationAnalyticsPage.checkBottomWidgetLabels(widgetTexts1[i]);
			Assert.assertTrue(bottomLabel1);
			operationAnalyticsPage.exitFromFullScreen(widgetTexts1[i]);						
		}		
		operationAnalyticsPage.SwitchtoDefault();		
			
		
		//Bots Dashboard		
		homePage.clickOnBotsLink();
		baseClass.waitForThreadToLoad(2);
		operationAnalyticsPage.SwitchToParentFrame();		
		String botsPageTitle = operationAnalyticsPage.dashboardTitleText();
		Assert.assertEquals(botsPageTitle, "Bots Dashboard");
		
		String[] widgetTexts2 = {"Bot Heartbeat","MVP Bots","Bot status","Top Failure Reasons","Upcoming Schedules"};
		for(int i=0; i<widgetTexts2.length; i++){		
			operationAnalyticsPage.clickToFullScreen(widgetTexts2[i]);		
			Boolean leftPanel2 = operationAnalyticsPage.checkLeftPanelControls(widgetTexts2[i]);
			Assert.assertTrue(leftPanel2);
			//String widgetLabelText2 = operationAnalyticsPage.getSubHeadNotificationText(widgetTexts2[i]);
			//System.out.println(widgetLabelText2);
			//String widgetLabelNumber2 = operationAnalyticsPage.getWidgetInnerContainserText(widgetTexts2[i]);
			//System.out.println(widgetLabelNumber2);
			Boolean bottomLabel2 = operationAnalyticsPage.checkBottomWidgetLabels(widgetTexts2[i]);
			Assert.assertTrue(bottomLabel2);
			operationAnalyticsPage.exitFromFullScreen(widgetTexts2[i]);		
		}		
		operationAnalyticsPage.SwitchtoDefault();
		
		
		//Devices Dashboard		
		homePage.clickOnDevicesLink();
		baseClass.waitForThreadToLoad(2);
		operationAnalyticsPage.SwitchToParentFrame();				
		String devicesPageTitle = operationAnalyticsPage.dashboardTitleText();
		Assert.assertEquals(devicesPageTitle, "Devices Dashboard");
		
		String[] widgetTexts3 = {"CPU Utilization","Memory Utilization","HDD Utilization","Upcoming Device Utilization","Failure Analysis"};
		for(int i=0; i<widgetTexts3.length; i++){				
			operationAnalyticsPage.clickToFullScreen(widgetTexts3[i]);				
			Boolean leftPanel3 = operationAnalyticsPage.checkLeftPanelControls(widgetTexts3[i]);
			Assert.assertTrue(leftPanel3);
			//String widgetLabelText3 = operationAnalyticsPage.getSubHeadNotificationText(widgetTexts3[i]);
			//System.out.println(widgetLabelText3);
			//String widgetLabelNumber3 = operationAnalyticsPage.getWidgetInnerContainserText(widgetTexts3[i]);
			//System.out.println(widgetLabelNumber3);
			Boolean bottomLabel3 = operationAnalyticsPage.checkBottomWidgetLabels(widgetTexts3[i]);
			Assert.assertTrue(bottomLabel3);
			operationAnalyticsPage.exitFromFullScreen(widgetTexts3[i]);				
		}
		
		operationAnalyticsPage.SwitchtoDefault();
		
		
		//Audit Dashboard		
		homePage.clickOnAuditLink();
		baseClass.waitForThreadToLoad(2);
		operationAnalyticsPage.SwitchToParentFrame();				
		String auditPageTitle = operationAnalyticsPage.dashboardTitleText();
		Assert.assertEquals(auditPageTitle, "Audit Dashboard");
		
		String[] widgetTexts4 = {"by Activity Type","by Host Machine Type","Trail by User Name","Log By User Name"};
		for(int i=0; i<widgetTexts4.length; i++){			
			operationAnalyticsPage.clickToFullScreen(widgetTexts4[i]);		
			Boolean leftPanel4 = operationAnalyticsPage.checkLeftPanelControls(widgetTexts4[i]);
			Assert.assertTrue(leftPanel4);
			/*String widgetLabelText4 = operationAnalyticsPage.getWidgetPrimaryLabelText(widgetTexts4[i]);
			Assert.assertNotNull(widgetLabelText4);
			String widgetLabelNumber4 = operationAnalyticsPage.getWidgetPrimaryLabelNumber(widgetTexts4[i]);
			Assert.assertNotNull(widgetLabelNumber4);*/
			//Boolean bottomLabel4 = operationAnalyticsPage.checkBottomWidgetLabels(widgetTexts4[i]);
			//Assert.assertTrue(bottomLabel4);
			operationAnalyticsPage.exitFromFullScreen(widgetTexts4[i]);					
		}		
		operationAnalyticsPage.SwitchtoDefault();		

	}

}
