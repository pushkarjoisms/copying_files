package com.aa.automation;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AnalyzeTest {
	
	WebDriver driver;
	WebDriverWait wait;
	AnalyzePage analyzePage;
	LoginPage loginPage;
	LogoutPage logoutPage;
	DashboardPage dashboardPage;
	BaseClass baseClass;
	LoginTest loginTest;
	RolesTest rolesTest;
	LogoutTest logoutTest;

	private String randomString() {
		int length = 10;
		boolean useLetters = true;
		boolean useNumbers = false;
		return RandomStringUtils.random(length, useLetters, useNumbers);
	}

	public AnalyzeTest(WebDriver driver,WebDriverWait wait) throws Exception
	{
		this.driver= driver;
		wait = new WebDriverWait(driver,60);
		this.wait = wait;
		analyzePage = new  AnalyzePage(driver, wait);
		loginPage = new  LoginPage(driver, wait);
		logoutPage = new  LogoutPage(driver,wait);
		dashboardPage = new  DashboardPage(driver,wait);
		baseClass = new BaseClass(driver,wait);
		loginTest = new LoginTest(driver,wait);
		rolesTest = new RolesTest(driver,wait);
		logoutTest = new LogoutTest(driver,wait);
		
	}

	//Compare Published Dashboard
	public void clickCompare_OpensAdjecentScreen_ForSearcingDashboardForCompare() throws Exception
	{
		System.out.println("Executing Method No: 46");

		//Arrange
		String publisheddb = this.randomString();
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(publisheddb);
		analyzePage.deletePublishedDashboard(publisheddb);
		baseClass.waitForThreadToLoad(2);
		analyzePage.clickOnConfigure();
		analyzePage.selectDashboard();
		analyzePage.searchAndSelectDashboard(BaseClass.GetPropertyName("Task1"));
		analyzePage.publishDashboard(publisheddb);
		
		//Act
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(publisheddb);
		analyzePage.clickOnCompareButton();
		
		//Assert
		String result = analyzePage.secondaryDashboardSearchBox();
		Assert.assertNotNull(result);
	}

	public void analyzeDashboard() throws Exception
	{
		System.out.println("Executing Method No: 5");
		
		//Arrange
		String publishedb = BaseClass.GetPropertyName("Publisheddb");
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(publishedb);
		
		//Act
		Boolean Result = analyzePage.publishedDashboardUiLinks();
		analyzePage.deletePublishedDashboard(publishedb);
		baseClass.waitForThreadToLoad(5);
		analyzePage.searchPublishedDashboard(publishedb);
		
		//Assert
		String dashboard = analyzePage.verifyPublishedDashboardIsDeleted(publishedb);
		analyzePage.clickOnConfigure();
		Assert.assertEquals("",dashboard);
		Assert.assertTrue(Result);
	}
	
	public void analyzeDashboardforParentTask() throws Exception
	{
		System.out.println("Executing Method No: 8");
		
		//Arrange
		String publisheddb = BaseClass.GetPropertyName("Publisheddb");
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(publisheddb);
		
		//Act
		Boolean Result = analyzePage.publishedDashboardUiLinks();
		analyzePage.deletePublishedDashboard(publisheddb);
		baseClass.waitForThreadToLoad(5);
		analyzePage.searchPublishedDashboard(publisheddb);
		
		//Assert
		String dashboard = analyzePage.verifyPublishedDashboardIsDeleted(publisheddb);
		analyzePage.clickOnConfigure();
		Assert.assertEquals("",dashboard);
		Assert.assertTrue(Result);
	}

	public void resize_PublishedDashboardWidget_ExpandAndCollpaseWidgetSuccessfully() throws Exception
	{
		System.out.println("Executing Method No: 47");

		//Arrange
		String publisheddb = this.randomString();
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(publisheddb);
		analyzePage.deletePublishedDashboard(publisheddb);
		baseClass.waitForThreadToLoad(2);
		analyzePage.clickOnConfigure();
		analyzePage.selectDashboard();
		analyzePage.searchAndSelectDashboard(BaseClass.GetPropertyName("Task1"));
		analyzePage.publishDashboard(publisheddb);

		//Arrange
		String result = "";
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(publisheddb);

		//Act	
		baseClass.switchToChildFrame("dashboardContent");
		baseClass.waitForThreadToLoad(2);
		analyzePage.selectFirstWidgetInIframe("the total Discount");
		analyzePage.expandWidget();		
		
		//Assert
		result = analyzePage.minimizeWidget();		
		Assert.assertEquals(result, "block");
		baseClass.switchToParentFrame();
	}
	
	public void checkAnalyticsExpert_DoesNot_SeeAnalyzeSection()throws Exception
	{
		System.out.println("Executing Method No: 59");		
		
		baseClass.waitForThreadToLoad(2);
		logoutTest.logout_FromBotInsight_VerifyLogout();
		
		//Act		
		baseClass.waitForThreadToLoad(2);
		loginTest.loginToControlRoom_AdminCredentials();
		rolesTest.CreateExpertRoleAndUser_AssignedRoleToUser_ForBotInsight();	
		
		//Arrange
		baseClass.waitForThreadToLoad(5);
		loginTest.loginToBotInsight_ValidCredentials_VerifyExpertLogin();
		dashboardPage.clickOnConfigure();
		dashboardPage.searchAndSelectDashboard("ProductionDashboardTask");
				
		
		//Assert	
		boolean result = dashboardPage.VisibiltyofDashbaord();
		boolean result1 = analyzePage.checkAnalyzeSectionAvailability();		
		Assert.assertTrue(result);
		Assert.assertFalse(result1);
		
	}

	public void checkAnalyticsConsumer_DoesNot_SeeConfigureSection() throws Exception
	{
		System.out.println("Executing Method No: 60");		
		
		baseClass.waitForThreadToLoad(2);
		logoutTest.logout_FromBotInsight_VerifyLogout();
		
		//Act		
		baseClass.waitForThreadToLoad(2);
		loginTest.loginToControlRoom_AdminCredentials();
		rolesTest.CreateConsumerRoleAndUser_AssignedRoleToUser_ForBotInsight();	
		
		//Arrange
		baseClass.waitForThreadToLoad(5);
		loginTest.loginToBotInsight_ValidCredentials_VerifyConsumerLogin();	
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(BaseClass.GetPropertyName("Publisheddb"));		
		
		//Assert
		boolean result = analyzePage.DefaultAnalyzeUiLinks();
		boolean result1 = analyzePage.checkConfigureSectionAvailability();		
		Assert.assertTrue(result);
		Assert.assertFalse(result1);
		
		/*//Setup For Next Script Execution
		logoutPage.logout();
		loginPage.enterUserForRelogin(BaseClass.GetPropertyName("Username"));
		loginPage.enterPassword();
		loginPage.login();
		
		//Execute task to Generate Dashboard
		baseClass.executeTaskFromAAEClient("TelecomOrderEntry.atmx", "AAPlayer.exe");
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("TelecomOrderEntry");
		dashboardPage.VisibiltyofDashbaord();
		*/
	}
}
