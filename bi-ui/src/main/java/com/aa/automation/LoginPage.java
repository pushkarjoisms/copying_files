package com.aa.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

	WebDriver driver;
	WebDriverWait wait;
	WebElement element;
	BaseClass baseClass;
	String text;

	//Selectors or Object Identifier
	By userName = By.xpath("//*[@ng-model='userName']");
	By password = By.xpath("//*[@ng-model='password']");
	By Login =By.id("loginBtn");
	By ErrorLabel = By.xpath("//*[@ng-show='login.showError']");
	By loginProfileIcon = By.xpath("//img[@src='assets/images/user.png']");
	By keepMeLoggedInCheckbox = By.xpath("//input[@id='keepMeLoggedIn']");

	//Constructor to initialize driver
	public LoginPage(WebDriver driver, WebDriverWait wait) throws Exception{
		this.driver = driver;
		wait = new WebDriverWait(driver,60);
		this.wait = wait;		
		baseClass = new BaseClass(driver, wait);
	}		

	public void enterUsername() throws Exception
	{
		element = driver.findElement(userName);
		text = BaseClass.GetPropertyName("Username");
		baseClass.enterDataIntoEditbox(element, text);
	}
	
	public void enterUserForRelogin(String username) throws Exception
	{
		element = driver.findElement(userName);
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, username);
	}
	
	public void enterUserForCRlogin(String username) throws Exception
	{
		element = driver.findElement(userName);
		baseClass.enterDataIntoEditbox(element, username);
	}
	
	public void enterPassword() throws Exception
	{
		element = driver.findElement(password);
		text = BaseClass.GetPropertyName("Password");
		baseClass.enterDataIntoEditbox(element, text);
	}
	
	public void checkKeepMeLoggedIn()throws Exception
	{		
		element = driver.findElement(keepMeLoggedInCheckbox);
		baseClass.clickOnWebElement(element);
	}

	public void login()throws Exception
	{
		element = driver.findElement(Login);
		baseClass.clickOnWebElement(element);
	}

	public String getLoginStatus() throws Exception
	{
		element = driver.findElement(loginProfileIcon);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTagNameFromWebElement(element);
	}
	
	@FindBy(how = How.NAME, using = "username")
	private WebElement username;

	@FindBy(how = How.NAME, using = "password")
	private WebElement userPassword;

	 @FindBy(how = How.XPATH, using = "//button[contains(., 'Log in')]")
	 private WebElement loginBtn;
	 
	public void doLoginToControlRoom(String login_username, String login_password) throws Exception {	
		
		element = driver.findElement(By.xpath("//input[@name='username']"));
		baseClass.enterDataIntoEditbox(element, login_username);		
		element = driver.findElement(By.xpath("//input[@name='password']"));
		baseClass.enterDataIntoEditbox(element, login_password);
		element = driver.findElement(By.xpath("//*[contains(text(), 'Remember my username')]"));
		baseClass.selectTheCheckbox(element);
		element = driver.findElement(By.xpath("//button[contains(., 'Log in')]"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
		baseClass.waitForThreadToLoad(5);
	}
	
	// User Details and logout option
		@FindBy(how = How.XPATH, using = "//*[contains(@class, 'headerbar-tray-option-button headerbar-tray-option--labeled')]")
		private WebElement loggedInUserName;
		
		
	// Logged in User status
		public boolean loggedInUserStatus() {
			element = driver.findElement(By.xpath("//*[contains(@class, 'headerbar-tray-option-button headerbar-tray-option--labeled')]"));
			boolean userStatus = false;
			try {
				if (element.isDisplayed()) {
					userStatus = true;
				}
			} catch (Exception e) {
				userStatus = false;
			}
			return userStatus;
		}
	
}
