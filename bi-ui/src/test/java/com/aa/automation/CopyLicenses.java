package com.aa.automation;

import com.aa.common.ControlRoomUtility;
import com.aa.common.Role;
import com.aa.common.UserCreation;
import org.apache.log4j.Logger;


public class CopyLicenses {
  final static Logger log = Logger.getLogger(CopyLicenses.class);
  //private ControlRoomUtility crutil = ControlRoomUtility.getInstance();

  public static void main(String[] args) {
    ControlRoomUtility crutil = ControlRoomUtility.getInstance();
    try {
      System.out.println("Copying License data from Artifacts: ");
      crutil.copyLicenseFilesFromArtifacts();
      //healthCheckCR();
      //setRepositoryPathAndAccessURL();
    } catch (Exception e) {
      e.printStackTrace();
    }


  }
}
