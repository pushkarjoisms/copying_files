package com.aa.automation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.IOException;
public class DashboardTest {

	WebDriver driver;
	WebDriverWait wait;
	DashboardPage dashboardPage;
	DataProfilePage dataProfilePage;
	AnalyzePage analyzePage;
	BaseClass baseClass;
	LoginPage loginPage;
	LogoutPage logoutPage;
	LoginTest loginTest;
	RolesTest rolesTest;
	LogoutTest logoutTest;
	String projectRepo = System.getProperty("user.dir")+"\\src\\test\\resources\\Tasks";

	public DashboardTest(WebDriver driver, WebDriverWait wait) throws Exception
	{
		this.driver= driver;
		wait = new WebDriverWait(driver, 60);
		this.wait = wait; 
		baseClass = new BaseClass(driver,wait);
		dashboardPage = new  DashboardPage(driver,wait);
		dataProfilePage = new DataProfilePage(driver, wait);
		analyzePage = new AnalyzePage(driver, wait);
		loginPage = new  LoginPage(driver,wait);
		logoutPage = new  LogoutPage(driver,wait);
		loginTest = new LoginTest(driver,wait);
		rolesTest = new RolesTest(driver,wait);
		logoutTest = new LogoutTest(driver,wait);
	}

	public void deleteSavedAsDashboard(String dashboard) throws Exception
	{
		boolean elementFound  = false;
		try{
			dashboardPage.clickOnConfigure();
			dashboardPage.selectDashboard();
			//elementFound = dashboardPage.existWebElement(dashboard);
			dashboardPage.deleteDashboard(dashboard);			
			}catch(Exception e){
				System.out.println("Error in deleteSavedDashboard() Method: " + e.getStackTrace());
		}
	}

	public void verifyBoomarkItemInsertedSuccessfully() throws Exception
	{
		System.out.println("Executing Method No: 4");
		
		//Arrange
		String dashboard = BaseClass.GetPropertyName("Dashboard");
		deleteSavedAsDashboard(dashboard);		
		dashboardPage.searchAndSelectDashboard(BaseClass.GetPropertyName("Task"));		
		dashboardPage.clickSaveAsButton();
		dashboardPage.enterDashboardName(dashboard);
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(5);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);

		//Act
		dashboardPage.clickOnBookMarkItem();
		baseClass.waitForThreadToLoad(2);
		
		//Assert
		dashboardPage.clickOnToolBarBookMarkIcon();
		String bookmarkeItem = dashboardPage.getBookmarkItem();
		Assert.assertTrue(bookmarkeItem.contains(dashboard));
	}

	public void verifyBoomarkItemInsertedSuccessfullyforParenTask() throws Exception
	{
		System.out.println("Executing Method No: 7");
		
		//Arrange
		String dashboard = BaseClass.GetPropertyName("Dashboard");
		deleteSavedAsDashboard(dashboard);		
		dashboardPage.searchAndSelectDashboard(BaseClass.GetPropertyName("ParentTask"));		
		dashboardPage.clickSaveAsButton();
		dashboardPage.enterDashboardName(dashboard);
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(5);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);

		//Act
		dashboardPage.clickOnBookMarkItem();
		baseClass.waitForThreadToLoad(2);

		//Assert
		dashboardPage.clickOnToolBarBookMarkIcon();
		String bookmarkeItem = dashboardPage.getBookmarkItem();
		Assert.assertTrue(bookmarkeItem.contains(dashboard));
	}
	public void clickDelete_RemoveBookmarkedDashboard_FromBookmarkList() throws Exception
	{
		System.out.println("Executing Method No: 24");
		
		//Arrange
		String task = BaseClass.GetPropertyName("Task");
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard(task);
		dashboardPage.clickOnBookMarkItem();
		dashboardPage.clickOnToolBarBookMarkIcon();

		//Act
		Boolean result = dashboardPage.getBookmarkSections();
		dashboardPage.deleteBookMarks(task);
		String result1 = dashboardPage.confirmBookmarkDeleted();

		//Assert
		Assert.assertTrue(result);
		Assert.assertTrue(result1.contains("Bookmark "+task+" deleted!"));		
	}

	public void delete_RemovesSavedDashboard_Successfully() throws Exception
	{		
		//Arrange
		String dashboard = BaseClass.GetPropertyName("Dashboard");
		String PublishedDb = BaseClass.GetPropertyName("Publisheddb");		
		analyzePage.clickOnAnalyze();
		analyzePage.searchAndDeletePublishedDashboard(PublishedDb);
		baseClass.waitForThreadToLoad(2);
		dashboardPage.clickOnConfigure();
		dashboardPage.searchAndSelectDashboard(dashboard);
		dashboardPage.accessBookmarkItem();
		baseClass.waitForThreadToLoad(5);
		dashboardPage.publishDashboard(PublishedDb);		

		//Act
		dashboardPage.deleteSavedDashbord(dashboard);
		baseClass.waitForThreadToLoad(5);
		dashboardPage.searchDeletedDashboard(dashboard);

		//Assert
		String dashboardDeleted = dashboardPage.verifydashboardDeleted();
		Assert.assertEquals("",dashboardDeleted);
	}

	public void resize_Widget_ExpandAndCollpaseWidgetSuccessfully() throws Exception
	{
		System.out.println("Executing Method No: 40");
		
		//Arrange
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("OnlyNumericDataType");

		//Act
		String result = "";
		baseClass.switchToChildFrame("dashboardContent");
		dashboardPage.selectFirstWidgetInIframe("the total Numeric1");
		dashboardPage.expandWidget();			

		//Assert
		result = dashboardPage.minimizeWidget();
		baseClass.switchToParentFrame();
		Assert.assertEquals(result, "block");
	}

	public void exportlinkBehaviour_OnExpandedWidget_OfDashboard() throws Exception
	{
		System.out.println("Executing Method No: 44");
		
		//Arrange
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("OnlyNumericDataType");

		//Act
		boolean  result =false;
		baseClass.switchToChildFrame("dashboardContent");
		dashboardPage.selectFirstWidgetInIframe("the total Numeric1");
		dashboardPage.expandWidget();
		baseClass.waitForThreadToLoad(2);

		//Assert
		result = dashboardPage.exportLinkOnleftPanelOfExpandedWidgets();
		//dashboardPage.minimizeWidget1();
		//baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(5);
		Assert.assertTrue(result);
		baseClass.switchToParentFrame();
	}

	public void dashboardUI_Shows_AllDefaulsLinks() throws Exception
	{
		System.out.println("Executing Method No: 23");
		
		//Arrange
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();		

		//Act
		dashboardPage.searchAndSelectDashboard(BaseClass.GetPropertyName("Task"));

		//Assert
		Boolean result = dashboardPage.dashboardUiLinks();
		Assert.assertTrue(result);
	}

	public void verifyDashboardSaved() throws Exception
	{
		//Arrange	
		String dashboard = BaseClass.GetPropertyName("Dashboard");
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		deleteSavedAsDashboard(dashboard);
		dataProfilePage.searchAndSelectTaskHasVariableNameChanges();
		baseClass.waitForThreadToLoad(2);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(5);		
			
		//Act
		dashboardPage.clickSaveAsButton();
		dashboardPage.enterDashboardName(dashboard);
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(3);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);
		
		//Assert
		Boolean result = dashboardPage.VisibiltyofDashbaord();
		Assert.assertTrue(result);
	}

	public void TrySaving_DuplicateDashboard_GivesErrorMessage() throws Exception
	{
		System.out.println("Executing Method No: 25");
		
		//Arrange
		String dashboard = BaseClass.GetPropertyName("Dashboard");
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		deleteSavedAsDashboard(dashboard);
		dashboardPage.searchAndSelectDashboard(BaseClass.GetPropertyName("Task"));
		baseClass.refreshThePage();

		//Act
		for (int i=0;i<2;i++){
			dashboardPage.clickSaveAsButton();
			dashboardPage.enterDashboardName(dashboard);
			dashboardPage.clickOkayLink();
			baseClass.waitForThreadToLoad(5);
		}		

		//Assert
		String result = dashboardPage.getErrorMessageOfDuplicateDashboard();
		Assert.assertTrue(result.contains(""+dashboard+" already exists!"));
	}

	public void tryPublish_DuplicateDashboard_GivesErrorMessage() throws Exception
	{
		System.out.println("Executing Method No: 36");
		
		//Arrange
		String dashboard = BaseClass.GetPropertyName("Task");
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard(dashboard);
		baseClass.waitForThreadToLoad(2);

		//Act
		for (int i=0;i<2;i++){
			dashboardPage.clickPublishLink();
			dashboardPage.enterDashboardName(dashboard);
			dashboardPage.clickOkayLink();
			baseClass.waitForThreadToLoad(5);
		}		

		//Assert
		String result = dashboardPage.getErrorMessageOfDuplicateDashboard();
		Assert.assertTrue(result.contains(""+dashboard+" already exists!"));
	}

	public void verifyNewAndRenamedVariableOnDashboard() throws Exception
	{
		//Arrange
		String newVariableText = null;
		String renamedVariableText = null;
		dashboardPage.selectDashboard();

		//Act		
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(5);
		//driver.switchTo().frame((WebElement) driver.findElement(By.xpath("//iframe")));
		baseClass.switchToChildFrame("dashboardContent");
		baseClass.waitForThreadToLoad(2);
		dashboardPage.expandTheFilter();
		newVariableText = dashboardPage.getInfoOfNewVariable();
		renamedVariableText = dashboardPage.getInfoOfRenamedVariable();			

		//Assert		
		baseClass.waitForThreadToLoad(2);
		//baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(5);
		baseClass.switchToParentFrame();
		Assert.assertEquals("New", newVariableText);
		//Assert.assertEquals("Renamed", renamedVariableText);
		Assert.assertEquals("What's the total Renamed?", renamedVariableText);
		
	}

	//Delete Widget
	public void deleteWidget_Removes_WidgetSuccessfully() throws Exception
	{
		System.out.println("Executing Method No: 35");
		
		//Arrange
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("OnlyNumericDataType");

		//Act
		/*List<WebElement> frameset = driver.findElements(By.tagName("iframe"));      
        for (WebElement framename : frameset){
            System.out.println("frameid is: " + framename.getAttribute("id")); 
        }*/
        //driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[id='dashboardContent']")));
		baseClass.switchToChildFrame("dashboardContent");
		//driver.switchTo().frame((WebElement) driver.findElement(By.xpath("//iframe[@id='dashboardContent']")));
		dashboardPage.selectFirstWidgetInIframe("the total Numeric1");
		
		dashboardPage.deleteFirstWdgetInIframe();
		dashboardPage.completeDeleteWidgetProcess();				

		//Assert
		boolean result = dashboardPage.selectFirstWidgetInIframe("the total Numeric1");
		baseClass.switchToParentFrame();
		Assert.assertFalse(result);
	}
	
	//Delete Widget
	public void deleteWidgetSuccessfully() throws Exception
	{					
		//Arrange
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("OnlyNumericDataType");

		//Act
		baseClass.switchToChildFrame("dashboardContent");
		//baseClass.waitForFrameToBeAvailableAndSwitchToIt("dashboardContent");
		dashboardPage.selectFirstWidgetInIframe("the total Numeric1");
		dashboardPage.deleteFirstWdgetInIframe();
		dashboardPage.completeDeleteWidgetProcess();				

		//Assert
		boolean result = dashboardPage.selectFirstWidgetInIframe("the total Numeric1");			
		Assert.assertFalse(result);
		baseClass.switchToParentFrame();
	}

	public void saveAs_Includes_DeletedWidget() throws Exception
	{
		System.out.println("Executing Method No: 37");
		
		//Arrange
		deleteSavedAsDashboard("SavedDashboard");
		deleteWidgetSuccessfully();

		//Act
		boolean result = false;
		dashboardPage.clickSaveAsButton();
		dashboardPage.enterDashboardName("SavedDashboard");
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(2);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);
		baseClass.switchToChildFrame("dashboardContent");

		//Assert
		result = dashboardPage.selectFirstWidgetInIframe("the total Numeric1");
		Assert.assertTrue(result);
		baseClass.switchToParentFrame();
		dashboardPage.deleteSavedAsDashboard();
	}

	public void save_DoesNotInclude_DeletedWidget() throws Exception
	{
		System.out.println("Executing Method No: 38");
		
		//Arrange
		deleteSavedAsDashboard("SavedDashboard");
		dashboardPage.searchAndSelectDashboard("OnlyNumericDataType");
		baseClass.waitForThreadToLoad(2);
		dashboardPage.clickSaveAsButton();
		dashboardPage.enterDashboardName("SavedDashboard");
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(5);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);

		//Act
		boolean result = true;	
		baseClass.switchToChildFrame("dashboardContent");
		dashboardPage.selectFirstWidgetInIframe("the total Numeric1");
		dashboardPage.deleteFirstWdgetInIframe();
		dashboardPage.completeDeleteWidgetProcess();
		baseClass.switchToParentFrame();
		dashboardPage.clickSaveOnButton();
		baseClass.waitForThreadToLoad(5);
		//baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);
		baseClass.switchToChildFrame("dashboardContent");		

		//Assert
		result = dashboardPage.selectFirstWidgetInIframe("the total Numeric1");	
		baseClass.switchToParentFrame();
		Assert.assertFalse(result);
	}

	public void saveDashboard_WithNameHasOnlyOneCharacter_ThrowsValidationMessage() throws Exception
	{
		System.out.println("Executing Method No: 39");
		
		//Arrange
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("OnlyNumericDataType");

		//Act
		dashboardPage.clickSaveAsButton();
		dashboardPage.enterDashboardName("D");
		dashboardPage.clickOkayLink();		

		//Assert
		String result = dashboardPage.getDashboardNameSizeValidation();
		baseClass.waitForThreadToLoad(2);
		//baseClass.refreshThePage();
		Assert.assertEquals(result, "Please enter at least 2 characters for dashboard name.");
	}

	public void dashbordUI_ShowsInforamtionText_ToSearchDashboard() throws Exception
	{
		System.out.println("Executing Method No: 41");
		
		//Arrange
		deleteSavedAsDashboard("SavedDashboard");
		dashboardPage.searchAndSelectDashboard("OnlyNumericDataType");

		//Act
		dashboardPage.clickSaveAsButton();
		dashboardPage.enterDashboardName("SavedDashboard");
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(5);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);
		dashboardPage.deleteSavedAsDashboard();		

		//Assert
		String result = dashboardPage.getDashboardUIMessage();
		Assert.assertEquals(result,"Please search and choose a task to start your configuration!");
	}									   

	public void dashbordSearched_loadsAllDefaultWidgets_Succesfully() throws Exception
	{
		System.out.println("Executing Method No: 42");
		
		//Arrange
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();		

		//Act
		boolean result = false;	
		dashboardPage.searchAndSelectDashboard("TelecomOrderEntry");
		baseClass.switchToChildFrame("dashboardContent");

		//Assert
		result = dashboardPage.dashboardWidgetsInIframe();			
		Assert.assertTrue(result);
		baseClass.switchToParentFrame();
	}

	public void verifyDeletedDashboard() throws Exception
	{
		//Arrange
		baseClass.switchToParentFrame();

		//Act
		dashboardPage.deleteSavedAsDashboard();

		//Assert
		String dashboardDeleted = dashboardPage.verifydashboardDeleted();
		Assert.assertEquals("",dashboardDeleted);
	}

	public void clickCompare_OpensAdjecentScreen_ForSearcingDashboardForCompare() throws IOException, Exception
	{
		System.out.println("Executing Method No: 21");
		
		//Arrange
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();	

		//Act
		dashboardPage.searchAndSelectDashboard(BaseClass.GetPropertyName("Task"));
		dashboardPage.clickOnCompareButton();		

		//Assert
		String result = dashboardPage.secondaryDashboardSearchBox();
		Assert.assertNotNull(result);
	}

	public void searchButton_loadsDashboard_OnCompareScreen() throws IOException, Exception
	{
		System.out.println("Executing Method No: 22");
		
		//Arrange
		String task = BaseClass.GetPropertyName("Task");
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard(task);		

		//Act
		dashboardPage.clickOnCompareButton();
		dashboardPage.searchSecondaryDashboard(task);		

		//Assert
		String result = dashboardPage.loadSeacondaryDashboard();
		Assert.assertNotNull(result);
	}

	public void publishedDashboard_GetsUpdated_WithProductionData() throws Exception
	{
		System.out.println("Executing Method No: 58");
		
		//Arrange
		baseClass.copyTheFileFromSourceToDestination1("ProductionDashboardTask.atmx", "ProductionDashboardTask.atmx");
		baseClass.waitForThreadToLoad(2);
		baseClass.executeTaskFromAAEClient1("ProductionDashboardTask.atmx", "AAPlayer.exe");
		logoutTest.logout_FromBotInsight_VerifyLogout();
		
		//Act		
		baseClass.waitForThreadToLoad(2);
		loginTest.loginToControlRoom_AdminCredentials();
		rolesTest.CreateRoleAndUser_AssignedRoleToUser_ForBotInsight1();		
		baseClass.executeTaskFromAAEClient("LoginToBotRunner.atmx", "AAPlayer.exe");		
		baseClass.waitForThreadToLoad(2);
				
		baseClass.logTextToFile(projectRepo+"\\uploadtest.txt", "taskToSelect=ProductionDashboardTask.atmx");
		baseClass.logTextToFile(projectRepo+"\\selectParentFolder.txt", "selectParentFolder=My Tasks");
		baseClass.logTextToFile(projectRepo+"\\selectChildFolder.txt", "selectChildFolder=resources");
		baseClass.logTextToFile(projectRepo+"\\selectSubChildFolder.txt", "selectSubChildFolder=resources");
		baseClass.executeTaskFromAAEClient("UploadTask_VCS_OFF.atmx", "AAPlayer.exe");
		baseClass.waitForThreadToLoad(5);
		
		rolesTest.CreateSchedule_RunBotInControlRoom();		
		
		logoutTest.logoutFromControlRoom();
		baseClass.waitForThreadToLoad(5);
		loginTest.loginToBotInsight_ValidCredentials_VerifyLogin();
				
		String PublishedDb = BaseClass.GetPropertyName("Publisheddb");
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(PublishedDb);
		analyzePage.deletePublishedDashboard(PublishedDb);	
		baseClass.waitForThreadToLoad(2);
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("ProductionDashboardTask");		
		dashboardPage.publishDashboard(PublishedDb);

		//Assert
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(PublishedDb);
		boolean flag = dashboardPage.VisibiltyofDashbaord();
		baseClass.waitForThreadToLoad(2);
		baseClass.executeTaskFromAAEClient("LoginToBotCreator.atmx", "AAPlayer.exe");
		
		Assert.assertTrue(flag);
		
		//int result = dashboardPage.getProductionDashboardData();
		//Assert.assertNotEquals(result, 0);
	}

	public void CreateRoleAndUser_ForBotInsight_RoleBasedAccess() throws Exception
	{
		System.out.println("Executing Method No: 61");
		
		
		//Arrange
		baseClass.copyTheFileFromSourceToDestination("ProductionDashboardTask_RBA.atmx", "ProductionDashboardTask_RBA.atmx");
		baseClass.waitForThreadToLoad(2);
		baseClass.executeTaskFromAAEClient("ProductionDashboardTask_RBA.atmx", "AAPlayer.exe");
		logoutTest.logout_FromBotInsight_VerifyLogout();
				
		//Act		
		baseClass.waitForThreadToLoad(2);
		loginTest.loginToControlRoom_AdminCredentials();
		rolesTest.CreateRoleAndUser_AssignedRoleToUser_ForRoleBasedAccess();		
		baseClass.executeTaskFromAAEClient("LoginToBotRunner1.atmx", "AAPlayer.exe");
		baseClass.waitForThreadToLoad(2);		
		baseClass.logTextToFile(projectRepo+"\\uploadtest.txt", "taskToSelect=ProductionDashboardTask_RBA.atmx");
		baseClass.logTextToFile(projectRepo+"\\selectParentFolder.txt", "selectParentFolder=My Tasks");
		baseClass.logTextToFile(projectRepo+"\\selectChildFolder.txt", "selectChildFolder=resources");
		baseClass.logTextToFile(projectRepo+"\\selectSubChildFolder.txt", "selectSubChildFolder=Tasks");
		baseClass.executeTaskFromAAEClient("UploadTask_VCS_OFF.atmx", "AAPlayer.exe");
		baseClass.waitForThreadToLoad(5);
		
		rolesTest.CreateSchedule_RunBotInControlRoom_RBA();		
				
		logoutTest.logoutFromControlRoom();
		baseClass.waitForThreadToLoad(5);
		loginTest.loginToBotInsight_ValidCredentials_VerifyRoleBasedAccess();				
				
		String PublishedDb1 = BaseClass.GetPropertyName("Publisheddb1");
		String PublishedDb = BaseClass.GetPropertyName("Publisheddb");
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(PublishedDb1);
		analyzePage.deletePublishedDashboard(PublishedDb1);	
		baseClass.waitForThreadToLoad(2);
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("ProductionDashboardTask_RBA");		
		dashboardPage.publishDashboard(PublishedDb1);

		//Assert
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(PublishedDb1);
		boolean flag1 = dashboardPage.VisibiltyofDashbaord();
		
		baseClass.waitForThreadToLoad(2);
		analyzePage.searchPublishedDashboard(PublishedDb);
		boolean flag = dashboardPage.VisibiltyofDashbaord();	
				
		baseClass.waitForThreadToLoad(2);
		baseClass.executeTaskFromAAEClient("LoginToBotCreator.atmx", "AAPlayer.exe");
		
		Assert.assertTrue(flag1);
		Assert.assertFalse(flag);
		
		
		
		/*
		//Arrange
		baseClass.executeTaskFromAAEClient("Upload.atmx", "AAPlayer.exe");
		baseClass.executeTaskFromAAEClient("ProductionDashboardTask.atmx", "AAPlayer.exe");

		//Act
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("ProductionDashboardTask");
		dashboardPage.publishDashboard(BaseClass.GetPropertyName("Publisheddb"));
		
		baseClass.executeTaskFromAAEClient("RBAC.atmx", "AAPlayer.exe");
		baseClass.waitForThreadToLoad(2);
		baseClass.switchToParentFrame();
		logoutPage.logout();
		loginPage.enterUserForRelogin("RBAC");
		loginPage.enterPassword();
		loginPage.login();
		analyzePage.clickOnAnalyze();		

		//Assert
		Boolean ElementFound = dashboardPage.existWebElement(BaseClass.GetPropertyName("Publisheddb"));
		Assert.assertFalse(ElementFound);*/
	}


	public void checkStateCityChart_ForMortgageTask_VerifyNewlyAddedChartLoaded() throws Exception 
	{		
		System.out.println("Executing Method No: 50");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("MortgageProcessing.atmx", "AAPlayer.exe");
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("MortgageProcessing");
		dataProfilePage.selectDataProfile();
		dataProfilePage.clickOnEditLink();

		//Act
		String Header  = "";
		String Header1  = "";
		int row[] = {3, 10};
		String dataType[] ={"Country Code","State Code"}; 
		dataProfilePage.setCityAndStateCode(row[0] ,dataType[0]);
		dataProfilePage.setCityAndStateCode(row[1] ,dataType[1]);	
		dataProfilePage.clickOnSaveAndGenerateNewDashboard();
		//dataProfilePage.clickGenerateNewDashbord();	
		baseClass.waitForThreadToLoad(10);
		baseClass.switchToChildFrame("dashboardContent");
		
		//Assert
		dashboardPage.checkStateElementJavaScriptExecutor();
		Header = dashboardPage.getTextbyState();
		//System.out.println(Header);
		dashboardPage.checkCityElementJavaScriptExecutor();
		Header1 = dashboardPage.getTextbyCity();
		//System.out.println(Header1);
		baseClass.switchToParentFrame();
		baseClass.refreshThePage();		
		Assert.assertEquals(Header, "Sum Of Loanbalance By State");
		Assert.assertEquals(Header1,"Sum Of Loanbalance By City");
	}


	public void checkDashboardWidgetLevelFilter_ForMonthlyProcessingTask_VerifyAddedFilters() throws Exception
	{

		System.out.println("Executing Method No: 51");
		
		//Arrange
		String dashboard = BaseClass.GetPropertyName("Dashboard");
		deleteSavedAsDashboard(dashboard);
		baseClass.executeTaskFromAAEClient("MonthlyCreditProcessing.atmx", "AAPlayer.exe");
		baseClass.logTextToFile(projectRepo+"\\uploadtest.txt", "taskToSelect=MonthlyCreditProcessing.atmx");
		baseClass.logTextToFile(projectRepo+"\\selectParentFolder.txt", "selectParentFolder=My Tasks");
		baseClass.logTextToFile(projectRepo+"\\selectChildFolder.txt", "selectChildFolder=resources");
		baseClass.logTextToFile(projectRepo+"\\selectSubChildFolder.txt", "selectSubChildFolder=Tasks");
		//baseClass.executeTaskFromAAEClient("UploadTask_VCS_OFF.atmx", "AAPlayer.exe");
		baseClass.waitForThreadToLoad(2);
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("MonthlyCreditProcessing");
		dashboardPage.clickSaveAsButton();		
		dashboardPage.enterDataInPlaceHolder(dashboard);
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(5);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);

		//Act
		String verifyFilters = null;
		baseClass.switchToChildFrame("dashboardContent");
		dashboardPage.clickOnFilter();		
		verifyFilters = dashboardPage.addGenericFilters();
		dashboardPage.clickOnCloseButton();
		dashboardPage.clickOnWidget();
		dashboardPage.clickOnWidgetFilter();
		dashboardPage.addWidgetLevelFilters();
		dashboardPage.clickOnWidgetFilter();
		
		String publisheddb = BaseClass.GetPropertyName("Publisheddb");
		baseClass.switchToParentFrame();
		dashboardPage.clkSaveButton1();
		baseClass.waitForThreadToLoad(2);
		analyzePage.clickOnAnalyze();
		//analyzePage.searchAndDeletePublishedDashboard(publisheddb);
		analyzePage.searchPublishedDashboard(publisheddb);
		analyzePage.deletePublishedDashboard(publisheddb);
		baseClass.waitForThreadToLoad(2);
		dashboardPage.clickOnConfigure();
		dashboardPage.searchAndSelectDashboard(dashboard);
		dashboardPage.clickPublishLink();
		dashboardPage.enterDataInPlaceHolder(publisheddb);
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(5);
		analyzePage.clickOnAnalyze();
		analyzePage.searchPublishedDashboard(publisheddb);
		

		//Assert
		String title = "";		
		baseClass.switchToChildFrame("dashboardContent");
		title = dashboardPage.dashboardTitle();			
		Assert.assertEquals("GenderMarital Status", verifyFilters);
		Assert.assertEquals("PublishedDb", title);
		baseClass.switchToParentFrame();
	}
	
	public void checkDisplayName_ForTelecomOrderEntryTask_VerifyTitleonWidgets() throws IOException, Exception
	{
		System.out.println("Executing Method No: 52");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("TelecomOrderEntry.atmx", "AAPlayer.exe");
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("TelecomOrderEntry");
		dataProfilePage.selectDataProfile();
		dataProfilePage.clickOnEditLink();
		
		//Act
		int row[] = {4, 6};
		String dispalyName[] ={"Na_@#$^*&%-12","Palak 123"}; 
		dataProfilePage.setDsiplayName(row[0],dispalyName[0]);
		dataProfilePage.setDsiplayName(row[1],dispalyName[1]);		
		dataProfilePage.clickOnSaveAndGenerateNewDashboard();
		//dataProfilePage.clickGenerateNewDashbord();
		baseClass.waitForThreadToLoad(10);
		baseClass.switchToChildFrame("dashboardContent");
				
		//Assert
		dashboardPage.checkTextOnWidget();
		baseClass.waitForThreadToLoad(5);
		String ageGroup = dashboardPage.getTextOnWidget();				
		dashboardPage.CheckTextonWidget1();		
		baseClass.waitForThreadToLoad(5);
		String category = dashboardPage.getTextOnWidget1();
		baseClass.switchToParentFrame();
		Assert.assertTrue(ageGroup.toLowerCase().contains(dispalyName[0].toLowerCase()));
		Assert.assertTrue(category.toLowerCase().contains(dispalyName[1].toLowerCase()));		
				
		
	}
	
	public void checkAddChartButton_ForSavedAsDashboard_VerifyChartsGettingLoaded() throws IOException, Exception
	{
		System.out.println("Executing Method No: 53");
		
		//Arrange	
		String dashboard = BaseClass.GetPropertyName("Dashboard");
		String publisheddb = BaseClass.GetPropertyName("Publisheddb");
		deleteSavedAsDashboard(dashboard);
		baseClass.executeTaskFromAAEClient("MonthlyCreditProcessing.atmx", "AAPlayer.exe");		
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("MonthlyCreditProcessing");
		dashboardPage.clickSaveAsButton();				
		dashboardPage.enterDataInPlaceHolder(dashboard);
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(5);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);
		
		//Act
		baseClass.switchToChildFrame("dashboardContent");
		String[] charts = {"Bars","Bars: Histogram","Bars: Multiple Metrics","Donut","Floating Bubbles","Heat Map","KPI","Line & Bars Trend","Line Trend: Attribute Values","Line Trend: Multiple Metrics","Packed Bubbles","Pie","Pivot Table","Raw Data Table","Scatter Plot","Tree Map","Word Cloud"};
		//String[] charts = {"Bars","Bars: Histogram"};
		for(int i=0; i<charts.length; i++){
			baseClass.waitForThreadToLoad(2);
			dashboardPage.clkAddChart();			
			String labelText = 	dashboardPage.getLabelText();
			dashboardPage.clkLabel();
			dashboardPage.clkCharts(charts, i);
			dashboardPage.clkLabelText(labelText);
			//dashboardPage.clkFullScreen(labelText);			
			dashboardPage.clkControInfo();
			dashboardPage.sendTexttoInfo(charts, i);
			dashboardPage.clkSaveButton();
			
		//Assert
		String chartName = dashboardPage.getChartName();
		baseClass.waitForThreadToLoad(2);
		Assert.assertEquals(chartName, charts[i]);			
		//dashboardPage.exitFullScreen(charts, i);			
			}
		baseClass.switchToParentFrame();
		dashboardPage.clkSaveButton1();
		baseClass.waitForThreadToLoad(2);
		analyzePage.clickOnAnalyze();
		//analyzePage.searchAndDeletePublishedDashboard(publisheddb);
		analyzePage.searchPublishedDashboard(publisheddb);
		analyzePage.deletePublishedDashboard(publisheddb);
		baseClass.waitForThreadToLoad(2);
		dashboardPage.clickOnConfigure();
		dashboardPage.searchAndSelectDashboard(dashboard);
		dashboardPage.clkPublishButton();
		dashboardPage.enterDataInPlaceHolder(publisheddb);
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(5);
		dashboardPage.clickOnAnalyze();
		dashboardPage.searchAndSelectDashboard(publisheddb);
		boolean flag = dashboardPage.VisibiltyofDashbaord();
		Assert.assertTrue(flag);
				
	}
	
	public void checkExportImages_ForUltronInvoiceProcessingTask_VerifyFilesGettingSaved() throws Exception
	{
		System.out.println("Executing Method No: 54");
		
		//Arrange
		String downloadFilePath = System.getProperty("user.dir")+"\\Images";
		String dashboard = BaseClass.GetPropertyName("Dashboard");
		baseClass.deleteFilesfromFolder(downloadFilePath);
		deleteSavedAsDashboard(dashboard);
		baseClass.executeTaskFromAAEClient("Ultron Invoice Processing.atmx", "AAPlayer.exe");
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("Ultron Invoice Processing");
		dashboardPage.clickSaveAsButton();				
		dashboardPage.enterDataInPlaceHolder(dashboard);
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(5);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);
				
		//Act
		baseClass.switchToChildFrame("dashboardContent");
		dashboardPage.clkOnExportButton();
		dashboardPage.expPngFormat();
		dashboardPage.clkPngFormat();
		
		//Assert
		baseClass.waitForThreadToLoad(5);	
		Assert.assertTrue(baseClass.isFileDownloaded(downloadFilePath, "screenshot.png"), "Failed to download Expected document");
		 
		dashboardPage.clkOnExportButton();
		dashboardPage.expPdfFormat();
		dashboardPage.headerText("Top");
		dashboardPage.footerText("Bottom");
		dashboardPage.clkPdfForamt();		
		baseClass.waitForThreadToLoad(5);
	    Assert.assertTrue(baseClass.isFileDownloaded(downloadFilePath, "SavedAsTask.pdf"), "Failed to download Expected document");
		
	    dashboardPage.clkBackButton();
	    //dashboardPage.expJsonFromat();
	    dashboardPage.clkCloseButton();
	    baseClass.deleteFilesfromFolder(downloadFilePath);	    
	    dashboardPage.expandWidget1();
	    dashboardPage.clkControlDownlaod();
	    dashboardPage.expPngFormat();
		dashboardPage.clkPngFormat();		
		baseClass.waitForThreadToLoad(5);	
		Assert.assertTrue(baseClass.isFileDownloaded(downloadFilePath, "screenshot.png"), "Failed to download Expected document");
		
		dashboardPage.expPdfFormat();
		dashboardPage.headerText("Top");
		dashboardPage.footerText("Bottom");
		dashboardPage.clkPdfForamt();		
		baseClass.waitForThreadToLoad(5);	
	    Assert.assertTrue(baseClass.isFileDownloaded(downloadFilePath, "SavedAsTask.pdf"), "Failed to download Expected document");
		
	    dashboardPage.clkBackButton();
	    dashboardPage.expXlsFormat();	    
	    baseClass.waitForThreadToLoad(5);
	   // Assert.assertTrue(baseClass.isFileDownloaded(downloadFilePath, "grid-chart.xlsx"), "Failed to download Expected document");
	    
	    dashboardPage.clkCloseButton();
	    dashboardPage.exitFullScreenWidget();	    
	    baseClass.switchToParentFrame();
	    //Desktop.getDesktop().open(new File(downloadFilePath));			
		
		
	}
	
	public void checkChart_ForStateAndZipTask_VerifyTitleOnWidgets() throws Exception{
		
		System.out.println("Executing Method No: 55");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("StateAndZipcode.atmx", "AAPlayer.exe");
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("StateAndZipcode");
		dataProfilePage.selectDataProfile();
		dataProfilePage.clickOnEditLink();
		
		//Act
		int row[] = {3, 4, 6};
		String dataType[] ={"Country Code","State Code","Zip Code"}; 
		dataProfilePage.selectDsiplayName(row[0],dataType[0]);
		dataProfilePage.selectDsiplayName(row[1],dataType[1]);
		dataProfilePage.selectDsiplayName(row[2],dataType[2]);	
		dataProfilePage.clickOnSaveAndGenerateNewDashboard();
		baseClass.waitForThreadToLoad(10);
		baseClass.switchToChildFrame("dashboardContent");
				
		//Assert
		dashboardPage.checkStateTextOnWidget();
		baseClass.waitForThreadToLoad(2);
		String State = dashboardPage.getStateTextOnWidget();
		dashboardPage.checkCountryTextOnWidget();
		baseClass.waitForThreadToLoad(2);
		String Country = dashboardPage.getCountryTextOnWidget();		
		String result = dashboardPage.getTransformOfDashboard();
		String refrenceString = "rgba(51, 51, 51, 1)";
		baseClass.switchToParentFrame();
		Assert.assertTrue(State.toLowerCase().contains(dataType[1].toLowerCase()));
		Assert.assertTrue(Country.toLowerCase().contains(dataType[0].toLowerCase()));
		Assert.assertEquals(result, refrenceString);		
				
		
	}
	
	public void VerifyDashboard_ForParentChildTask_ParentDoesNotHaveNumeric() throws Exception{
		
		System.out.println("Executing Method No: 56");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("ParentChild_DashboardCase.atmx", "AAPlayer.exe");		
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(2);
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("ParentChild_DashboardCase");		
		boolean flag = dashboardPage.VisibiltyofDashbaord();
		Assert.assertTrue(flag);
		
	}

}
