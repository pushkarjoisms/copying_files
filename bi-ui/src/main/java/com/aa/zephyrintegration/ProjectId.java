package com.aa.zephyrintegration;

import org.json.JSONObject;
public class ProjectId {

	public String get() throws Exception
	{
		HttpHelper request = new HttpHelper();
		HttpResult response = request.getJiraId(null);

		if (response.getStatusCode()>= 200 && response.getStatusCode() < 300) {
			JSONObject projectinfo= new JSONObject(response.getResponse());
			String projectid = (String) projectinfo.get("id");
			return projectid;
		}
		return null;
	}
}
