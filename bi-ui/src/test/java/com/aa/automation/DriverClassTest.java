package com.aa.automation;

import com.aa.common.AAEClient;
import com.aa.common.ControlRoomSetup;
import com.aa.common.Utility;
import com.aa.zephyrintegration.HttpHelper;
import com.aa.zephyrintegration.UpdateExecutionStatusToJira;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.apache.log4j.Logger;
import java.lang.reflect.Method;

public class DriverClassTest {
  final static Logger log = Logger.getLogger(Test.class);
  WebDriver driver;
  WebDriverWait wait;
  BaseClass baseClass;
  LoginTest botInsightLoginTest;
  DataProfileTest botInsightDataProfileTest;
  DashboardTest botInsightDashboardTest;
  AnalyzeTest botInsightAnalyzeTest;
  LogoutTest botInsightLogoutTest;
  HttpHelper httpHelper;
  DataProfilePage dataProfilePage;
  DashboardPage dashboardPage;
  LoginPage loginPage;
  OperationAnalyticsTest botinsightOperationAnalyticsTest;
  RolesTest rolesTest;
  private final String BI_USERNAME = "biadmin";
  private final String BI_PASSWORD = "W0nderland";
  private AAEClient aaClient = AAEClient.getInstance();
  private Utility util = Utility.getInstance();

  String aaeApplicationPath = "C:\\Users\\" + System.getProperty("user.name") + "\\Documents\\Automation Anywhere Files";
  String aaeMyTaskFolderPath = aaeApplicationPath+"\\Automation Anywhere\\My Tasks\\resources";
  String aaeMyMetabotFolderPath = aaeApplicationPath+"\\Automation Anywhere\\";
  String aaeMyDocsFolderPath = aaeApplicationPath+"\\Automation Anywhere\\My Docs\\";
  String projectRepository = System.getProperty("user.dir")+"\\src\\test\\resources";

  @BeforeClass(alwaysRun=true)
  public void setup() throws Exception{
    // Setup control-room
    ControlRoomSetup.setup();

    // Get the instance of the utility and copy the taskbots
    util.copyTaskBots();

		// Start the AAE Client
    aaClient.loginToClient(BI_USERNAME, BI_PASSWORD, true);

    baseClass = new BaseClass(driver);
    String browserName = BaseClass.GetPropertyName("Browser");
		String processName = BaseClass.GetPropertyName("Process");
		baseClass.killProcess(processName);
		driver = baseClass.launchBrowser(browserName);
    String URL = util.getHost() + ":80";
		baseClass.enterURLIntoBrowser(URL);
		wait = baseClass.waitForElementToBeLoad(driver);

		baseClass = new BaseClass(driver, wait);
		dataProfilePage = new DataProfilePage(driver, wait);
		dashboardPage = new DashboardPage(driver, wait);
		botInsightLoginTest = new LoginTest(driver, wait);
		botInsightDataProfileTest = new DataProfileTest(driver, wait);
		botInsightDashboardTest = new DashboardTest(driver, wait);
		botInsightAnalyzeTest = new AnalyzeTest(driver, wait);
		botInsightLogoutTest = new LogoutTest(driver, wait);
		botinsightOperationAnalyticsTest = new OperationAnalyticsTest(driver, wait);
		rolesTest = new RolesTest(driver, wait);
 }
  /*
  @Test(priority=1, groups = {"sanity"})
	//@CustomAnnotationTofetchIssueId(value = 21928)
	public void clearDaashboardDataFromDatabaseAndZoomData() throws Exception
	{
		log.info("\nExecuting Method No: 1");

		String arr[] = {"17022017","AN7_1","Blank_Variables","ExitLoop_at_Counter10","OnlyNumericDataType","OnlyStringDataType","ReadFromText_with_Header","ReadFromText_without_Header","TelecomOrderEntry","ParentTask","ChildTask"};
		for (int i=0; i<arr.length; i++){
		httpHelper.deleteStandardDashboard(arr[i]);
		}

		httpHelper.deleteSavedAsAndPublishedDashboard(BaseClass.GetPropertyName("Dashboard"),BaseClass.GetPropertyName("zoomDevCredentials"));
		httpHelper.deleteSavedAsAndPublishedDashboard(BaseClass.GetPropertyName("Publisheddb"),
    BaseClass.GetPropertyName("zoomProdCredentials"));
	}
  */

	@Test(priority=2, groups = {"sanity"})
	public void LogintoBotInsight() throws Exception
	{
		//botInsightLoginTest.loginToControlRoom_WithValidCredentials();
		//rolesTest.CreateRoleAndUser_AssignedRoleToUser_ForBotInsight();
		botInsightLoginTest.loginToBotInsight_ValidCredentials_VerifyLoginSuccessfully();
	}

	// AN-1288
  @Test(priority=3,enabled=true, groups = {"sanity"})
  public void CheckDataProfile() throws Exception
  {
      botInsightDataProfileTest.taskNameLable_OnExistingDataProfile_IsVisible();
  }

  // AN-1287
  @Test(priority=4,enabled=true)
  public void CheckDashboard() throws Exception
  {
      botInsightDashboardTest.verifyBoomarkItemInsertedSuccessfully();
      botInsightDashboardTest.delete_RemovesSavedDashboard_Successfully();
  }

  // AN-1289
  @Test(priority=5,enabled=true, groups = {"sanity"})
  public void CheckAnalyzeSection() throws Exception, Exception
  {
      botInsightAnalyzeTest.analyzeDashboard();
  }

  // AN-1290
  @Test(priority=6,enabled=true)
  public void CheckDataProfileforTaskhasSubtasks() throws Exception
  {
      botInsightDataProfileTest.taskNameLableForDepedencyTask_OnExistingDataProfile_IsVisible();
  }

  // AN-1291
  @Test(priority=7,enabled=true)
  public void VerifyBookmarkAndRemovedSavedDashboard() throws Exception
  {
      botInsightDashboardTest.verifyBoomarkItemInsertedSuccessfullyforParenTask();
      botInsightDashboardTest.delete_RemovesSavedDashboard_Successfully();
  }

  // AN-1292
  @Test(priority=8,enabled=true)
  public void CheckAnalyzeforTaskhasSubtasks() throws Exception	{
      botInsightAnalyzeTest.analyzeDashboardforParentTask();
  }

  // AN-1293
  @Test(priority=9,enabled=true)
  public void DataProfileNewVariableCheck() throws Exception
  {
      botInsightDataProfileTest.verifyNewlyExecutedTask();
      botInsightDashboardTest.verifyDashboardSaved();
  }

  // AN-1294
  @Test(priority=10,enabled=true)
  public void DashboardNewVariableCheck() throws Exception
  {
      botInsightDataProfileTest.reExecutedTask_AfterRenamingAnalyticsVariable_ReflectsChangesOnDataProfile();
      botInsightDashboardTest.verifyNewAndRenamedVariableOnDashboard();
  }

  // AN-1295
  @Test(priority=11,enabled=true, groups = {"sanity"})
  public void clickPreviewData_ForExistingDataProfile_DisplayCorrectValuesInNewlyOpenedTab() throws Exception
  {
      botInsightDataProfileTest.clickPreviewData_ForExistingDataProfile_DisplayCorrectValuesInNewlyOpenedTab();
  }

  /* Timestamp issue
  // AN-1296
  @Test(priority=12,enabled=true)
  public void executedTask_generateDataProfileTable_WithCorrectValues() throws Exception
  {
      botInsightDataProfileTest.executedTask_GenerateDataProfileTable_WithCorrectValues();
  }
  */

  // AN-1297
  @Test(priority=13,enabled=true)
  public void executedTask_LogginNullValueForAnalyticVariable_IsNotSeenOnDataProfile() throws Exception
  {
     botInsightDataProfileTest.executedTask_LogginNullValueForAnalyticVariable_IsNotSeenOnDataProfile();;
  }

  // AN-1298
  @Test(priority=14,enabled=true)
  public void clickSave_ForUpdatedDataProfileTable_RetainsChanges() throws Exception
  {
      botInsightDataProfileTest.clickSave_ForUpdatedDataProfileTable_RetainsChanges();
  }

  // AN-1299
  @Test(priority=15,enabled=true)
  public void fields_InDataProfileTable_IsNotEditable() throws Exception
  {
      botInsightDataProfileTest.fields_InDataProfileTable_IsNotEditable();
  }

  // AN-1300
  @Test(priority=16,enabled=true)
  public void clickEdit_OnDataProfile_MakeDataProfileEditable() throws Exception
  {
      botInsightDataProfileTest.clickEdit_OnDataProfile_MakeDataProfileEditable();
  }

  // AN-1301
  @Test(priority=17,enabled=true)
  public void listItems_InDataTypeDropDown_IsPresent() throws Exception
  {
      botInsightDataProfileTest.listItems_InDataTypeDropDown_IsPresent();
  }

  // AN-1302
  @Test(priority=18,enabled=true)
  public void dataProfile_PreviewDataButton_IsPresent() throws Exception
  {
      botInsightDataProfileTest.dataProfile_PreviewDataButton_IsPresent();
  }

  // AN-1335
  @Test(priority=19,enabled=true)
  public void editAndGenerateNewDashboard_OnDataProfile_IsAvailableTrue() throws Exception
  {
      botInsightDataProfileTest.editAndGenerateNewDashboardLinks_OnDataProfile_IsAvailableTrue();
  }

  // AN-1303
  @Test(priority=20,enabled=true)
  public void check_InformationMessage_ToSearchTaskToLoadDataProfile() throws Exception
  {
      botInsightDataProfileTest.check_InformationMessage_ToSearchTaskToLoadDataProfile();
  }

  // AN-1304
  @Test(priority=21,enabled=true)
  public void clickCompareOnDashboard_OpensAdjecentScreen_ForSearcingDashboardForCompare() throws Exception
  {
      botInsightDashboardTest.clickCompare_OpensAdjecentScreen_ForSearcingDashboardForCompare();
  }

  // AN-1305
  @Test(priority=22,enabled=true)
  public void searchButton_loadsDashboard_OnCompareScreen() throws Exception
  {
      botInsightDashboardTest.searchButton_loadsDashboard_OnCompareScreen();
  }

  // AN-1306
  @Test(priority=23,enabled=true, groups = {"sanity"})
  public void dashboardUI_Shows_AllDefaulsLinks() throws Exception
  {
      botInsightDashboardTest.dashboardUI_Shows_AllDefaulsLinks();
  }

  // AN-1307
  @Test(priority=24,enabled=true)
  public void clickDelete_RemoveBookmarkedDashboard_FromBookmarkList() throws Exception
  {
      botInsightDashboardTest.clickDelete_RemoveBookmarkedDashboard_FromBookmarkList();
  }

  // AN-1308
  @Test(priority=25,enabled=true)
  public void TrySaving_DuplicateDashboard_GivesErrorMessage() throws Exception
  {
      botInsightDashboardTest.TrySaving_DuplicateDashboard_GivesErrorMessage();
  }

  // AN-1309
  @Test(priority=26,enabled=true)
  public void executedTask_ReadFromTextWithHeaderCommand_LogsAllVariablesWithStringDataType() throws Exception
  {
      botInsightDataProfileTest.executedTask_ReadFromTextWithHeaderCommand_LogsAllVariablesWithStringDataType();
  }

  // AN-1310
  @Test(priority=27,enabled=true)
  public void executedTask_ExitsLoopAtCounter10_LogsAllVariablesWithCorrectData() throws Exception
  {
      botInsightDataProfileTest.executedTask_ExitsLoopAtCounter10_LogsAllVariablesWithCorrectData();
  }

  // AN-1311
  @Test(priority=28,enabled=true)
  public void saveDataProfile_bySettingAllVariableDataTypeAsString_GiveInformationMessage() throws Exception
  {
      botInsightDataProfileTest.saveDataProfile_bySettingAllVariableDataTypeAsString_GiveInformationMessage();
  }

  /* TODO : run when timestamp list is fixed
  // AN-1312
  @Test(priority=29,enabled=true)
  public void dataProfile_OnlyNumericDataType_ShowsMinMaxAvgSumValues() throws Exception
  {
      botInsightDataProfileTest.dataProfile_OnlyNumericDataType_ShowsMinMaxAvgSumValues();
  }
  */

  // AN-1313
  @Test(priority=30,enabled=true)
  public void fontColor_OfStandardDashboard_IsOrange() throws Exception
  {
      botInsightDataProfileTest.fontColor_OfStandardDashboard_IsOrange();
  }

  // AN-1314
  @Test(priority=31,enabled=true)
  public void fontColor_OfCustomDashboard_IsBlack() throws Exception
  {
      botInsightDataProfileTest.fontColor_OfCustomDashboard_IsBlack();
  }

  // AN-1315
  @Test(priority=32,enabled=true)
  public void clickAnalyzeButtonFromAAEClientEditor_ForUpdatedDataProfileTable_CleansUpChangesBackToOriginal() throws Exception
  {
      botInsightDataProfileTest.clickAnalyzeButtonFromAAEClientEditor_ForUpdatedDataProfileTable_CleansUpChangesBackToOriginal();
  }

  // AN-1316
  @Test(priority=33,enabled=true)
  public void clickAnalyzeButton_ForOnlyStringVariableTask_VerifyDashboardNotCreated() throws Exception
  {
      botInsightDataProfileTest.clickAnalyzeButton_ForOnlyStringVariableTask_VerifyDashboardIsCreated();
  }

  // AN-1317
  @Test(priority=34,enabled=true, groups = {"sanity"})
  public void clickAnalyzeButton_ForOnlyNumericVariableTask_VerifyDashboardCreated() throws Exception
  {
      botInsightDataProfileTest.clickAnalyzeButton_ForOnlyNumericVariableTask_VerifyDashboardCreated();
  }

  // AN-1318
  @Test(priority=35,enabled=true)
  public void deleteWidget_Removes_WidgetSuccessfully() throws Exception
  {
      botInsightDashboardTest.deleteWidget_Removes_WidgetSuccessfully();
  }

  // AN-1319
  @Test(priority=36,enabled=true)
  public void tryPublish_DuplicateDashboard_GivesErrorMessage() throws Exception
  {
      botInsightDashboardTest.tryPublish_DuplicateDashboard_GivesErrorMessage();
  }

  // AN-1320
  @Test(priority=37,enabled=true)
  public void saveAs_Includes_DeletedWidget() throws Exception
  {
      botInsightDashboardTest.saveAs_Includes_DeletedWidget();
  }

  // AN-1322
  @Test(priority=38,enabled=true)
  public void save_DoesNotInclude_DeletedWidget() throws Exception
  {
      botInsightDashboardTest.save_DoesNotInclude_DeletedWidget();
  }

  // AN-1323
  @Test(priority=39,enabled=true)
  public void saveDashboard_WithNameHasOnlyOneCharacter_ThrowsValidationMessage() throws Exception
  {
      botInsightDashboardTest.saveDashboard_WithNameHasOnlyOneCharacter_ThrowsValidationMessage();
  }

  // AN-1324
  @Test(priority=40,enabled=true)
  public void resize_Widget_ExpandAndCollpaseWidgetSuccessfully() throws Exception
  {
      botInsightDashboardTest.resize_Widget_ExpandAndCollpaseWidgetSuccessfully();
  }

  // AN-1325
  @Test(priority=41,enabled=true)
  public void dashbordUI_ShowsInformationText_ToSearchDashboard() throws Exception
  {
      botInsightDashboardTest.dashbordUI_ShowsInforamtionText_ToSearchDashboard();
  }

  // AN-1326
  @Test(priority=42,enabled=true)
  public void dashbordSearched_loadsAllDefaultWidgets_Succesfully() throws Exception
  {
      botInsightDashboardTest.dashbordSearched_loadsAllDefaultWidgets_Succesfully();
  }

  /* TODO : run when timestamp list is fixed
  // AN-1327
  @Test(priority=43,enabled=true)
  public void dataProfileTable_ShowsCorrectValues_AfterTraversingFromAnalyzeToDataProfile() throws Exception
  {
      botInsightDataProfileTest.dataProfileTable_ShowsCorrectValues_AfterTraversingFromAnalyzeToDataProfile();
  }
  */

  // AN-1328
  @Test(priority=44,enabled=true)
  public void linksAvailable_OnExpandedWidget_OfDashboard() throws Exception
  {
      botInsightDashboardTest.exportlinkBehaviour_OnExpandedWidget_OfDashboard();
  }

  // AN-1329
  @Test(priority=45,enabled=true)
  public void clickGenerateNewDashboardLink_LoadsDashboard_WithModifiedDataProfile() throws Exception
  {
      botInsightDataProfileTest.clickGenerateNewDashboardLink_LoadsDashboard_WithModifiedDataProfile();
  }

  // AN-1330
  /* 46, 47 are related to maps and tests need to be fixed
  @Test(priority=46,enabled=true)
  public void clickCompare_OpensAdjacentScreen_ForSearchingDashboardForCompare() throws Exception
  {
      botInsightAnalyzeTest.clickCompare_OpensAdjecentScreen_ForSearcingDashboardForCompare();
  }

  // AN-1331
  @Test(priority=47,enabled=true)
  public void resize_PublishedDashboardWidget_ExpandAndCollpaseWidgetSuccessfully() throws Exception
  {
      botInsightAnalyzeTest.resize_PublishedDashboardWidget_ExpandAndCollpaseWidgetSuccessfully();
  }
  */

  // AN-1332
  @Test(priority=48,enabled=true)
  public void clickDefaultDashboardLink_FromSavedAsDashboard_LoadsOriginalDashboard() throws Exception
  {
      botInsightDataProfileTest.clickDefaultDashboardLink_FromSavedAsDashboard_LoadsDashboard();
  }

  /* TODO: enable when parent-child issue is fixed
  // AN-1333
  @Test(priority=49,enabled=true)
  public void clickRankButton_ForParenChildTask_VerifyRankTableLoaded() throws Exception
  {
      botInsightDataProfileTest.clickRankButton_ForParenChildTask_VerifyRankTableLoaded();
  }
  */

  /* TODO :- bug on state, city maps
  // AN-1334
  @Test(priority=50,enabled=true)
  public void checkStateCityChart_ForMortgageTask_VerifyNewlyAddedChartLoaded() throws Exception
  {
      botInsightDashboardTest.checkStateCityChart_ForMortgageTask_VerifyNewlyAddedChartLoaded();
  }
  */

/* // TODO :- needs to fix these tests
  @Test(priority=51)
  public void checkDashboardWidgetLevelFilter_ForMonthlyProcessingTask_VerifyAddedFilters() throws Exception
  {
      botInsightDashboardTest.checkDashboardWidgetLevelFilter_ForMonthlyProcessingTask_VerifyAddedFilters();
  }

  @Test(priority=52)
  public void checkDisplayName_ForTelecomOrderEntryTask_VerifyTitleonWidgets() throws Exception
  {
      botInsightDashboardTest.checkDisplayName_ForTelecomOrderEntryTask_VerifyTitleonWidgets();
  }

  @Test(priority=53)
  public void checkAddChartButton_ForSavedAsDashboard_VerifyChartsGettingLoaded() throws Exception
  {
      botInsightDashboardTest.checkAddChartButton_ForSavedAsDashboard_VerifyChartsGettingLoaded();
  }

  @Test(priority=54)
  public void checkExportImages_ForUltronInvoiceProcessingTask_VerifyFilesGettingSaved() throws Exception
  {
      botInsightDashboardTest.checkExportImages_ForUltronInvoiceProcessingTask_VerifyFilesGettingSaved();
  }

  @Test(priority=55)
  public void checkChart_ForStateAndZipTask_VerifyTitleOnWidgets() throws Exception
  {
      botInsightDashboardTest.checkChart_ForStateAndZipTask_VerifyTitleOnWidgets();
  }

  @Test(priority=56)
  public void VerifyDashboard_ForParentChildTask_ParentDoesNotHaveNumeric() throws Exception
  {
      botInsightDashboardTest.VerifyDashboard_ForParentChildTask_ParentDoesNotHaveNumeric();
  }

  @Test(priority=57)
  public void VarifyDataProfile_ForParentChildTask_IfChildTaskHasFails() throws Exception
  {
      botInsightDataProfileTest.VarifyDataProfile_ForParentChildTask_IfChildTaskHasFails();
  }

  @Test(priority=58)
  public void publishedDashboard_GetsUpdated_WithProductionData() throws Exception
  {
      botInsightDashboardTest.publishedDashboard_GetsUpdated_WithProductionData();
  }

  @Test(priority=59)
  public void checkAnalyticsExpert_DoesNot_SeeAnalyzeSection() throws Exception
  {
      botInsightAnalyzeTest.checkAnalyticsExpert_DoesNot_SeeAnalyzeSection();
  }

  @Test(priority=60)
  public void checkAnalyticsConsumer_DoesNot_SeeConfigureSection() throws Exception
  {
      botInsightAnalyzeTest.checkAnalyticsConsumer_DoesNot_SeeConfigureSection();
  }

  @Test(priority=61)
  public void CreateRoleAndUser_ForBotInsight_RoleBasedAccess() throws Exception
  {
      botInsightDashboardTest.CreateRoleAndUser_ForBotInsight_RoleBasedAccess();
  }

  //CR11.0
  @Test(priority=62, groups = { "Sanitytest"})
  public void checkOperationAnalytics_Dashboardsloading_Successfully() throws Exception
  {
      botinsightOperationAnalyticsTest.OperationAnalytics_Dashboardsloading_Successfully();
  }

  @Test(priority=63, groups = { "Sanitytest" })
  public void SeccessfulLogout() throws Exception
  {
      botInsightLogoutTest.logout_FromBotInsight_VerifySuccessfulLogout();
      //botInsightLogoutTest.logoutFromControlRoom();
  }
*/
	@AfterMethod(alwaysRun=true)
	public void tearDown(ITestResult testResult) throws Exception {

		String methodName = testResult.getMethod().getMethodName();
		Method method  = this.getClass().getMethod(methodName);
		String testMethodName = method.getName();
		int methodExecutionStatus = testResult.getStatus();
		if (methodExecutionStatus == 1){
			log.info(testMethodName + " : Executed\n");
		}else{
			log.info(testMethodName + " : Not Executed\n");
		}

		UpdateExecutionStatusToJira updateTestCaseExecutionStatusToZephyrForJira = new UpdateExecutionStatusToJira();
		updateTestCaseExecutionStatusToZephyrForJira.update(testMethodName, methodExecutionStatus);
	}

	@AfterClass(alwaysRun=true)
	public void tearDown()
	{
		driver.quit();
	}
}
