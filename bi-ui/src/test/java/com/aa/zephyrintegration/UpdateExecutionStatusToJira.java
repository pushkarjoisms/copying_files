package com.aa.zephyrintegration;

public class UpdateExecutionStatusToJira {
	
	public void update(String methodName, int methodExecutionStatus) throws Exception
	{
	String projectId = new ProjectId().get();
	String versionId = new VersionId().get();
	String cycleId = new CycleId().get(projectId,versionId);
	long issueId = new TestCaseId().get(cycleId, projectId, versionId, methodName);
	String executionId = new ExecutionId().get(issueId, cycleId, projectId, versionId);
	new ExecutionStatus().update(issueId, executionId, methodExecutionStatus,projectId);
	}
}
