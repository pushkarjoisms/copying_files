package com.thed.zephyr;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TimeConverter {

	
	public static void main(String args[]) throws ParseException
	{
		//long unixSeconds = 1490076517404L;
		//Date date = new Date(unixSeconds); // *1000 is to convert seconds to milliseconds
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS z"); // the format of your date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
			//sdf.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone reference for formating (see comment at the bottom
		//String formattedDate = sdf.format(date);
		//System.out.println(formattedDate);
		long unixtime = sdf.parse("2017-03-21").getTime();
		System.out.println(unixtime);
		
	}
}
