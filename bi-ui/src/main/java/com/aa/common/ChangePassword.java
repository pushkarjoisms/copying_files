package com.aa.common;

import java.util.List;

public class ChangePassword {
  private String oldPassword;
  private String password;
  private List<Questionaires> questionsAnswers;

  public String getOldPassword() {
    return oldPassword;
  }

  public void setOldPassword(String oldPassword) {
    this.oldPassword = oldPassword;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<Questionaires> getQuestionsAnswers() {
    return questionsAnswers;
  }

  public void setQuestionsAnswers(List<Questionaires> questionsAnswers) {
    this.questionsAnswers = questionsAnswers;
  }
}
