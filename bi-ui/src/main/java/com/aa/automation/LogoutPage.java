package com.aa.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogoutPage {
	
	WebDriver driver;
	WebDriverWait wait;
	BaseClass baseClass;
	WebElement element;
	
	By Usericon = By.xpath("//img[contains(@src, 'assets/images/user.png')]");
	By Logout = By.xpath("//button[text()='Log out']");

	public LogoutPage(WebDriver driver, WebDriverWait wait) throws Exception{
		this.driver = driver;
		wait = new WebDriverWait(driver,60);
		this.wait = wait;
		baseClass = new BaseClass(driver,wait);
	}

	public void logout() throws Exception
	{	
		try{
			element = driver.findElement(Usericon);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(Logout);
			baseClass.clickOnWebElement(element);
			}catch(Exception e){
				System.out.println("Error in logout Method: "+e.getStackTrace());
			}
	}
	
	public void clickOnHeaderBarTrayButton() {
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//button[@class= 'headerbar-tray-option-button headerbar-tray-option--labeled']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}
	
	public void clickOnLogOutButton() {
		element = driver.findElement(By.xpath("//button[@class='commandbutton-button commandbutton-button--clickable']/div"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}
	
}
