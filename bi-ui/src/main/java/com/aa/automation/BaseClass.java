package com.aa.automation;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.os.WindowsUtils;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseClass {
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement element;
	Actions action;
	Process process;
	JavascriptExecutor executor;
	static Properties properties; 
	
	String installationPath= WindowsUtils.readStringRegistryValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Automation Anywhere\\AAE\\AAEClient\\Path");
	String applicationPath = "C:\\Users\\" + System.getProperty("user.name") + "\\Documents\\Automation Anywhere Files";
	String resourceFolderPath = applicationPath+"\\Automation Anywhere\\My Tasks\\resources\\Tasks";
	String resourceFolderPath1 = applicationPath+"\\Automation Anywhere\\My Tasks\\resources";
	String projectRepository = System.getProperty("user.dir")+"\\src\\test\\resources\\Tasks";
	String projectRepository1 = System.getProperty("user.dir")+"\\src\\test\\resources";
		
	static{
		properties = new Properties();
		//InputStream inputStream = BaseClass.class.getClassLoader().getResourceAsStream("datafile.properties");
		InputStream inputStream = BaseClass.class.getClassLoader().getResourceAsStream("otherdatafile.properties");
		try {
			properties.load(inputStream);
			System.out.println("Load property from Properties file: "+inputStream);
		} catch (IOException e) {
			System.out.println("Unable to Load property from Properties file: "+e.getStackTrace());
		}
	}
	
	public static String GetPropertyName(String name) throws IOException
	{
		return (String)properties.get(name);
	}
	
	public BaseClass(WebDriver driver) throws Exception {
		
		this.driver= driver;		
	}
	
	public BaseClass(WebDriver driver, WebDriverWait wait) throws Exception {

		this.driver= driver;
		wait = new WebDriverWait(driver,60);
		this.wait = wait;
		action = new Actions(driver);
		executor = ((JavascriptExecutor)driver);
	}
	
	@SuppressWarnings("null")
	public void getResourceAndProjectFolderPaths() 
	{
		String variable = "sourceFolder="+resourceFolderPath+"\n projectRepo="+projectRepository+"\n projectRepo1="+projectRepository1;
		logTextToFile(System.getProperty("java.io.tmpdir")+"\\folders.txt", variable);
	}
	
	public void copyTheFolderFromSourceToDestination(String sourceLocation, String targetLocation) {
		try { 
			getResourceAndProjectFolderPaths();
			File source = new File(sourceLocation);
			File target = new File(targetLocation);
			deleteFilefromFolder(sourceLocation+"\\FileInfo.dat");
	        FileUtils.copyDirectory(source, target);    		
	        	System.out.println("Copy the Folder from "+source+" To "+target);
			} catch (IOException e) {
				System.out.println("Unable to copy the folder from Source to Destination: "+e.getStackTrace());
			}
	}
	
	public void deleteFilefromFolder(String filePath) {		
		File file = new File(filePath);		                  	
		     if(file.exists()) {
		        file.delete();
		           System.out.println("File deleted Successfully: "+file);
		           }
		      }	  
			
	public void deleteFilesfromFolder(String filesPath) {		
		File dir = new File(filesPath);
		File[] files = dir.listFiles();
		    for(File file: files) {	        	
		        if(file.exists()) {
		           file.delete();
		            System.out.println("File deleted Successfully: "+file);
		            }
		       }	  
		}	
	
	public boolean isFileDownloaded(String downloadPath, String fileName) {
		boolean flag = false;
	    File dir = new File(downloadPath);
	    File[] files = dir.listFiles();	      
	    	for (int i = 0; i < files.length; i++) {
	    		if (files[i].getName().equals(fileName))
	            return flag=true;
	           }
	    return flag;
	}

	public void copyTheFileFromSourceToDestination (String sourceLocation, String targetLocation){		  
		try {
			getResourceAndProjectFolderPaths();
			Path source = Paths.get(projectRepository+"\\"+sourceLocation);
			Path target = Paths.get(resourceFolderPath+"\\"+targetLocation);
			Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
				System.out.println("Copy the Files from "+source+" To "+target);	
			} catch (IOException e) {
				System.out.println("Unable to copy the files from Source to Destination: " +e.getStackTrace());
			}			
		}
	
	public void copyTheFileFromSourceToDestination1 (String sourceLocation, String targetLocation){		  
		try {
			getResourceAndProjectFolderPaths();
			Path source = Paths.get(projectRepository+"\\"+sourceLocation);
			Path target = Paths.get(resourceFolderPath1+"\\"+targetLocation);
			Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
				System.out.println("Copy the Files from "+source+" To "+target);	
			} catch (IOException e) {
				System.out.println("Unable to copy the files from Source to Destination: " +e.getStackTrace());
			}
			
		}
	
	public void executeTaskFromAAEClient(String taskName,String aaPlayer) throws Exception {
		try {
			String TaskOperation = (resourceFolderPath+"\\"+taskName+"");
			String[] cmdArrayrun = new String[2];
			cmdArrayrun[0]=installationPath+"\\"+aaPlayer;
			cmdArrayrun[1]="/f"+TaskOperation+"/e";
			process= Runtime.getRuntime().exec(cmdArrayrun,null);
			process.waitFor();
				System.out.println("Task has been executed Successfully: "+taskName);
			} catch (IOException e) {
				System.out.println("Unable to execute the task from AAE Client: " +e.getStackTrace());
			}
		}
	
	public void executeTaskFromAAEClient1(String taskName,String aaPlayer) throws Exception {
		try {
			String TaskOperation = (resourceFolderPath1+"\\"+taskName+"");
			String[] cmdArrayrun = new String[2];
			cmdArrayrun[0]=installationPath+"\\"+aaPlayer;
			cmdArrayrun[1]="/f"+TaskOperation+"/e";
			process= Runtime.getRuntime().exec(cmdArrayrun,null);
			process.waitFor();
				System.out.println("Task has been executed Successfully: "+taskName);
			} catch (IOException e) {
				System.out.println("Unable to execute the task from AAE Client: " +e.getStackTrace());
			}
		}
	
		public void killProcess(String serviceName) {
			String TASKLIST = "tasklist";
			String KILL = "taskkill /F /IM ";
			try{
				Process p = Runtime.getRuntime().exec(TASKLIST);
				BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line;
				while ((line = reader.readLine()) != null) {
					if (line.contains(serviceName)) {
						Runtime.getRuntime().exec(KILL + serviceName);
				 	}
			 	}
			 		System.out.println("Process has been Killed: "+serviceName);
				}catch(Exception e){
					System.out.println("Unable to kill the Process: "+e.getStackTrace());
			}

		 }
	
	public WebDriver launchBrowser(String browser) {
		try{		
		 if (browser.equalsIgnoreCase("Chrome")) {
			//To Choose Chrome			
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			String downloadFilePath = System.getProperty("user.dir")+"\\Images";					
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.default_directory", downloadFilePath);
				
			ChromeOptions options = new ChromeOptions();		
			options.addArguments("disable-infobars"); 
			options.setExperimentalOption("prefs", chromePrefs);		
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			driver = new ChromeDriver(capabilities);
				
		} else if (browser.equalsIgnoreCase("IE")) {
			//To Choose Internet Explorer
			System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability("ignoreZoomSetting", true);
			driver = new InternetExplorerDriver(capabilities);			
				
		} else if (browser.equalsIgnoreCase("Firefox")) {
			//To Choose Firefox
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");						
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("marionette", true);
			driver =  new FirefoxDriver(capabilities);

		} else {

			System.out.println("Browser is not correct");
		}		 
				System.out.println("Browser has been launched Successfully: "+browser);			
			}catch(Exception e){
				System.out.println("Unable to launch the Browser: "+e.getStackTrace());
		}
			
			return driver;
	}
	
	public void enterURLIntoBrowser(String url){		
		try{
			//driver.manage().deleteAllCookies();
		 	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.get(url);
			driver.manage().window().maximize();
				System.out.println("Entered URL into Browser: "+url);	
			}catch(Exception e){
				System.out.println("Unable to enter the URL into Browser: "+e.getStackTrace());
			}
		}
	
	public WebDriverWait waitForElementToBeLoad(WebDriver driver){
		try{
			wait = new WebDriverWait(driver,2);
			}catch(Exception e){
				System.out.println("Unable to wait for to Load Element: "+e.getStackTrace());
		}
		return wait;		
	}
	
	public void waitForElementToBeClickable(WebElement element) {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));	
				System.out.println("Waited for element to be Click: " +element);		
			} catch (Exception e) {
				System.out.println("Element " + element + " was not waited for Clickable: " +e.getStackTrace());			
			}	
		}
	
	public void waitForElementToBePresent(WebElement element) {
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
				System.out.println("Waited for element to be Visible: " +element);		
			} catch (Exception e) {
				System.out.println("Element " + element + " was not waited for Visible: " +e.getStackTrace());
			}
		}
	
	public boolean waitForFrameToBeAvailableAndSwitchToIt(String frame) {
		try {
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));
				System.out.println("Waited for frame to be Available and Switch to it: " +frame);
			} catch(Exception e) {
				System.out.println("Frame " + frame + " was not available for Switch to it: " +e.getStackTrace ());	
				return false;
		}	
		return true;
	}
	
	public int waitForThreadToLoad(int seconds){	       
		try {
			Thread.sleep(seconds * 1000);
				System.out.println("Current thread is waited for "+ seconds +" Seconds");			
			}catch(Exception e){
				System.out.println("Current thread is not waited for specific period of Time: " +e.getStackTrace());		
			}
			return seconds;
		}

	public void printMessageOnMessageBox(String message){	       
		try {
			JOptionPane.showMessageDialog(null, message);
				System.out.println("Printed message on Message Box: " +message);			
			}catch(Exception e){
				System.out.println("Message was not printed on Message Box: " +e.getStackTrace());		
			}		
		}
	
	public void refreshThePage(){		
		try {
			driver.navigate().refresh();
				System.out.println("Page has been Refreshed");			
			}catch(Exception e){
				System.out.println("Unable to do the Page refresh: " +e.getStackTrace());		
			}		
		}
	
	public void clickOnWebElement(WebElement element) {
		try {
			action.click(element).build().perform();
				System.out.println("Clicked on Web element: " +element);		
			} catch (Exception e) {
				System.out.println("Element " + element + " was not Clickable: " +e.getStackTrace());
			}
		}
	
	
	public void enterDataIntoEditbox(WebElement element, String text) {
		try {						        	
		        element.clear();
		        //element.sendKeys(text);
				action.sendKeys(element, text).build().perform();
		        //action.click(element).build().perform();
		        //action.sendKeys(Keys.chord(Keys.CONTROL,"a"));
		        //action.sendKeys(Keys.BACK_SPACE);
		        //action.sendKeys(element, text).build().perform();			        	
				System.out.println("Entered data into Editbox: " +element);     			
			} catch (Exception e) {
				System.out.println("Element " + element + " was not entered data into Editbox: " +e.getStackTrace() ); 
			}
		}
	
	public void moveToWebElementandClick(WebElement element) {
		try {
			action.moveToElement(element).click().build().perform();
			System.out.println("Moved to web element and Click: " +element);	
			} catch (Exception e) {
				System.out.println("Element " + element + " was not clickable: " + e.getStackTrace());
				}
			}
	
	public void mouseHoverEventToElemnt(WebElement element){
		try {
			String strJavaScript = "var element = arguments[0];"
					+ "var mouseEventObj = document.createEvent('MouseEvents');"
					+ "mouseEventObj.initEvent( 'mouseover', true, true );"
					+ "element.dispatchEvent(mouseEventObj);";
				((JavascriptExecutor) driver).executeScript(strJavaScript,element);
				System.out.println("Mouse Hoverd to web element: " +element);
			} catch (Exception e) {
				System.out.println("Element " + element + " was not clickable: " + e.getStackTrace());
			}
		}

	
	public void selectDataFromDropdownList(WebElement element, String text){
		try {		
			Select dropdown =  new Select(element);
			dropdown.selectByVisibleText(text);
				System.out.println("Selected Visible text from Element: " +element);	
			} catch (Exception e) {
				System.out.println("Element " + element + " was not available for Selected Text: " +e.getStackTrace());			
			}
		}
	
	public void selectDataFromDropdownListByIndex(WebElement element, int index){
		try {		
			Select dropdown =  new Select(element);
			dropdown.selectByIndex(index);
				System.out.println("Selected text based on Index Value from Element: " +element);	
			} catch (Exception e) {
				System.out.println("Element " + element + " was not available for Selected Text: " +e.getStackTrace());			
			}
		}
	
	public void selectTheCheckbox(WebElement element) {
		try {
		       if (element.isSelected()) {
		           System.out.println("Checkbox is already Selected: "+element);
		        } else {
		           element.click();
		           System.out.println("Checkbox is Selected: "+element);
		        }
		        } catch (Exception e) {
		        	System.out.println("Element " + element + " was not found in Web page: " + e.getStackTrace());
		        }		
		}
	
	public boolean selectTheCheckbox1(WebElement element) {
		boolean flag = false;
		try {
			flag = element.isSelected();
			if(flag){
		           System.out.println("Checkbox is already Selected: "+element);
		        } else {
		           element.click();
		           System.out.println("Checkbox is Selected: "+element);
		        }
			
		        } catch (Exception e) {
		        	System.out.println("Element " + element + " was not found in Web page: " + e.getStackTrace());
		        	return flag;
		        }	
		return flag;
		}
	
	public void selectRadioButton(WebElement element) {
		try {
		       if (element.isSelected()) {
		           System.out.println("Radio button is already Selected: "+element);
		        } else {
		           element.click();
		           System.out.println("Radio button is Selected: "+element);
		        }
		        } catch (Exception e) {
		        	System.out.println("Element " + element + " was not found in Web page: " + e.getStackTrace());
		        }		
		}

	
	public String getTagNameFromWebElement(WebElement element){
	   	String tagName = "";
		try {
			tagName =  element.getTagName();
				System.out.println("Captured Tag name from Web element: " +element);		
			}catch(Exception e){
				System.out.println("Element " + element + " was not found in Web page: " +e.getStackTrace());
				return tagName;
			}		
			return tagName;		
		}	
	
	public String getTextFromWebElement(WebElement element){
	   	String text = "";
		try {
			text =  element.getText();
				System.out.println("Captured Text from Web Element: " +element);		
			}catch(Exception e){
				System.out.println("Element " + element + " was not found in Web page: " +e.getStackTrace());
				return text;
			}		
			return text;		
		}
		
	public void switchToChildFrame(String frame1){
		try	{
			driver.switchTo().frame(frame1);
				System.out.println("Frame has been switched to: "+frame1);
			}catch(Exception e)	{
				System.out.println("Unable to switch to the "+frame1+" Frame: " +e.getStackTrace());
			}
		}
	
	public void switchToParentFrame(){
		try	{
			driver.switchTo().defaultContent();
			driver.switchTo().parentFrame();
			}catch(Exception e)	{
				System.out.println("Unable to switch to the Parent Frame: " + e.getStackTrace());
			}
		}
	
	public String switchToChildWindow() {
		String winHandleBefore = "";
		try	{
			winHandleBefore = driver.getWindowHandle();
			for(String winHandle : driver.getWindowHandles()){
				driver.switchTo().window(winHandle);
				}					
				System.out.println("Window has been switched to: " +winHandleBefore);
			}catch(Exception e)	{
				System.out.println("Unable to switch to the "+winHandleBefore+" Original Window: " +e.getStackTrace());
			}		
		return winHandleBefore;
	}	
	
	public void switchToWindow(String window){
		try	{
			driver.switchTo().window(window);
			}catch(Exception e)	{			
				System.out.println("Unable to switch to the Window: " +e.getStackTrace());
			}
	}
	
	public String getClipboardValue(){
		String result = "";
		try{
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Clipboard clipboard = toolkit.getSystemClipboard();
			result = (String) clipboard.getData(DataFlavor.stringFlavor);
			}catch(Exception e){
				System.out.println("Unable to get the Clipboard Value: "+e.getStackTrace());
			}
		return result;
	}
	
	public  void scrollIntoViewPageElementandClick(WebElement element) {
		try{
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].scrollIntoView(true);", element);
			executor.executeScript("arguments[0].click();", element);
				System.out.println("View and Clicked on Web element: " +element);		
			} catch (Exception e) {
				System.out.println("Element " + element + " was not Clickable: " + e.getStackTrace());
			}
		}
	
	public  void scrollIntoViewPageElement(WebElement element) {
		try{
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].scrollIntoView(true);", element);
			//executor.executeScript("arguments[0].click();", element);
				System.out.println("View and Clicked on Web element: " +element);		
			} catch (Exception e) {
				System.out.println("Element " + element + " was not Clickable: " + e.getStackTrace());
			}
		}
	
	public void scrollIntoPageElementandClick(WebElement element) {
		try{
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
				System.out.println("Clicked on Web element: " +element);		
			} catch (Exception e) {
				System.out.println("Element " + element + " was not Clickable: " + e.getStackTrace());
			}
		}
	
	public void waitForListOfElementsToBeVisible(List<WebElement> element, WebDriver driver) {
		try{
			//WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.visibilityOfAllElements(element));
			System.out.println("Waited for list of element to be visible: " +element);	
			} catch (Exception e) {
				System.out.println("Elements " + element + " was not waited for visible: " +e.getStackTrace() );
		}
	}
	
	public void scrollDownToPageEnd(WebDriver driver) {
		try{
			//JavascriptExecutor Executor = ((JavascriptExecutor)driver);
			executor.executeScript("window.scrollTo(0, document.body.scrollHeight);");			
			} catch (Exception e) {
				System.out.println("Scroll to down has completed: " +e.getStackTrace());
		}
	}

	public void scrollDownToMiddle(WebDriver driver) {
		try{
			//JavascriptExecutor Executor = ((JavascriptExecutor)driver);
			executor.executeScript("window.scrollBy(0, 120);");
			} catch (Exception e) {
				System.out.println("Scroll to middle has completed: " +e.getStackTrace());
		}
	}
	
	public void scrollUpToPageStart(WebDriver driver) {
		try{
			//JavascriptExecutor Executor = ((JavascriptExecutor)driver);
			executor.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
			} catch (Exception e) {
				System.out.println("Scroll to up has completed: " +e.getStackTrace());
		}
	}

	
	@SuppressWarnings("deprecation")
	public void logTextToFile(String fileName, String text) {
		try{
			FileUtils.writeStringToFile(new File(fileName), text);
			}catch(Exception e){
				System.out.println("Unalbe to write the data into File: "+e.getStackTrace());	
			}
	}
	
	public void closeBrowser(){
		try	{
			driver.close();
			}catch(Exception e)	{			
				System.out.println("Unable to close the Browser: " +e.getStackTrace());
		}
	}	
	
	public String convertDateFormat (String time) throws Exception {
		String text = "";
		try{
			SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
			SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
			Date date = displayFormat.parse(time);
			System.out.println("Converted Time format: "+displayFormat.format(date) + " = " + parseFormat.format(date));
			int length = parseFormat.format(date).length();
			text = parseFormat.format(date).substring(0, length);
			}catch(Exception e){
				System.out.println("Unable to convert into Time format: "+e.getStackTrace());
			return text;
		}
		return text;
	}

	
}
