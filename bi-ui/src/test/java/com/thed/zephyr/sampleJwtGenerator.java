package com.thed.zephyr;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.jar.JarException;

import org.codehaus.jettison.json.JSONException;

import com.thed.zephyr.cloud.rest.ZFJCloudRestClient;
import com.thed.zephyr.cloud.rest.client.JwtGenerator;

/**
 * @author swapna.vemula 12-Dec-2016
 *
 */
public class sampleJwtGenerator {

	/**
	 * @param args
	 * @author Created by swapna.vemula on 12-Dec-2016.
	 * @throws URISyntaxException
	 * @throws JobProgressException
	 * @throws JSONException
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	
	public static void main(String[] args) throws URISyntaxException, JarException, JSONException, IllegalStateException, IOException {
		// Replace Zephyr BaseUrl with the <ZAPI_CLOUD_URL> shared with ZAPI Cloud Installation
		String zephyrBaseUrl = "https://prod-play.zephyr4jiracloud.com/connect";
		//String zephyrBaseUrl = "http://www.google.com";
		// zephyr accessKey , we can get from Addons >> zapi section
		String accessKey = "amlyYToyMWVjNTQwZC1mZTczLTQwOTItYjgxMi04NjhkZjY2N2FmZGIgcGFsYWsuc2hhcm1hIFVTRVJfREVGQVVMVF9OQU1F";
							//amlyYToyMWVjNTQwZC1mZTczLTQwOTItYjgxMi04NjhkZjY2N2FmZGIgcGFsYWsuc2hhcm1hIFVTRVJfREVGQVVMVF9OQU1F
		
		// zephyr secretKey , we can get from Addons >> zapi section
		String secretKey = "wW95mn-fDXRn4NXyQrbB8BXnmGhHxWisQmiK4tNTHhc";
							//lew-iYcVQ3Vv8sLvdQ-VqnfoA6vFxPNVjKWexZTUwd4
		// Jira UserName
		String userName = "palak.sharma";
		ZFJCloudRestClient client = ZFJCloudRestClient.restBuilder(zephyrBaseUrl, accessKey, secretKey, userName).build();
		JwtGenerator jwtGenerator = client.getJwtGenerator();
		
		// API to which the JWT token has to be generated
		/*For GET CYCLE REQUEST*/
		//String createCycleUri = zephyrBaseUrl + "/public/rest/api/1.0/cycle/0001487747206662-242ac112-0001?versionId=13800&projectId=11000";
		/*For PUT/UPDATE Execution Status*/
		String createCycleUri = zephyrBaseUrl + "/public/rest/api/1.0/executions/search/cycle/0001487747206662-242ac112-0001?projectId=11000&versionId=13800";
		
		
		URI uri = new URI(createCycleUri);
		int expirationInSec = 136000000;
		/*For GET CYCLE REQUEST*/
		//String jwt = jwtGenerator.generateJWT("GET", uri, expirationInSec);
		/*For PUT/UPDATE Execution Status**/
		String jwt = jwtGenerator.generateJWT("GET", uri, expirationInSec);

		
		// Print the URL and JWT token to be used for making the REST call
		System.out.println  ("API URL : " + uri.toString());
		System.out.println  ("Token : " + (jwt));	

		
	
	}

}
