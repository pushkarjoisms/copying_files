package com.aa.automation;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RolesPage {	
	
	WebDriver driver;
	WebDriverWait wait;
	BaseClass baseClass;
	RolesPage rolesPage ;
	WebElement element;
	JavascriptExecutor executor;	
	
	public RolesPage(WebDriver driver, WebDriverWait wait) throws Exception {
		this.driver = driver;
		wait = new WebDriverWait(driver,60);
		baseClass = new BaseClass(driver, wait);
		executor = (JavascriptExecutor) driver;
	}
	
		// Bread Crumbs Verification
       @FindBy(how = How.CSS, using = ".breadcrumbs")
       private WebElement breadcrumbs_;

        public String breadcrumbs_User_Text(){
	    String breadcrumbs_User_Text=breadcrumbs_.getText();
	    return breadcrumbs_User_Text;
	    }
        
        // Create Role Button        
        @FindBy(how=How.XPATH, using="//div[@class='commandbutton-button-label' and text()='Create role']")
        private WebElement CreateRoleButton;
               
        
        // Cancel Button        
        @FindBy(how=How.XPATH, using="//div[@class='commandbutton-button-label' and text()='Close']")
        private WebElement Close;
        
        public  void ClickClose(){
        baseClass.waitForElementToBeClickable(Close);  
        executor.executeScript("arguments[0].click();",Close );
            }
                
        public void sendTextToRoleNameInputBox(String roleName){
        	
        	element = driver.findElement(By.xpath("//input[contains(@name,'name')]"));        	
        	baseClass.waitForElementToBePresent(element);
        	baseClass.enterDataIntoEditbox(element, roleName);        
        }     
        
        
        public void sendTextToRoleDescriptionInputBox(String roleDescription){
        	
        	element = driver.findElement(By.xpath("//input[contains(@name,'description')]"));        	
        	baseClass.waitForElementToBePresent(element);
        	baseClass.enterDataIntoEditbox(element, roleDescription);        	
        }

        //DASHBOARDS TAB
        
        //View Dashboards        
        @FindBy(how=How.XPATH, using="//div[contains(@class,'gridlayout-content')]/div/div[1]/label/div[2]/div/div/div/label/div")
        private WebElement ViewDashboardsCheckBox;
        
        public void ClickViewDashboardsCheckBox(){
        ViewDashboardsCheckBox.click();
        }
        
        //ACTIVITY TAB
        
        //View My In Progress Activity        
        @FindBy(how=How.XPATH, using="//div[contains(@class,'gridlayout-content')]/div/div[2]/label/div[2]/div/div[1]/div/label/div")
        private WebElement ViewMyInProgressActivityCheckBox;
        
        public void ClickViewMyInProgressActivityCheckBox(){
        ViewMyInProgressActivityCheckBox.click();
        }
        
        //View Everyone's In Progress Activity        
        @FindBy(how=How.XPATH, using="//input[@name = 'features.taskscheduling-everyoneschedule']/parent::div[@class = 'checkboxinput-control']")
        private WebElement ViewEveryonesProgressActivityCheckBox;
        
        public void ClickViewEveryonesProgressActivityCheckBox(){
        	executor.executeScript("arguments[0].click();",ViewEveryonesProgressActivityCheckBox );
        } 
        
 
        
        //View My Scheduled bots Check Box         
        @FindBy(how=How.XPATH, using="//span[text()='View my scheduled bots']/preceding-sibling::div[@class = 'checkboxinput-control']")
        private WebElement ViewMyScheduledBotsCheckBox;
        
        public void ClickViewMyScheduledBotsCheckBox(){
        	baseClass.waitForElementToBeClickable(ViewMyScheduledBotsCheckBox);
        	executor.executeScript("arguments[0].click();",ViewMyScheduledBotsCheckBox );
        }
 
        //Schedule my bots to run
                  @FindBy(how=How.XPATH, using="//span[text()='Schedule my bots to run']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement ScheduleMyBotsToRunCheckBox;
        
                  public void ClickScheduleMyBotsToRunCheckBox(){
                	  baseClass.waitForElementToBeClickable(ScheduleMyBotsToRunCheckBox);
                  	executor.executeScript("arguments[0].click();",ScheduleMyBotsToRunCheckBox );
                  }
        
        //Edit my scheduled activity
                  @FindBy(how=How.XPATH, using="//span[text()='Edit my scheduled activity']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement EditMyScheduledActivityCheckBox;
        
                  public void ClickEditMyScheduledActivityCheckBox(){
                	  baseClass.waitForElementToBeClickable(EditMyScheduledActivityCheckBox);
                      executor.executeScript("arguments[0].click();",EditMyScheduledActivityCheckBox );
                  }
		
        //Delete my scheduled activity
                  @FindBy(how=How.XPATH, using="//span[text()='Delete my scheduled activity']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement DeleteMyScheduledActivityCheckBox;
        
                  public void ClickDeleteMyScheduledActivityCheckBox(){
                	  baseClass.waitForElementToBeClickable(DeleteMyScheduledActivityCheckBox);
                      executor.executeScript("arguments[0].click();",DeleteMyScheduledActivityCheckBox );
                  } 
                  
        //View And Manage ALL Scheduled Activity
                  @FindBy(how=How.XPATH, using="//span[text()='View and manage ALL scheduled activity']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement ViewAndManageALLScheduledActivityCheckBox;
        
                  public void ClickViewAndManageALLScheduledActivityCheckBox(){
                	  baseClass.waitForElementToBeClickable(ViewAndManageALLScheduledActivityCheckBox);
                      executor.executeScript("arguments[0].click();",ViewAndManageALLScheduledActivityCheckBox );
                  }         

                  //BOTS TAB                  
                  
                  //View My Bots
         @FindBy(how=How.XPATH, using="//span[text()='View my bots']/preceding-sibling::div[@class = 'checkboxinput-control']")
         private WebElement ViewMyBotsCheckBox;
                  
         public void ClickViewMyBotsCheckBox(){
        	 baseClass.waitForElementToBeClickable(ViewMyBotsCheckBox);
        	 executor.executeScript("arguments[0].click();",ViewMyBotsCheckBox );
             
         }
          		 
        //Run My Bots                  
                  @FindBy(how=How.XPATH, using="//span[text()='Run my bots']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement RunMyBotsCheckBox;
                  
                  public void ClickRunMyBotsCheckBox(){
                	  baseClass.waitForElementToBeClickable(RunMyBotsCheckBox);
                      executor.executeScript("arguments[0].click();",RunMyBotsCheckBox );
                  }
          	          
        //Unlock Locked Bots
                  @FindBy(how=How.XPATH, using="//span[text()='Unlock locked bots']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement UnlockLockedBotsCheckBox;
                  
                  public void ClickUnlockLuckedBotsCheckBox(){
                	  baseClass.waitForElementToBeClickable(UnlockLockedBotsCheckBox);
                      executor.executeScript("arguments[0].click();",UnlockLockedBotsCheckBox );
                  }
         
        //Set Production Version Of Bots         
                  @FindBy(how=How.XPATH, using="//span[text()='Set production version of bots']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement SetProductionVersionOfBotsCheckBox;
                  
                  public void ClickSetProductionVersionofBotsCheckBox(){
                	  baseClass.waitForElementToBeClickable(SetProductionVersionOfBotsCheckBox);
                      executor.executeScript("arguments[0].click();",SetProductionVersionOfBotsCheckBox );
                  }
                  
                  //Export Bots
                  		  @FindBy(how=How.XPATH, using="//span[text()='Export bots']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  		  private WebElement ExportRolesCheckBox;
                  
                  		  public void ClickExportRolesCheckBox(){
                  			baseClass.waitForElementToBeClickable(ExportRolesCheckBox);
                            executor.executeScript("arguments[0].click();",ExportRolesCheckBox );
                          }   
                  
                  
        //Import Bots
                  @FindBy(how=How.XPATH, using="//span[text()='Import bots']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement ImportRolesCheckBox;
                 
                  public void ClickImportRolesCheckBox(){
                	  baseClass.waitForElementToBeClickable(ImportRolesCheckBox);
                      executor.executeScript("arguments[0].click();",ImportRolesCheckBox );
                  }  
                  
                  //Manage My Credentials And Lockers                  
         @FindBy(how=How.XPATH, using="//span[text()='Manage my credentials and lockers']/preceding-sibling::div[@class = 'checkboxinput-control']")
         private WebElement ManageMyCredentialsAndLockersCheckBox;
                 
         public void ClickManageMyCredentialsAndLockersCheckBox(){
        	 baseClass.waitForElementToBeClickable(ManageMyCredentialsAndLockersCheckBox);
             executor.executeScript("arguments[0].click();",ManageMyCredentialsAndLockersCheckBox );
         }   
                  
         //Manage My Lockers      
                  @FindBy(how=How.XPATH, using="//span[text()='Manage my lockers']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement ManageMyLockersCheckBox;
                 
                  public void ClickManageMyLockersCheckBox(){
                	  baseClass.waitForElementToBeClickable(ManageMyLockersCheckBox);
                      executor.executeScript("arguments[0].click();",ManageMyLockersCheckBox );
                  }  
                  
         //Administer ALL Lockers         
                  @FindBy(how=How.XPATH, using="//span[text()='View and manage my Bot runners, Bot creators and device pools']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement AdministerALLLockersCheckBox;
                 
                  public void ClickAdministerALLLockersCheckBox(){
                	  baseClass.waitForElementToBeClickable(AdministerALLLockersCheckBox);
                      executor.executeScript("arguments[0].click();",AdministerALLLockersCheckBox );
                  }
                  
         //Administer ALL Lockers         
                  public boolean AdministerALLLockersCheckBoxIsEnabled(){
                	  baseClass.waitForElementToBeClickable(AdministerALLLockersCheckBox);
                	  boolean returnValue;
                      if(AdministerALLLockersCheckBox.isEnabled()){
                    	  System.out.println("enabled");
                    	  returnValue=true;
                      }else{
                    	  returnValue=false;
                      }
                      System.out.println(returnValue);
                      return returnValue;
                  }
                  
          //Administer ALL Lockers         
                  public boolean AdministerALLLockersCheckBoxIsSelected(){
                	  baseClass.waitForElementToBeClickable(AdministerALLLockersCheckBox);
                	  boolean returnValue;
                      if(AdministerALLLockersCheckBox.isSelected()){
                    	  returnValue=true;
                      }else{
                    	  returnValue=false;
                      }
                      System.out.println(returnValue);
                      return returnValue;
                  }
                  
                  //DEVICES TAB
                  
                  //View And Manage My Bot Runners And Bot Creators                  
         @FindBy(how=How.XPATH, using="//span[text()='View and manage my Bot runners and Bot creators']/preceding-sibling::div[@class = 'checkboxinput-control']")
         private WebElement ViewAndManageMyBotRunnersAndBotCreatorsCheckBox;
                 
         public void ClickViewAndManageMyBotRunnersAndBotCreatorsCheckBox(){
        	 baseClass.waitForElementToBeClickable(ViewAndManageMyBotRunnersAndBotCreatorsCheckBox);
             executor.executeScript("arguments[0].click();",ViewAndManageMyBotRunnersAndBotCreatorsCheckBox );
         
         } 
         
         public boolean ViewAndManageMyBotRunnersAndBotCreatorsCheckBoxisSelected(){
        	 baseClass.waitForElementToBePresent(ViewAndManageMyBotRunnersAndBotCreatorsCheckBox);
        	 boolean returnValue;
        	 if(ViewAndManageMyBotRunnersAndBotCreatorsCheckBox.isEnabled()){
        		 returnValue=true;
        	 }else{
        		 returnValue=false;
        	 }
        	 return returnValue;
         }
         
         //View And Manage Bot Farm         
         @FindBy(how=How.XPATH, using="//span[text()='View and manage Bot Farm']/preceding-sibling::div[@class = 'checkboxinput-control']")
         private WebElement ViewAndManageBotFarmCheckBox;
                 
         public void ClickViewAndManageBotFarmCheckBox(){
        	 baseClass.waitForElementToBeClickable(ViewAndManageBotFarmCheckBox);
             executor.executeScript("arguments[0].click();",ViewAndManageBotFarmCheckBox );
         } 
         
         //AUDIT LOG TAB

         //View Everyone's Audit Log Actions         
         @FindBy(how=How.XPATH, using="//input[@name = 'features.recentactivities-recentactivities']/parent::div[@class='checkboxinput-control']")
         private WebElement ViewEveryonesAuditLogActionsCheckBox;
                 
         public void ClickViewEveryonesAuditLogActionsCheckBox(){
        	 baseClass.waitForElementToBeClickable(ViewEveryonesAuditLogActionsCheckBox);
             executor.executeScript("arguments[0].click();",ViewEveryonesAuditLogActionsCheckBox );
         } 
         
         //Archive Audit Log Actions
         			@FindBy(how=How.XPATH, using="//span[text()='Archive audit log actions']/preceding-sibling::div[@class = 'checkboxinput-control']")
         			private WebElement ArchiveAuditLogActionsCheckBox;
                 
         			public void ClickArchiveAuditLogActionsCheckBox(){
         				baseClass.waitForElementToBeClickable(ArchiveAuditLogActionsCheckBox);
         				executor.executeScript("arguments[0].click();",ArchiveAuditLogActionsCheckBox );
         			}
         
         			//ADMINISTRATION TAB       
         			
         			//View And Manage Settings         
         @FindBy(how=How.XPATH, using="//span[text()='View and manage Settings']/preceding-sibling::div[@class = 'checkboxinput-control']")
         private WebElement ViewAndManageSettingsCheckBox;
                 
         public void ClickViewAndManageSettingsCheckBox(){
        	 baseClass.waitForElementToBeClickable(ViewAndManageSettingsCheckBox);
             executor.executeScript("arguments[0].click();",ViewAndManageSettingsCheckBox );
         } 
         
         // View Users         
         @FindBy(how=How.XPATH, using="//span[text()='View users']/preceding-sibling::div[@class = 'checkboxinput-control']")
         private WebElement ViewUsersCheckBox;
                 
         public void ClickViewUsersCheckBox(){
        	 baseClass.waitForElementToBeClickable(ViewUsersCheckBox);
             executor.executeScript("arguments[0].click();",ViewUsersCheckBox );
         } 
         
         //Create Users
                  @FindBy(how=How.XPATH, using="//span[text()='Create users']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement CreateUsersCheckBox;
                 
                  public void ClickCreateUsersCheckBox(){
                      executor.executeScript("arguments[0].click();",CreateUsersCheckBox );
                  } 
         //Edit Users
                  @FindBy(how=How.XPATH, using="//span[text()='Edit users']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement EditUsersCheckBox;
                 
                  public void ClickEditUsersCheckBox(){
                      executor.executeScript("arguments[0].click();",EditUsersCheckBox );
                  } 
         //Delete Users
                  @FindBy(how=How.XPATH, using="//span[text()='Delete users']/preceding-sibling::div[@class = 'checkboxinput-control']")
                  private WebElement DeleteUsersCheckBox;
                 
                  public void ClickDeleteUsersCheckBox(){
                      executor.executeScript("arguments[0].click();",DeleteUsersCheckBox );
                  } 
         
                  //View And Manage Roles         
          @FindBy(how=How.XPATH, using="//span[text()='View and manage roles']/preceding-sibling::div[@class = 'checkboxinput-control']")
          private WebElement ViewAndManageRolesCheckBox;
                 
          public void ClickViewAndManageRolesCheckBox(){
              executor.executeScript("arguments[0].click();",ViewAndManageRolesCheckBox );
          }        
         
          //View And Manage License         
          @FindBy(how=How.XPATH, using="//span[text()='View and manage licenses']/preceding-sibling::div[@class = 'checkboxinput-control']")
          private WebElement ViewAndManageLicenseCheckBox;
                 
          public void ClickViewAndManageLicenseCheckBox(){
              executor.executeScript("arguments[0].click();",ViewAndManageLicenseCheckBox );
          }          
         
          //CLIENT UI FEATURES

          //Access The IQ Bot Validator 
          @FindBy(how=How.XPATH, using="//span[text()='Access the IQ Bot Validator']/preceding-sibling::div[@class = 'checkboxinput-control']")
          private WebElement AccessTheIQBotValidatorCheckBox;
                 
          public void ClickAccessTheIQBotValidatorCheckBox(){
        	  baseClass.waitForElementToBeClickable(AccessTheIQBotValidatorCheckBox);
              executor.executeScript("arguments[0].click();",AccessTheIQBotValidatorCheckBox );
          }          
         
          // Next Button                  
          @FindBy(how=How.XPATH, using="//button[contains(.,'Next')]")
          private WebElement NextButton;
                 
          public void clickOnNextButton(){
        	  element = driver.findElement(By.xpath("//button[contains(.,'Next')]"));        	  
        	  baseClass.waitForElementToBeClickable(element); 
        	  baseClass.clickOnWebElement(element);
          }
          
          // Next Button for Add Users                 
          @FindBy(how=How.XPATH, using="//button[contains(.,'Next')]")
           private WebElement NextButtonForAddUsers;
                        
           public void ClickNextButtonAddUsers(){
           baseClass.waitForElementToBeClickable(NextButton);  
           executor.executeScript("arguments[0].click();",NextButtonForAddUsers );
           }
           
           //  Add Users To Role               
           @FindBy(how=How.XPATH, using="//div[@class='wizard-steps']//div[text()='Users']/parent::button")
            private WebElement AddUsersToRole;
                         
            public void ClickAddUsersToRole(){
            baseClass.waitForElementToBeClickable(AddUsersToRole);  
            executor.executeScript("arguments[0].click();",AddUsersToRole );
            }
                                      //CREATE ROLE----ADD USER PAGE          
          
            //DropDown            
          @FindBy(how = How.CSS, using = ".selectinput")
          private WebElement textinput_cell_dropdown ;

          public void userFieldDropdown(String userFieldDropDown){
	       Select select=new Select(textinput_cell_dropdown);
	       select.selectByVisibleText(userFieldDropDown);
           }

          //Search User          
          @FindBy(how = How.CSS, using = "div[class='textinput-cell textinput-cell-input']")
          private WebElement search_user ;

          //Search Icon          
          @FindBy(how = How.CSS, using = "div[class='textinput-cell textinput-cell--after']")
          private WebElement search_icon ;
          
          
          //TABLE
 
          //Select All CheckBox          
          @FindBy(how = How.XPATH, using = "//div[contains(@class,'datatable-column datatable-column--selector datatable-column--clickable')]/label/div/label/div")
          public WebElement select_all_checkbox;
          
          								
          //Method for Create New Role
        public void selectTypeOfRolePermission(String...RoleTypeName) throws InterruptedException{        
      	
      	  for(String Str:RoleTypeName){
      		  if(Str == "View everyone's audit log actions"){
      			String AddRoleTypeXpath="//input[@name = 'features.recentactivities-recentactivities']/parent::div[@class='checkboxinput-control']";
      			 WebElement AddRoleTypeCheckBox=driver.findElement(By.xpath(AddRoleTypeXpath));
                  baseClass.waitForElementToBeClickable(AddRoleTypeCheckBox);
                  executor.executeScript("arguments[0].click();",AddRoleTypeCheckBox );
      		  } else if(Str == "View everyone's In progress activity"){
      			String AddRoleTypeXpath="//input[@name = 'features.taskscheduling-everyoneschedule']/parent::div[@class='checkboxinput-control']" ;
      			WebElement AddRoleTypeCheckBox=driver.findElement(By.xpath(AddRoleTypeXpath));
                baseClass.waitForElementToBeClickable(AddRoleTypeCheckBox);
                executor.executeScript("arguments[0].click();",AddRoleTypeCheckBox );
      		  } else {
      		  String AddRoleTypeXpath="//span[text()='"+Str+"']/preceding-sibling::div[@class = 'checkboxinput-control']";  
      		  WebElement AddRoleTypeCheckBox=driver.findElement(By.xpath(AddRoleTypeXpath));
            baseClass.waitForElementToBeClickable(AddRoleTypeCheckBox);
            baseClass.scrollIntoPageElementandClick(AddRoleTypeCheckBox);
            //executor.executeScript("arguments[0].click();",AddRoleTypeCheckBox );
      		  }
           }
    }
        
     // Get header And Its Column Data
     		public boolean getColumnDataFromTable(String columnHeader, String row)throws InterruptedException {
     			boolean TableRow = true;
     			List<String> DataList = new ArrayList<String>();
    			String startaXpath = "//*[@class='datatable-rows']/div[";
    			String endaXpath = "/div[";
    			int counta = 0;
    			baseClass.waitForThreadToLoad(2);
    			List<WebElement> allHeaders = driver.findElements(By.xpath("//*[@class='datatable-header']/div[1]/div"));
    			String columnaStart = "//div[@class='datatable-header']/div[1]/div[";
    			String columnaEnd = "]";
    			int coulmnaCount = allHeaders.size();
    			if (coulmnaCount > 1) {
    				for (int i = 1; i < coulmnaCount - 1; i++) {
    					String columnName = driver.findElement(By.xpath(columnaStart + i + columnaEnd)).getText();
    					if (columnName.equalsIgnoreCase(columnHeader)) {
    						List<WebElement> rows = driver.findElements(By.xpath("//*[@class='datatable-rows']/div"));
    						int rowCount = rows.size();
    						for (int j = 1; j <= rowCount; j++) {
    							int a = counta + 1;
    							String getData = driver.findElement(By.xpath(startaXpath + j + "]" + endaXpath + a + "]")).getText();
    							DataList.add(getData);
    						}
    					} else {
    						counta++;
    					}
    				}
    			}
    			//if (DataList.stream().filter(s -> s.equalsIgnoreCase(row)).findFirst().isPresent()){
     			if (DataList.contains(row)) {
     				return TableRow = true;
     			} else {
     				return TableRow = false;
     			}

     		}

     		public void deleteRoleFromDataTable(String roleName) throws Exception
     		{
     			searchForRole(roleName);
     			mouseHoverEventToExpandActionMenu();
     			element= driver.findElement(By.xpath("//div[@class='datatable-actions-content']//span[contains(@class,'icon aa-icon aa-icon-action-delete icon--animate-none icon--block')]"));
     			baseClass.waitForElementToBeClickable(element);
     			//baseClass.scrollIntoPageElementandClick(element);
     			baseClass.clickOnWebElement(element);
     			baseClass.waitForThreadToLoad(2);
     		}    		
     		 

     		private void searchForRole(String taskbot)
     		{
     			element = driver.findElement(By.xpath("//input[@placeholder='Search role name']"));
     			baseClass.enterDataIntoEditbox(element, taskbot);     			
     			element = driver.findElement(By.xpath("//*[@class='datafiltersearch-icon']"));
     			baseClass.clickOnWebElement(element);     			
     			//element.sendKeys(Keys.ENTER);
     		}
     		
     		private void searchBotForVerify(String taskbot)
     		{
     			element = driver.findElement(By.xpath("//input[@placeholder='Search bot name']"));
     			baseClass.waitForElementToBePresent(element);
     			baseClass.enterDataIntoEditbox(element, taskbot);
     			element = driver.findElement(By.xpath("//*[@class='datafiltersearch-icon']"));
     			baseClass.clickOnWebElement(element);
     			//element.sendKeys(Keys.ENTER);
     		}
     		
     		private void selectTypeFromDropdown(String text){
     			
     			element = driver.findElement(By.xpath("//select[@class='datafilterchoose-input']"));
     			baseClass.waitForElementToBePresent(element);
     			baseClass.selectDataFromDropdownList(element, text);
     		}
     		
     		
     		//Accept Delete Row		
     		@FindBy(how = How.XPATH, using = "//div[@class='modal-content confirm confirm--theme-info']//div[text()='Yes, delete']")
     		private WebElement acceptDeleteRowButton;

     		public void clickAcceptDeleteRowButton() {
     			
     			element = driver.findElement(By.xpath("//div[@class='modal-content confirm confirm--theme-info']//div[text()='Yes, delete']"));
    			baseClass.waitForElementToBePresent(element);
    			baseClass.moveToWebElementandClick(element);    			
     		}
     		
     		@FindBy(how = How.XPATH, using = "//button[contains(., 'Create role')]")
     		private WebElement createRole;
     	// click on create Role 
     		public void clickOnCreateRoleButton() {
     			baseClass.waitForThreadToLoad(2);
     			element = driver.findElement(By.xpath("//button[contains(., 'Create role')]"));     			
     			baseClass.waitForElementToBeClickable(element);
     			baseClass.scrollIntoPageElementandClick(element);
     		}
     		
     		public void clickOnSaveChangesButton() {
     			baseClass.waitForThreadToLoad(2);
     			element = driver.findElement(By.xpath("//button[contains(., 'Save changes')]"));     			
     			baseClass.waitForElementToBeClickable(element);
     			baseClass.scrollIntoPageElementandClick(element);
     		}
     		
     		//My Tasks SelectAll Check Box
     		@FindBy(how = How.XPATH, using = "//div[@class='foldercheckboxlist']/div[6]/div/span[1]//div[@class='checkboxinput-control']")
     		private WebElement MyTasksSelectAll;

     		public void clickOnMyTasksForSelectAll(){
     			element = driver.findElement(By.xpath("//div[@class='foldercheckboxlist']/div[6]/div/span[1]//div[@class='checkboxinput-control']"));
     			baseClass.waitForElementToBeClickable(element);     			
     			baseClass.scrollIntoPageElementandClick(element);
     		}
     		
     		public void clickOnresourcesForSelectAll(){
     			element = driver.findElement(By.xpath("//div[@class='foldercheckboxlist']/div[7]/div/span[1]//div[@class='checkboxinput-control']"));
     			baseClass.waitForElementToBeClickable(element);    			
     			baseClass.scrollIntoPageElementandClick(element);
     		}
     		
     		public void clickOnTasksForSelectAll(){
     			element = driver.findElement(By.xpath("//div[@class='foldercheckboxlist']/div[8]/div/span[1]//div[@class='checkboxinput-control']"));
     			baseClass.waitForElementToBeClickable(element);     			
     			baseClass.scrollIntoPageElementandClick(element);
     		}
     		
     		//Click On Select All Devices	
     		@FindBy(how=How.XPATH, using="//div[@class='datatableinput-source']//tbody[@class='datatable-table-rows']/tr[2]/td[1]//div[@class='checkboxinput-control']")
     	    private WebElement SelectDevice;
     	           
     	    public void clickOnSelectAllDevicesCheckBox(){
     	    element = driver.findElement(By.xpath("//div[@class='checkboxinput checkboxinput--interactive checkboxinput--theme-light']/label[1]/div[1]"));
     	    baseClass.waitForElementToBeClickable(element);  
     	    baseClass.scrollIntoPageElementandClick(element);
     	    baseClass.waitForThreadToLoad(2);
     	    }
     	    
     	 //Click On Move Next table Button	
     	  	@FindBy(how=How.XPATH, using="//div[@class='datatableinput-controls']/div[1]")
     	      private WebElement MoveNextTableButton;
     	             
     	    public void clickOnMoveToNextTableButton(){
     	    element = driver.findElement(By.xpath("//div[@class='datatableinput-controls']/div[2]"));     	    	  
     	    baseClass.waitForElementToBeClickable(element); 
     	    baseClass.moveToWebElementandClick(element);
     	      }
     	      
     	     public void mouseHoverEventToExpandActionMenu() throws InterruptedException
     		{
     	    	baseClass.waitForThreadToLoad(2);
     			WebElement mouseOverOnRow = driver.findElement(By.xpath("//div[contains(@class,'datatable-actions')]//div[1]"));
     			String strJavaScript = "var element = arguments[0];"
     					+ "var mouseEventObj = document.createEvent('MouseEvents');"
     					+ "mouseEventObj.initEvent( 'mouseover', true, true );"
     					+ "element.dispatchEvent(mouseEventObj);";
     			((JavascriptExecutor) driver).executeScript(strJavaScript,mouseOverOnRow);
     			baseClass.waitForThreadToLoad(2);
     		}
     	      
     	     public boolean verifyRoleAccessPermission(String roleName, String locator) throws Exception
     		{
     			
     	    	selectTypeFromDropdown("Bot name");
     	    	searchBotForVerify(roleName);
     			mouseHoverEventToExpandActionMenu();
     			try {
     				element = driver.findElement(By.xpath("//span[contains(@class,'"+locator+"')]"));
     				baseClass.waitForElementToBePresent(element);
     				return element.isDisplayed();
     			} catch(Exception e){
     				System.out.println("Element not found: "+e.getMessage());
     				return false;

     			}
     		}
     	     
     	  //Method for Create New Role
     	    public void giveDetailsForCreatingRole(String RoleName,String Description,String...RoleTypeName) throws InterruptedException{
     	    	baseClass.waitForThreadToLoad(2);
     	    	sendTextToRoleNameInputBox(RoleName);
     	    	sendTextToRoleDescriptionInputBox(Description);
     	    	selectTypeOfRolePermission(RoleTypeName);
     	  	
     	    }
        

}
