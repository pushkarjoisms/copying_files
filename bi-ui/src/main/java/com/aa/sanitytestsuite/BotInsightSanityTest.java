/*
package com.aa.sanitytestsuite;

import org.testng.annotations.Test;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;

import javax.swing.JOptionPane;

import org.openqa.selenium.Platform;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.os.WindowsUtils;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aa.automation.AnalyzeTest;
import com.aa.automation.BaseClass;
import com.aa.automation.DashboardPage;
import com.aa.automation.DashboardTest;
import com.aa.automation.DataProfilePage;
import com.aa.automation.DataProfileTest;
import com.aa.automation.LoginPage;
import com.aa.automation.LoginTest;
import com.aa.automation.LogoutTest;
import com.aa.automation.OperationAnalyticsTest;
import com.aa.automation.RolesTest;
import com.aa.zephyrintegration.HttpHelper;
import com.aa.zephyrintegration.UpdateExecutionStatusToJira;


public class BotInsightSanityTest {

	WebDriver driver;
//	WebDriverWait wait;
//	BaseClass baseClass;
//	LoginTest botInsightLoginTest;
//	DataProfileTest botInsightDataProfileTest;
//	DashboardTest botInsightDashboardTest;
//	AnalyzeTest botInsightAnalyzeTest;
//	LogoutTest botInsightLogoutTest;
//	HttpHelper httpHelper;
//	DataProfilePage dataProfilePage;
//	DashboardPage dashboardPage;
//	LoginPage loginPage;
//	OperationAnalyticsTest botinsightOperationAnalyticsTest;	
//	RolesTest rolesTest;
	
	//String aaeApplicationPath= WindowsUtils.getEnvVarIgnoreCase("HKEY_CURRENT_USER\\Software\\Automation Anywhere\\ApplicationPath.NET");
	//String aaeMyTaskFolderPath = aaeApplicationPath+"\\Automation Anywhere\\My Tasks\\resources";
	//String aaeMyMetabotFolderPath = aaeApplicationPath+"\\Automation Anywhere\\";
	//String aaeMyDocsFolderPath = aaeApplicationPath+"\\Automation Anywhere\\My Docs\\";
	//String projectRepository = System.getProperty("user.dir")+"\\src\\test\\resources";
	
	@BeforeClass
	@Parameters("browser")
	public void testSetUp(String browser) throws InvocationTargetException {		
		
		//baseClass = new BaseClass(driver);
		//String browserName = BaseClass.GetPropertyName("Browser");
		//String processName = BaseClass.GetPropertyName("Process");
		//baseClass.killProcess(processName);
		//driver = baseClass.launchBrowser(browser);
		
		
		if (browser.equalsIgnoreCase("Chrome")) {
			//To Choose Chrome	
			
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			String downloadFilePath = System.getProperty("user.dir")+"\\Images";					
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.default_directory", downloadFilePath);
				
			ChromeOptions options = new ChromeOptions();		
			options.addArguments("disable-infobars"); 
			options.setExperimentalOption("prefs", chromePrefs);		
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			
			
			//System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			try {
			driver = new ChromeDriver();
			} catch(Exception e){
				System.out.println(e.getMessage());
			}


				
		} else if (browser.equalsIgnoreCase("IE")) {
			//To Choose Internet Explorer
			System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability("ignoreZoomSetting", true);
			driver = new InternetExplorerDriver(capabilities);			
				
		} else if (browser.equalsIgnoreCase("Firefox")) {
			//To Choose Firefox
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");						
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("marionette", true);
			driver =  new FirefoxDriver(capabilities);

		} else {

			System.out.println("Browser is not correct");
		}	
		
		String URL;
		try {
			URL = BaseClass.GetPropertyName("URL");
	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(URL);
		
		
//		wait = baseClass.waitForElementToBeLoad(driver);	
//
//		baseClass = new BaseClass(driver, wait);
//		dataProfilePage = new DataProfilePage(driver, wait);
//		dashboardPage = new DashboardPage(driver, wait);
//		botInsightLoginTest = new LoginTest(driver, wait);
//		botInsightDataProfileTest = new DataProfileTest(driver, wait);
//		botInsightDashboardTest = new DashboardTest(driver, wait);
//		botInsightAnalyzeTest = new AnalyzeTest(driver, wait);
//		botInsightLogoutTest = new LogoutTest(driver, wait);
//		botinsightOperationAnalyticsTest = new OperationAnalyticsTest(driver, wait);
//		rolesTest = new RolesTest(driver, wait);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//baseClass.copyTheFolderFromSourceToDestination(projectRepository+"\\Tasks", aaeMyTaskFolderPath+"\\Tasks");
		//baseClass.copyAtmxTestDataToAAERepository("Tasks", aaeApplicationPath, projectRepo);		
		
	}

	
	@Test(priority=1)
	public void clearDaashboardDataFromDatabaseAndZoomData() throws Exception
	{
				
		System.out.println("\nExecuting Method No: 1");
		String arr[] = {"17022017","AN7_1","Blank_Variables","ExitLoop_at_Counter10","OnlyNumericDataType","OnlyStringDataType","ReadFromText_with_Header","ReadFromText_without_Header","TelecomOrderEntry","ParentTask","ChildTask"};
		for (int i=0; i<arr.length; i++){
		httpHelper.deleteStandardDashboard(arr[i]);
		}
			httpHelper.deleteSavedAsAndPublishedDashboard(BaseClass.GetPropertyName("Dashboard"),BaseClass.GetPropertyName("zoomDevCredentials"));
			httpHelper.deleteSavedAsAndPublishedDashboard(BaseClass.GetPropertyName("Publisheddb"),BaseClass.GetPropertyName("zoomProdCredentials"));
		
	}
	

	@Test(priority=2)
	public void LogintoBotInsight() throws Exception
	{				
		//botInsightLoginTest.loginToControlRoom_WithValidCredentials();
		//rolesTest.CreateRoleAndUser_AssignedRoleToUser_ForBotInsight();
		botInsightLoginTest.loginToBotInsight_ValidCredentials_VerifyLoginSuccessfully();
	}

	@Test(priority=3)
	public void CheckDataProfile() throws Exception
	{
		botInsightDataProfileTest.taskNameLable_OnExistingDataProfile_IsVisible();
	}

	@Test(priority=4)
	public void CheckDashboard() throws Exception
	{
		botInsightDashboardTest.verifyBoomarkItemInsertedSuccessfully();
		botInsightDashboardTest.delete_RemovesSavedDashboard_Successfully();
	}

	@Test(priority=5)
	public void CheckAnalyzeSection() throws Exception, Exception
	{
		botInsightAnalyzeTest.analyzeDashboard();
	}
	
	@Test(priority=61)
	public void operationAnalytics_Dashboardsloading_Successfully() throws Exception
	{
		botinsightOperationAnalyticsTest.OperationAnalytics_Dashboardsloading_Successfully();
	}		
	
	@Test(priority=62)
	public void SeccessfulLogout() throws Exception
	{
		//botInsightLogoutTest.logout_FromBotInsight_VerifySuccessfulLogout();
		botInsightLogoutTest.logoutFromControlRoom();
	}

	@Test(priority=63)
	public void tearDown(ITestResult testResult) throws Exception {

		String methodName = testResult.getMethod().getMethodName();
		Method method  = this.getClass().getMethod(methodName);
		String testMethodName = method.getName();
		int methodExecutionStatus = testResult.getStatus();
		if (methodExecutionStatus == 1){
			System.out.println(testMethodName + " : Executed\n");
		}else{
			System.out.println(testMethodName + " : Not Executed\n");
		}

		//UpdateExecutionStatusToJira updateTestCaseExecutionStatusToZephyrForJira = new UpdateExecutionStatusToJira();
		//updateTestCaseExecutionStatusToZephyrForJira.update(testMethodName, methodExecutionStatus);
	}

	@AfterClass
	public void tearDown()
	{
		driver.quit();
	}   
}
*/