package com.aa.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class DataProfilePage {

	WebDriver driver;
	WebDriverWait wait;
	WebElement element;
	BaseClass baseClass;	
	
	//Object location Identifiers
	By configure = By.xpath("//*[@class='configure']");
	By analyze = By.xpath("//*[@class='analyze']");
	By dataprofile = By.xpath("//*[text()='Data Profile']");
	By searchTask = By.id("search");
	By searchIcon = By.xpath("//div[@class='searchBtn searchIcon']");
	By taskName = By.xpath("//*[text()='17022017']");
	By getTaskName = By.xpath("//*[contains(text(), '17022017')]");
	By dataProfileInfoMessage = By.xpath("//span[text()='Please search and choose a task to start your configuration!']");
	By dataProfileTable = By.xpath("//table[@class='profileTable aa-table']");
	By editDataProfileTable = By.xpath("//div[text()='Edit']");
	By generateNewDashboard = By.xpath("//div[text()='Generate New Dashboard']");
	By saveDataProfileChanges = By.xpath("//div[text()='Save and Generate Dashboard']");
	By cancelDataProfileChanges = By.xpath("//div[text()='Cancel']");
	By enterPublishDashboardName = By.xpath("//input[@placeholder='Please name your dashboard']");
	By displayName= By.xpath("//table[@class='profileTable aa-table']//tr[3]//input");
	By dataType = By.xpath("//table[@class='profileTable aa-table']//tr[3]//select");
	By inclusion = By.xpath("//table[@class='profileTable aa-table']//tr[3]//div[@class='aa-checkbox enabled']");
	By inclusionCheckBoxState = By.xpath("//table[@class='profileTable aa-table']//tr[3]//div[@class='check ng-scope']");
	By previewData = By.xpath("//a[text()='Preview data']");
	By botInsightText = By.xpath("//span[@class='pull-left paddingTop12 ng-binding']");
	By rowCountMessage = By.xpath("//div[@class='rowCountMessage ng-binding']");
	By previewDataTable = By.id("tableBody");
	By unableToGenerateDataProfile = By.xpath("//span[text()='Unable to load data profile.']");
	By atLeastOneNumericDataType = By.xpath("//div[@class='status-message']/div[1]");
	By dashboard = By.xpath("//iframe[@id='dashboardContent']");
	By defaultDb = By.xpath("//a[@href='#/dev?dashboardName=17022017']");
	By dbTitle = By.xpath("//span[@class='bp3-editable-text-content']");
	By search = By.id("search");
	By searchBtn = By.xpath("//div[@class='searchBtn searchIcon']");
	By dataProfile = By.xpath("//div[@class='navBar']/ul/li[2]");
	By rank = By.xpath("//table[@class='profileTable aa-table']/tbody/tr[7]/td[9]/rank-btn/a");
	By recordCount = By.xpath("//select[@ng-model='vm.recordCount']");
	By tableData = By.xpath("//table[@class='rankTable ng-scope']");		
	By currentSort = By.xpath("//select[@ng-model='vm.currentSorting']");
	By iFrame = By.xpath("//iframe[@id='dashboardContent']");		
	By srcUser = By.xpath("//img[@src='assets/images/user.png']");
	By logOut = By.xpath("//button[text()='Log out']");	
	By rankChild = By.xpath("//table[@class='profileTable aa-table']/tbody/tr[12]/td[9]/rank-btn/a");
	By getDataProfileText = By.xpath("//div[@class='ng-binding']");

	public DataProfilePage(WebDriver driver,WebDriverWait wait) throws Exception{
		this.driver = driver;
		wait = new WebDriverWait(driver,60);
		this.wait = wait;
		baseClass = new BaseClass(driver,wait);
	}

	public void clickOnConfigure() throws Exception
	{
		element = driver.findElement(configure);
		baseClass.waitForElementToBePresent(element);
		baseClass.clickOnWebElement(element);			
		}
	
	public String getFrameTextFromDashboard()
	{
		WebDriver frame = driver.switchTo().frame("dashboardContent");
		element = frame.findElement(dbTitle);
		return baseClass.getTextFromWebElement(element);
	}	

	public void clickOnAnalyze()
	{
		element = driver.findElement(analyze);
		baseClass.clickOnWebElement(element);
	}

	public void clickOnDefaultDbLink()
	{
		element = driver.findElement(defaultDb);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void selectDataProfile()throws Exception
	{		
		element = driver.findElement(dataprofile);
		baseClass.clickOnWebElement(element);
	}

	public String getInfoMessageAskingToSearchTask()
	{
		element = driver.findElement(dataProfileInfoMessage);
		return baseClass.getTextFromWebElement(element);
	}

	public void searchAndSelectTask(String taskSearchString) throws Exception
	{
		try{
			element = driver.findElement(searchTask);
			baseClass.enterDataIntoEditbox(element, taskSearchString);
			element = driver.findElement(searchIcon);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			baseClass.waitForThreadToLoad(10);
			}catch(Exception e){			
				System.out.println("Error in searchAndSelectTask() Method: "+e.getStackTrace());
		}
	}

	public String getFontColorOfDashboard() throws Exception
	{
		String text = "";
		try{
			text =  driver.findElement(searchTask).getCssValue("color");
			}catch(Exception e){
				System.out.println("Error in getFontColorOfDashboard() Method: "+e.getStackTrace());
				return text;
		}
		return text;
	}	

	public void clickOnEditLink() throws Exception
	{		
		element = driver.findElement(editDataProfileTable);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);		
	}

	public void clickOnCancelButton()
	{
		element = driver.findElement(cancelDataProfileChanges);
		baseClass.clickOnWebElement(element);
	}

	public void clickOnSaveAndGenerateNewDashboard()
	{
		element = driver.findElement(saveDataProfileChanges);
		baseClass.clickOnWebElement(element);
	}	

	public Boolean atLeastOneNumericDataTypeMessage()
	{
		try	{
			element = driver.findElement(atLeastOneNumericDataType);
			baseClass.getTextFromWebElement(element);			
			}catch(Exception e)	{
				System.out.println("Error in atLeastOneNumericDataTypeMessage() Method: "+e.getStackTrace());
			return true;
		}
		return false;
	}

	public void clickGenerateNewDashbord()
	{
		element = driver.findElement(generateNewDashboard);
		baseClass.clickOnWebElement(element);
	}

	public String getEditAndGenerateNewDashbordLinks()
	{
		String text = null;
		try{		
			//WebElement element1 = driver.findElement(generateNewDashboard);
			WebElement element2 = driver.findElement(editDataProfileTable);
			//text = element1.getCssValue("color") + element2.getCssValue("color");
			text = element2.getCssValue("color");
			}catch(Exception e){
				System.out.println("Error in getEditAndGenerateNewDashbordLinks() Method: "+e.getStackTrace());
			return text;
		}
		return text;
	}

	public boolean getClassNameOfEditableFields()
	{
		try	{
			driver.findElement(displayName).getClass().getName();
			driver.findElement(dataType).getClass().getName();
			driver.findElement(inclusion).getClass().getName();
			}catch(Exception e)	{
				System.out.println("Error in getClassNameOfEditableFields() Method: "+e.getStackTrace());
			return false;
		}
		return true;
	}

	public void modifyDataTableFields(int rowNumber) throws Exception
	{
		try{
			baseClass.waitForThreadToLoad(2);
			element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']//tr["+rowNumber+"]//input"));		
			baseClass.waitForElementToBePresent(element);
			baseClass.enterDataIntoEditbox(element, "Numeric");		
			element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']//tr["+rowNumber+"]//select"));
			baseClass.selectDataFromDropdownList(element, "String");		
			element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']//tr["+rowNumber+"]//div[@class='aa-checkbox enabled']"));
			baseClass.clickOnWebElement(element);
			}catch(Exception e){
				System.out.println("Error in modifyDataTableFields() Method: "+e.getStackTrace());
		}
	}

	public void setCountryAndStateCode(int rowNumber, String dataType) throws Exception
	{
		try{
			baseClass.waitForThreadToLoad(3);
			element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']//tr["+rowNumber+"]//input"));
			baseClass.enterDataIntoEditbox(element, dataType);
			element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']//tr["+rowNumber+"]//select"));
			baseClass.selectDataFromDropdownList(element, dataType);
			}catch(Exception e){
				System.out.println("Error in setCountryAndStateCode() Method: "+e.getStackTrace());
		}
	}
	
	public void setCityAndStateCode(int rowNumber, String dataType) throws Exception
	{
		baseClass.waitForThreadToLoad(3);
		element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']//tbody//tr["+rowNumber+"]//td[3]//select"));
		baseClass.selectDataFromDropdownList(element, dataType);
	}

	public void modifyDataType()
	{
		element = driver.findElement(dataType);
		baseClass.selectDataFromDropdownList(element, "String");
	}

	public void selectDataType(String datatype)
	{
		element = driver.findElement(dataType);
		baseClass.selectDataFromDropdownList(element, datatype);
	}

	public String getDataProfileRowValues() throws Exception
	{		
		int arr[]={2,3,4,5};
		String rowValues = "";
		try{
		for(int i=0; i<arr.length; i++)	{
			element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']//tr["+arr[i]+"]"));			
			rowValues = rowValues + baseClass.getTextFromWebElement(element);			
			}
			}catch(Exception e){
			System.out.println("Error in getDataProfileRowValues() Method: "+e.getStackTrace());
		}
		return rowValues;
	}

	public String getNullValuesForVariables() throws IOException
	{		
		int arr[]={4, 6, 14};
		String rowValues = "";
		try	{
		for(int i=0; i<arr.length; i++)	{			
			element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']//tr["+arr[i]+"]//td[9]"));
			rowValues = rowValues + baseClass.getTextFromWebElement(element);
			}
			}catch(Exception e){
				System.out.println("Error in getNullValuesForVariables() Method: " + e.getStackTrace());			
		}
		return rowValues;
	}

	public String getDataTypeForAllVariables() throws Exception
	{
		String rowValues = "";
		try{		
		baseClass.waitForThreadToLoad(2);
		for(int i=4; i<15; i++)	{
			int j= i+1;
			element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']//tr["+j+"]//td[3]"));
			rowValues = rowValues + baseClass.getTextFromWebElement(element);
			}
			}catch(Exception e){
				System.out.println("Error in getDataTypeForAllVariables() Method: "+e.getStackTrace());
		}
		return rowValues;
	}

	public boolean getInclusionCheckboxState(){
		try	{
			driver.findElement(inclusionCheckBoxState).getClass().getName();
			} catch(Exception e) {	
				System.out.println("Unable to get the Check box Status: "+e.getStackTrace());
			return false;
		}
		return true;
	}

	public boolean getDataTypeListItems()throws Exception
	{
		boolean flag = false;
		try{
			element = driver.findElement(dataType);
			List<WebElement> options = element.findElements(By.tagName("option"));
			for (WebElement option : options) {
				if("Numeric,String,Zip Code,Country Code,State Code".contains(option.getText()))
					return flag = true; 
				}
			}catch(Exception e){
				System.out.println("Error in getDataTypeListItems() Method: "+e.getStackTrace());
			return flag = false;
		}
		return flag;
	}

	public String isPreviewDataButtonPresent()
	{
		String cssValues = null;
		try{
			element = driver.findElement(previewData);
			cssValues = element.getCssValue("font-size") + element.getCssValue("background-color");
			}catch(Exception e){
				System.out.println("Error in isPreviewDataButtonPresent() Method: "+e.getStackTrace());
			}
		return cssValues;
	}
	
	public String clickOnPreviewData() throws Exception
	{
		String cssValues = null;
		try{
			element = driver.findElement(previewData);
			cssValues = element.getCssValue("font-size") + element.getCssValue("background-color");
			element = driver.findElement(previewData);
			baseClass.clickOnWebElement(element);
			}catch(Exception e){
				System.out.println("Error in clickOnPreviewData() Method: "+e.getStackTrace());
		}
		return cssValues;
	}

	public String getRowCountMessage()
	{
		element = driver.findElement(rowCountMessage);
		return baseClass.getTextFromWebElement(element);		
	}

	public String getPreviewDataTable()
	{
		element = driver.findElement(previewDataTable);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}

	public String getBotInsightText()
	{
		element = driver.findElement(botInsightText);
		return baseClass.getTextFromWebElement(element);
	}

	public String getDataProfileTable() throws Exception
	{
		element = driver.findElement(dataProfileTable);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}
	
	public String getDataProfileText() throws Exception
	{
		element = driver.findElement(getDataProfileText);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}

	public String getParentChildTaskNames(String ParentTask)
	{
		element = driver.findElement(dataProfileTable);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}

	public String getInfoOnNewVariable()
	{
		element = driver.findElement(By.xpath("//span[contains(text(),'New')]"));
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}

	public String getInfoOnRenamedVariable()
	{
		element = driver.findElement(By.xpath("//span[text()='Renamed']/../span[text()='Renamed']"));
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}

	public void searchAndSelectTaskHasVariableNameChanges() throws Exception
	{
		try{
			@SuppressWarnings("resource")
			String data = new Scanner(new File(System.getProperty("user.dir")+"\\src\\test\\resources\\read.txt")).useDelimiter("\\Z").next();		
			element = driver.findElement(searchTask);
			baseClass.waitForElementToBePresent(element);
			baseClass.enterDataIntoEditbox(element, data);		
			element = driver.findElement(searchIcon);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			}catch(Exception e){
				System.out.println("Error in searchAndSelectTaskHasVariableNameChanges() Method: "+e.getStackTrace());
		}
		
	}
	
	public void clickOnRankButton() throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(rankChild); 
		baseClass.scrollIntoViewPageElementandClick(element);
		//baseClass.clickOnWebElement(element);
	}
	
	public String getTextFromTable() throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(tableData);
		return baseClass.getTextFromWebElement(element);		
	}
	
	public void selectDropdownForRecordCount(int index){
		
		element = driver.findElement(recordCount);
		baseClass.selectDataFromDropdownListByIndex(element, index);						
	}
	
	public void selectDropdownForCurrentSort(int index){
		
		element = driver.findElement(currentSort);
		baseClass.selectDataFromDropdownListByIndex(element, index);
	}	
	
	public void setDsiplayName(int rowNumber, String displayName) throws Exception
	{
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']/tbody/tr["+rowNumber+"]/td[2]/input"));
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, displayName);
	}
	
	public void selectDsiplayName(int rowNumber, String displayName) throws Exception
	{
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//table[@class='profileTable aa-table']/tbody/tr["+rowNumber+"]/td[3]/select"));
		baseClass.waitForElementToBePresent(element);
		baseClass.selectDataFromDropdownList(element, displayName);
	}

	
}
