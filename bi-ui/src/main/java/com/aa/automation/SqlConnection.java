package com.aa.automation;

import java.sql.*;

public class SqlConnection {
	
	private static final String jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	private static final String dbURL = "jdbc:sqlserver://ec2-34-213-47-209.us-west-2.compute.amazonaws.com:1433;user=sa;password=aabots@123;databaseName=analytics";
	//private static final String dbURL = "jdbc:sqlserver://dev-analytics.automationanywhere.net:1433;user=analytics;password=zMxYfh8QCFfJDgeB;databaseName=analytics";

	public Connection connection;

	public SqlConnection() throws SQLException, ClassNotFoundException 
	{
		try	{
			Class.forName(jdbcDriver);
			connection = DriverManager.getConnection(dbURL);
		}catch(Exception e)	{
			System.out.println("sqlconnection error in Consructor" + e);
		}
	}
	//Delete Query
	public static void main(String args[]) throws ClassNotFoundException, SQLException
	{
		SqlConnection sql = new SqlConnection();
		sql.deleteDashboardDetails("OnlyNumericDataType");
	}
	
	public int deleteDashboardDetails(String taskname)throws SQLException {
		try	{			
			ResultSet set = select(taskname);
			String arr[]={"task","task_run","run_data","variable","dashboard"};
			while(set.next())
			{
				System.out.println(set.getString(1));
				for (int i=0; i<arr.length; i++)
				{
					System.out.println("delete from analytics.dwa."+arr[i]+" where taskid = '"+set.getString(1)+"'");
					PreparedStatement st  = connection.prepareStatement("delete from analytics.dwa."+arr[i]+" where taskid = '"+set.getString(1)+"'");
					//get Prepared Stament outside look & google on how to pass only array value or variables at run time in loop
					st.executeUpdate();
				}
			}
			return 0;
		}catch(Exception e)	{
			System.out.println("deleteDashboardFromSQLError" + e.getMessage());
			return 1;
		}
	}

	//Delete Saved Dashboard from SQL
	public void deleteSavedDashboardFromSQL(String zoomdatasourceid)throws SQLException {
		try	{
			PreparedStatement st  = connection.prepareStatement("delete from [analytics].[dwa].[dashboard] where zoomdatasourceid= '"+zoomdatasourceid+"'");
			st.executeUpdate();
		}catch(Exception e)	{
			System.out.println("deleteDashboardFromSQLError" + e.getMessage());
		}
	}
	//Get Task ID Query
	public ResultSet select(String taskname)throws SQLException {
		try	{
			String query ="select taskid from [analytics].[dwa].[task_run] where [taskname]='"+taskname+"'" ;
			//String query ="select taskid from [analytics].[dwa].[task_run] where [taskname]="+taskname;
			Statement st1= connection.createStatement();
			st1.execute(query);
			ResultSet rs = st1.getResultSet();
			return rs;
		}catch(Exception e)	{
			System.out.println("getTaskNameError" + e.getMessage());
			return null;
		}
	}

	public String getZoomDataIdsForSavedDashboard(String dashboardname, String selectQuery)throws SQLException {

		try	{
			String zoomDataId= "";
			Statement stmt = null;

			stmt= connection.createStatement();
			String query = selectQuery+"='"+dashboardname+"'" ;

			stmt.execute(query);
			ResultSet zoomDataIdResultSet = stmt.getResultSet();
			while(zoomDataIdResultSet.next())
			{
				zoomDataId = zoomDataIdResultSet.getString(1);
			}
			System.out.println(zoomDataId);
			return zoomDataId;
		}catch(Exception e)	{
			System.out.println("getZoomDataIdError" + e.getMessage());
			return null;
		}
	}

	//Get ZoomDataSourceID & ZoomData DashboardID by TaskID
	public String getZoomDataIdsForStandardDashboard(String taskname, String selectQuery)throws SQLException {

		try	{
			ResultSet set = select(taskname);
			String taskId = "";
			String zoomDataId= "";
			Statement stmt = null;
			//set.next();
			//taskId = set.getString(1);
			while(set.next())
			{
				taskId = set.getString(1);
			}
			
			stmt= connection.createStatement();
			String query = selectQuery+"='"+taskId+"'" ;

			stmt.execute(query);
			ResultSet zoomDataIdResultSet = stmt.getResultSet();
			while(zoomDataIdResultSet.next())
			{
				zoomDataId = zoomDataIdResultSet.getString(1);
			}
			System.out.println(zoomDataId);
			return zoomDataId;
		}catch(Exception e)	{
			System.out.println("getZoomDataIdError" + e.getMessage());
			return null;
		}
	}

	public void close() {
		/*
		 * OVERVIEW: This function is used to close the database connection once opened
		 * PRECONDITIONS: Database connection is opened
		 * MODIFIES: None
		 * POSTCONDITIONS: The database connection is closed
		 */
		try	{
			connection.close();
		}catch (SQLException sqlException){
			sqlException.printStackTrace();
			connection = null;
		}
	}
	protected void finalize()
	{
		close();
	}
	//  public int insert(String query)throws SQLException {
	//	query ="select taskid from [analytics].[dwa].[task] where [taskname]='TelecomOrderEntry'" ;
	//	PreparedStatement st  = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
	//    st.executeUpdate();
	//	ResultSet res = st.getGeneratedKeys();
	//	if(res.next()) {
	//		return res.getInt(1);
	//	} else {
	//		return 0;
	//	}
	//}
}
