package com.aa.common;

import java.io.File;
import java.io.IOException;
import java.util.StringJoiner;
import org.apache.log4j.Logger;

public class AAEClient {
  final static Logger log = Logger.getLogger(AAEClient.class);
  private volatile static AAEClient instance;
  private String authenticationType = System.getProperty("AuthenticationType"); //ACTIVE_DIRECTORY or DATABASE;
  private String domain = System.getProperty("domain"); //SJCENTERPRISE  or DRTEST;
  private final String AAECLIENT_APP = "\"Automation Anywhere.exe\"";
  private final String TASK_KILL = "taskkill /f /IM ";
  private Utility utility = Utility.getInstance();

  public static AAEClient getInstance() {
    if (instance == null) {
      synchronized(Utility.class) {
        if (instance == null) {
          instance = new AAEClient();
        }
      }
    }
    return instance;
  }

  /**
   * To test Active directory the AuthenticationType and domain properties should be set
   * @param username - client username
   * @param password - client password
   * @param closeAAEClient
   * @throws IOException
   * @throws InterruptedException
   */
  public void loginToClient(String username, String password, boolean closeAAEClient)
      throws IOException, InterruptedException {

    StringJoiner cmdString = new StringJoiner(" ");
    String aaeInstallationPath = WindowsReqistry
        .readRegistryValueWithSpace(
       "HKEY_LOCAL_MACHINE\\SOFTWARE\\WOW6432Node\\Automation Anywhere\\AAE\\AAEClient","Path").trim();

    aaeInstallationPath = aaeInstallationPath + "\\Automation Anywhere.exe";
    File file = new File(aaeInstallationPath);
    if(!file.exists()) {
      System.out.println("aaeInstallationPath: with client exe. doesn't exist" + aaeInstallationPath);
    }
    cmdString.add("\"" + aaeInstallationPath.replace("\\", "\\\\") + "\"");
    cmdString.add("\"/c" + utility.getHost() + "\"");

    if(authenticationType == "ACTIVE_DIRECTORY" && domain != null) {
      cmdString.add("\"/u" +domain+"\\"+username+ "\"");
    }else {
      cmdString.add("\"/u" + username + "\"");
    }

    cmdString.add("\"/p" + password+ "\"");
    cmdString.add("\"/llogin\"");

    if(closeAAEClient==true){
      cmdString.add("\"/aclose\"");
    }

    log.info("Login to AAE Client");
    utility.runCommand(cmdString.toString());
  }

  public void killAAEClient() {
    String cmd = TASK_KILL + AAECLIENT_APP;
    utility.runCommand(cmd);
    log.info("AAE Client is terminated");
  }

  public static void main(String[] args) throws IOException, InterruptedException {
    AAEClient aaec = AAEClient.getInstance();
    aaec.loginToClient("biadmin", "W0nderland", false );
    aaec.killAAEClient();
  }
}
