package com.aa.automation;

import com.aa.zephyrintegration.HttpHelper;
import com.aa.zephyrintegration.HttpResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;



public class DashboardPage {

	WebDriver driver;
	Process process;
	WebDriverWait wait;
	WebElement element;
	JavascriptExecutor Executor;
	BaseClass baseClass;	

	//Object location Identifiers
	By configure = By.xpath("//*[@class='configure']");
	By dataprofile = By.xpath("//*[text()='Data Profile']");
	By analyze = By.xpath("//*[@class='analyze']");
	By dashboard = By.xpath("//*[text()='Dashboard']");
	By searchTask = By.id("search");
	By searchIcon = By.xpath("//div[@class='searchBtn searchIcon']");
	By shareToEmailLink = By.xpath("//img[@src='assets/images/icons/share.png']");
	By compareLink = By.xpath("//img[@src='assets/images/icons/compare.png']");
	By saveAsLink = By.xpath("//img[contains(@src ,'assets/images/icons/save_as.png')]");
	By save = By.xpath("//span[text()='Save']");
	By bookmarkLink = By.xpath("//img[contains(@src, 'assets/images/icons/bookmark.png')]");
	By publishLink = By.xpath("//img[contains(@src, 'assets/images/icons/publish.png')]");
	By deleteDashboardLink = By.xpath("//img[contains(@src, 'assets/images/icons/delete.png')]");
	By toolbarBookmark = By.xpath("//img[contains(@src, 'assets/images/toolbar_bookmark.png')]");
	By configureInBookMarkMenu = By.xpath("//span[contains(@ng-click, 'Configure')]");
	By analyzeInBookMarkMenu = By.xpath("//span[contains(@ng-click, 'Analyze')]");
	By expanWidgetOnDashboard = By.xpath("//img[contains(@src, 'expand.png')]");
	By savedDashbord = By.xpath("//a[contains(text(),'SavedAsTask')]");
	By deleteBookmarkItem = By.xpath("//a[contains(text(),'SavedAsTask')]/preceding::img[1]");
	By enterPublishDashboardName = By.xpath("//input[@placeholder='Please name your dashboard']");
	By okay = By.xpath("//*[text()='Okay']");
	By cancel = By.xpath("//span[text()='Cancel']");
	By deletedDashboard = By.xpath("//*[@id='search']/following::ul[1]");
	By createNewWidget = By.id("controlAddVis");
	By addNewWidget = By.xpath("//div[@class='zd icon-data-sources']/a[1]");
	By confirmDashboardDeleted = By.xpath("//div[@id='toast-container']//*[contains(text(), 'Dashboard deleted')]");
	By confirmBookmarkDeleted =By.xpath("//div[@id='toast-container']//*[contains(text(), 'Bookmark')]");
	By errorMessageOFSuplicateDashboard =By.xpath("//div[@id='toast-container']//*[contains(text(), 'already')]");
	By secondaryDashboardSearchBox = By.xpath("//search-input[@dashboard-name='vm.dashboardNameSecondary']//input[@id='search']");
	By secondarySearchIcon = By.xpath("//search-input[@dashboard-name='vm.dashboardNameSecondary']//div[@class='searchBtn searchIcon']");
	By seacondaryDashboardloadig = By.xpath("//div[@class='dashboardContainer'][2]/dashboard[1]");
	By selectIframeWidget = By.xpath("//span[contains(text(),'the total of Numeric1')]");
	By deleteWidgetButton = By.xpath("//*[contains(text(), 'the total Numeric1')]");
	By showMoreOptions = By.xpath("//div[@id='cid-c23']//*[@data-icon='more']");
	By deleteChartOnShowMoreOptions = By.xpath("//div[contains(text(),'Delete Chart')]");
	By completeDeletingWidget = By.xpath("//a[@class='button confirm-button']");
	By expandWidgetButton = By.xpath("//*[contains(text(),'the total Numeric1')]//following::div[@class='header-control full-screen-button']");
	By minimizeWidgetButton = By.xpath("//*[contains(text(),'the total Numeric')]//following::div[@class='exitFullscreen']/i");
	By minimizeWidget = By.xpath("//*[@name='fullscreen_min']");
	By dashboardNameSizeValidation = By.xpath("//div[@class='errors ng-scope']");
	By defaultDashboardUItext= By.xpath("//span[text()='Please search and choose a task to start your configuration!']");
	By exportLinkOnExpandedWidget = By.xpath("//div[@id='cid-c23']//button[@title='Show More']");

	By exportedPNGImage = By.xpath("//li[@type='image/png']//i[@class='zd icon export helper']");
	By exportToPdf = By.xpath("//div[@class='zdView-DataDownloadView dashboard-download']//li[@class='download-item export-pdf']");
	By exportedToPNG = By.xpath("//div[contains(text(),'Export')]");
	By crossTabs = By.xpath("//section[@class='export-data']/ul/li");

	By cUsername = By.name("UserName");
	By cPassword = By.name("Password");
	By cLogin = By.xpath("//span[@id='loginButton-btnEl']");
	By cRoom = By.xpath("//label[text()='Control Room']");
	By opRoom = By.xpath("//span[text()='Operations Room']");	
	By opAnalytics = By.xpath("//span[@id='LinkButton-1442-btnInnerEl']");	
	By opIframe = By.xpath("//iframe[@id='ext-element-43']");
	By opDashboard = By.xpath("//span[text()='AUDIT TRAIL Dashboard']");
    	By titleDB = By.xpath("//span[@class='bp3-editable-text-content']");
	By lbWidget = By.xpath("//*[@id='cid-c51']/div[3]/div[1]/div[1]/div[1]/div/span");
	By lbWidget1 = By.xpath("//*[@id='cid-c47']/div[3]/div[1]/div[1]/div[1]/div/span");
	By lbWidget2 = By.xpath("//*[@id='cid-c46']/div[3]/div[1]/div[1]/div[1]/div/span");	
	By opDropdown = By.xpath("//*[@id='simple-dropdown']");
	By opAudit = By.xpath("//*[@class='dropdown-menu']");	
	By userMenu = By.xpath("//*[@id='usermenu-1108-btnIconEl']");
	By cLogout = By.xpath("//*[@id='hhp_um_Logout-textEl']");

	By search = By.id("search");
	By searchBtn = By.xpath("//div[@class='searchBtn searchIcon']");
	By dataProfile = By.xpath("//div[@class='navBar']/ul/li[2]");
	By iFrame = By.xpath("//iframe[@id='dashboardContent']");
	By taskName = By.xpath("//table[@class='profileTable aa-table']/tbody/tr[1]/td/div[1]/div");
	By clkEdit = By.xpath("//div[text()='Edit']");
	By drpCity = By.xpath("//table[@class='profileTable aa-table']/tbody/tr[3]/td[3]/select");
	By drpState = By.xpath("//table[@class='profileTable aa-table']/tbody/tr[10]/td[3]/select");
	By clkSave = By.xpath("//*[text()='Save']");
	By clkGenerate = By.xpath("//*[text()='Generate New Dashboard']");
	By completeWidget = By.xpath("//*[@id='cid-c48']");
	By selectWidget = By.xpath("//*[contains(text(),'Sum Of Loanbalance By State')]");
	By completeWidget1 = By.xpath("//*[@id='cid-c45']");
	By selectWidget1 = By.xpath("//*[contains(text(),'Sum Of Loanbalance By City')]");

	By saveAs = By.xpath("//span[contains(text(),'Save As')]");
	By placeHolder = By.xpath("//input[@placeholder='Please name your dashboard']");
	By clkFilter = By.xpath("//div[@class='dashboard-header-controls']/span[2]/span/i");
	By addFilter = By.xpath("//*[text()='Add Filter']");
	By ObjField = By.xpath("//span[@title='Gender']");
	By checkMark = By.xpath("//div[@data-value='Male']");
	By asGender = By.xpath("//*[text()='Gender']");

	By clkApply = By.xpath("//button[@value='applyFilter']");
	By objField1 = By.xpath("//span[@title='Marital Status']");
	By checkMark1 = By.xpath("//div[@data-value='Married']");
	By asMaritalStatus = By.xpath("//*[text()='Marital Status']");
	By clkWidget = By.xpath("//*[contains(text(),'total Account Balance')]");
	By clkWidget1 = By.xpath("//*[contains(text(),'total Account Balance')]//following::div[@class='header-control full-screen-button']");
	By clkWidjetFilter = By.xpath("//*[@id='controlFilter']/div[1]");
	By ObjField2 = By.xpath("//span[@title='Credit Card Type']");
	By checkMark2 = By.xpath("//div[@data-value='americanexpress']");
	By ObjField3 = By.xpath("//span[@title='Task Id']");
	By checkMark3 = By.xpath("//div[@data-value='b05dfc1e-bc95-4b1e-a3b0-2bce28272fa8']");		
	By exitScreen = By.xpath("//div[@class='exitFullscreen']/i");
	By clkSave1 = By.xpath("//img[@src='assets/images/icons/save.png']");		
	By clkPublish = By.xpath("//img[@src='assets/images/icons/publish.png']");
	By clkAnalyze = By.xpath("//div[@class='analyze']");
	By clkClose = By.xpath("//div[@data-name='Add Filters']/div[1]/button[1]");
	
	By ageGroup = By.xpath("//table[@class='profileTable aa-table']/tbody/tr[4]/td[2]/input");
	By category = By.xpath("//table[@class='profileTable aa-table']/tbody/tr[7]/td[2]/input");		
	By checkWidget = By.xpath("//*[@id='cid-c48']");
	By checkText = By.xpath("//*[contains(text(),'Amount vary by Na')]");
	By checkStateText = By.xpath("//*[contains(text(),'Sum Of Value By State Code')]");
	By checkCountryText = By.xpath("//*[contains(text(),'Sum Of Value By Country Code')]");
	By checkWidget1 = By.xpath("//*[@id='cid-c49']");		
	By checkText1 = By.xpath("//*[contains(text(),'Amount vary by Pa')]");
	By transformMap = By.xpath("//*[contains(@id,'map-')]");
	
	By addChart = By.xpath("//*[@id='controlAddVis']/div[2]");
	By listLabel = By.xpath("//div[contains(@class, 'listLabel-')]");
	By newWidget = By.xpath("//*[contains(@id, 'cid-c')]/div[3]/div[1]/div[1]/div[1]/div/span");
	By widgetWrapper = By.xpath("//div[@class='widget-label-wrapper ']");
	By expandWidget = By.xpath("//*[@id='cid-c5291']/div[3]/div[1]/div[2]/span[4]/i");
	By clkInfo = By.xpath("//*[@id='controlInfo']/div");
	By inputInfo = By.xpath("//*[@id='vis-name']");
	By saveButton = By.xpath("//button[@value='save']/span");
	
	By clkExport = By.xpath("//span[@title='Export']/i");
	By expPng = By.xpath("//div[text()='Screenshot (png)']");		
	By clikPng = By.xpath("//a[@title='screenshot.png']/img");	
	By expPdf = By.xpath("//div[text()='PDF']");
	By txtHeader = By.xpath("//input[@name='pdf-header']");
	By txtFooter = By.xpath("//input[@name='pdf-footer']");
	By clikPdf = By.xpath("//button[@value='export-pdf']");
	By clkBack = By.xpath("//div[@class='controls back-controls']/button[1]/i");	
	By expJson = By.xpath("//div[text()='Dashboard Configuration (json)']");		
	By clkClose1 = By.xpath("//div[@data-name='Export']/div[1]/button[1]");		
	By expXls = By.xpath("//div[text()='Chart Data']");
	By exitFullScreen = By.xpath("//div[@class='leftPane']//following::div[@class='exitFullscreen']/i");	
	By widgetText = By.xpath("//div[@class='fullScreenContainer']//following::div[@class='widget-label-wrapper ']/span");
	By dbText = By.xpath("//*[text()='Unable to find the dashboard.']");

	public DashboardPage(WebDriver driver,WebDriverWait wait) throws Exception{
		this.driver = driver;
		wait = new WebDriverWait(driver,60);
		this.wait = wait;
		baseClass = new BaseClass(driver, wait);
	}

	public void clickOnConfigure()
	{
		element = driver.findElement(configure);
		baseClass.clickOnWebElement(element);
	}

	public void selectDashboard() throws Exception
	{
		element = driver.findElement(dashboard);
		baseClass.waitForElementToBePresent(element);
		baseClass.clickOnWebElement(element);
	}

	public void clickOnAnalyze()
	{
		element = driver.findElement(analyze);
		baseClass.clickOnWebElement(element);
	}
	
	public void searchAndSelectDashboard(String dashboard) throws Exception
	{
		try{
			baseClass.waitForThreadToLoad(2);
			element = driver.findElement(searchTask);
			baseClass.waitForElementToBePresent(element);
			baseClass.enterDataIntoEditbox(element, dashboard);
			element = driver.findElement(searchIcon);
			baseClass.clickOnWebElement(element);
			baseClass.waitForThreadToLoad(2);
			//baseClass.refreshThePage();
			baseClass.waitForThreadToLoad(5);
			element = driver.findElement(iFrame);
			}catch(Exception e){
				System.out.println("Error in searchAndSelectDashboard() Method: "+e.getStackTrace());
		}
		
	}

	public void deleteDashboard(String dashboard) throws Exception
	{
		boolean flag = false;
		try{
			element = driver.findElement(searchTask);
			baseClass.enterDataIntoEditbox(element, dashboard);
			driver.findElement(searchIcon).click();
			baseClass.waitForThreadToLoad(2);
			baseClass.refreshThePage();
			baseClass.waitForThreadToLoad(5);
			flag = driver.findElement(iFrame).isDisplayed();			
			System.out.println("Delete Saved As Dashboard: "+dashboard);			
			element = driver.findElement(deleteDashboardLink);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			element = driver.findElement(okay);
			baseClass.clickOnWebElement(element);
			} catch(Exception e) {
				System.out.println("Saved As Dashboard has been Deleted: "+dashboard);				
			}	
		}

	public Boolean existWebElement(String task) throws Exception {
		boolean flag = false;
		try {
			element = driver.findElement(searchTask);
			baseClass.enterDataIntoEditbox(element, task);
			driver.findElement(searchIcon).click();
			List<WebElement> element = driver.findElements(By.xpath("//strong[contains(text(),'"+task.substring(0, 8)+"')]"));
			flag = element.size()!= 0;			
				System.out.println("Task is found for delete Dashbaord: "+ task);
			} catch (NoSuchElementException e) {
				System.out.println("Error in existWebElement() Method: "+ e.getStackTrace());
			return false;
		}
		return true;
	}

	public void clickSaveAsButton() throws Exception
	{
		element = driver.findElement(saveAsLink);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}

	public void clickSaveOnButton()
	{
		element = driver.findElement(clkSave1);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);		
	}

	public void enterDashboardName(String Dashboard) throws Exception
	{
		element = driver.findElement(enterPublishDashboardName);
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, Dashboard);
	}

	public void clickOkayLink() throws Exception
	{
		element = driver.findElement(okay);
		baseClass.clickOnWebElement(element);
	}

	public void clickOnBookMarkItem() throws Exception
	{		
		element = driver.findElement(bookmarkLink);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}

	public void clickOnToolBarBookMarkIcon() throws Exception
	{
		element = driver.findElement(toolbarBookmark);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}

	public Boolean getBookmarkSections()
	{
		try{
			element = driver.findElement(configureInBookMarkMenu);
			baseClass.getTextFromWebElement(element);
			element = driver.findElement(analyzeInBookMarkMenu);
			baseClass.getTextFromWebElement(element);	
			}catch(Exception e)	{
				System.out.println("Error in getBookmarkSections() Method: "+ e.getStackTrace());
			return false;
		}
		return true;
	}

	public String getBookmarkItem() throws Exception
	{
		element = driver.findElement(savedDashbord);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}

	public String getDashboardUIMessage() throws Exception
	{
		element = driver.findElement(defaultDashboardUItext);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}

	public void deleteBookMarks(String dashboard)
	{
		element = driver.findElement(By.xpath("//a[contains(text(),'"+dashboard+"')]/preceding::img[1]"));
		baseClass.clickOnWebElement(element);
	}

	public String confirmBookmarkDeleted()
	{
		element = driver.findElement(confirmBookmarkDeleted);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}

	public String getErrorMessageOfDuplicateDashboard()
	{
		element = driver.findElement(errorMessageOFSuplicateDashboard);
		return baseClass.getTextFromWebElement(element);
	}

	public String getDashboardNameSizeValidation()
	{
		element = driver.findElement(dashboardNameSizeValidation);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);

	}
	public void accessBookmarkItem()
	{
		element = driver.findElement(savedDashbord);
		baseClass.clickOnWebElement(element);		
	}

	public void clickOnCompareButton()
	{
		element = driver.findElement(compareLink);
		baseClass.waitForElementToBePresent(element);
		baseClass.clickOnWebElement(element);
	}

	public String  secondaryDashboardSearchBox()
	{
		element = driver.findElement(secondaryDashboardSearchBox);
		return baseClass.getTagNameFromWebElement(element);
	}

	public void  searchSecondaryDashboard(String dashboard) throws Exception
	{
		try{
			element = driver.findElement(secondaryDashboardSearchBox);
			baseClass.waitForElementToBePresent(element);
			baseClass.enterDataIntoEditbox(element, dashboard);
			element = driver.findElement(secondarySearchIcon);
			baseClass.clickOnWebElement(element);
			baseClass.waitForThreadToLoad(10);
			}catch(Exception e){
				System.out.println("Error in searchSecondaryDashboard() Method: "+e.getStackTrace());
		}
	}

	public String  loadSeacondaryDashboard()
	{
		element = driver.findElement(seacondaryDashboardloadig);
		return baseClass.getTagNameFromWebElement(element);
	}

	//Dashboard UI Links after Loading Dashboard
	public Boolean  dashboardUiLinks()
	{
		try	{
			driver.findElement(compareLink).getClass().getName();
			driver.findElement(shareToEmailLink).getClass().getName();
			driver.findElement(saveAsLink).getClass().getName();
			driver.findElement(bookmarkLink).getClass().getName();
			driver.findElement(publishLink).getClass().getName();
			driver.findElement(toolbarBookmark).getClass().getName();
			driver.findElement(expanWidgetOnDashboard).getClass().getName();
			} catch(Exception e){
				System.out.println("Error in dashboardUiLinks() Method: " + e.getStackTrace());
			return false;
		}
		return true;
	}	

	public void deleteSavedAsDashboard() throws Exception
	{
		try{
			element = driver.findElement(deleteDashboardLink);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			element = driver.findElement(okay);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			baseClass.waitForThreadToLoad(2);
			}catch(Exception e){
				System.out.println("Error in deleteSavedAsDashboard() Method: "+e.getStackTrace());
		}
	}

	public void deleteSavedDashbord(String Publisheddb) throws Exception
	{
		try{
			element = driver.findElement(deleteDashboardLink);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			element = driver.findElement(okay);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			}catch(Exception e){
				System.out.println("Error in deleteSavedDashbord() Method: "+e.getStackTrace());
		}
	}

	public void clickPublishLink() throws Exception
	{
		element = driver.findElement(publishLink);
		baseClass.clickOnWebElement(element);
	}

	public void expandWidget() throws Exception
	{
		element = driver.findElement(expandWidgetButton);
		baseClass.mouseHoverEventToElemnt(element);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}

	public void minimizeWidget1() throws Exception
	{
		element = driver.findElement(minimizeWidgetButton);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public String minimizeWidget() throws Exception
	{
		String result = driver.findElement(minimizeWidget).getCssValue("display");
		element = driver.findElement(minimizeWidget);
		baseClass.mouseHoverEventToElemnt(element);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
		return result;
	}

	public void publishDashboard(String Publisheddb) throws Exception
	{
		try{
			element = driver.findElement(publishLink);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			element = driver.findElement(enterPublishDashboardName);
			baseClass.enterDataIntoEditbox(element, Publisheddb);
			element = driver.findElement(okay);
			baseClass.clickOnWebElement(element);
			baseClass.waitForThreadToLoad(5);
			}catch(Exception e){
				System.out.println("Error in publishDashboard() Method: "+e.getStackTrace());
		}
	}

	public void searchDeletedDashboard(String Dashboard) throws Exception
	{
		element = driver.findElement(searchTask);
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, Dashboard);
		element = driver.findElement(searchIcon);
		baseClass.clickOnWebElement(element);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(5);		
	}

	public String verifydashboardDeleted() throws Exception
	{
		element = driver.findElement(deletedDashboard);
		return baseClass.getTextFromWebElement(element);		
	}	

	public void expandTheFilter()throws Exception
	{
		//element = driver.findElement(By.xpath("//*[@class='zd icon filter-dashboard']"));
		element = driver.findElement(By.xpath("//*[@class='zd icon filter-dark']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
		element = driver.findElement(By.xpath("//button[@title='Add Filter']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}

	public boolean selectFirstWidgetInIframe(String text)
	{
		try{
			element = driver.findElement(By.xpath("//div[contains(text(),'"+text+"')]"));
			baseClass.clickOnWebElement(element);
			}catch(Exception e)	{
				System.out.println("Error in selectFirstWidgetInIframe() Method: " + e.getStackTrace());
			return false;
		}
		return true;
	}

	public boolean dashboardWidgetsInIframe()throws Exception
	{
		try{
		String arr[]={"the total Amount","the total Discount","How does Amount vary by Age Group?","How does Amount vary by Category?","This is the value of Amount over time","This is the value of Discount over time"};
		for(int i=0; i<arr.length; i++)	{			
				element = driver.findElement(By.xpath("//div[contains(text(),'"+arr[i]+"')]"));				
				baseClass.scrollIntoPageElementandClick(element);
				baseClass.waitForElementToBePresent(element);
				baseClass.getTextFromWebElement(element);
			}
				} catch(Exception e){
				System.out.println("Error in dashboardWidgetsInIframe() Method: " + e.getStackTrace());
				return false;
			}
		
		return true;
	}

	public boolean exportLinkOnleftPanelOfExpandedWidgets()
	{
		try{
			element = driver.findElement(exportLinkOnExpandedWidget);
			baseClass.waitForElementToBeClickable(element);
			baseClass.scrollIntoPageElementandClick(element);
			element = driver.findElement(exportedToPNG);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			baseClass.waitForThreadToLoad(2);
			driver.findElement(exportedPNGImage).getAttribute("href");;
			element = driver.findElement(exportToPdf);
			baseClass.waitForElementToBePresent(element);
			baseClass.getTextFromWebElement(element);
			element = driver.findElement(crossTabs);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			}catch(Exception e){
				System.out.println("Error in exportLinkOnleftPanelOfExpandedWidgets() Method: " + e.getStackTrace());
			return false;
		}
		return true;
	}

	public void deleteFirstWdgetInIframe() throws Exception
	{
		element = driver.findElement(showMoreOptions);
		baseClass.waitForElementToBeClickable(element);
		baseClass.mouseHoverEventToElemnt(element);
		baseClass.clickOnWebElement(element);
		element = driver.findElement(deleteChartOnShowMoreOptions);
		baseClass.clickOnWebElement(element);

	}
	
	public void completeDeleteWidgetProcess()
	{
		element = driver.findElement(completeDeletingWidget);
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
		
	}

	public String getInfoOfNewVariable() throws Exception
	{
		element = driver.findElement(By.xpath("//*[contains(@title,'New')]"));
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}

	public String getInfoOfRenamedVariable() throws Exception
	{
		element = driver.findElement(By.xpath("//*[contains(@title,'Renamed')]"));
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);	
		
	}

	public int getProductionDashboardData() throws Exception
	{
		int totalRecords = 0;
		try	{
			HttpHelper request = new HttpHelper();
			HttpResult response = request.getBotInsightProductDataJson("ProductionDashboardTask");

			if (response.getStatusCode() >= 200 && response.getStatusCode() < 300) {
				JSONObject productionDataJson= new JSONObject(response.getResponse());
				JSONArray jsonarr = productionDataJson.getJSONArray("profileVariables");
				JSONObject jobj = jsonarr.getJSONObject(1);
				totalRecords = (Integer) jobj.get("totalRecords");
			}
			} catch(Exception e){
			System.out.println("Error in getProductionDashboardData() Method : " + e.getStackTrace());
			return 1;
		}
		return totalRecords;
	}



	public void checkStateElementJavaScriptExecutor() {

		element = driver.findElement(selectWidget);
		baseClass.waitForElementToBePresent(element);
		baseClass.scrollIntoViewPageElement(element);
	}

	public String getTextbyState() throws Exception {
		
		baseClass.waitForThreadToLoad(5);
		element = driver.findElement(selectWidget);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}


	public void checkCityElementJavaScriptExecutor(){

		element = driver.findElement(selectWidget1);
		baseClass.waitForElementToBePresent(element);
		baseClass.scrollIntoViewPageElement(element);
	}

	public String getTextbyCity() throws Exception{

		baseClass.waitForThreadToLoad(5);
		element = driver.findElement(selectWidget1);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}

	public void enterDataInPlaceHolder(String Dashboard){
		
		element = driver.findElement(placeHolder);
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, Dashboard);
	}

	public void clickOnFilter() throws Exception{

		element = driver.findElement(clkFilter);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}

	public String addGenericFilters()throws Exception{
		String strGender = "";
		String strMaritalStatus = "";
		
		try{		
			element = driver.findElement(addFilter);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(ObjField);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(checkMark);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(clkApply);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);	
			baseClass.waitForThreadToLoad(2);
			element = driver.findElement(asGender);		
			strGender = baseClass.getTextFromWebElement(element);	
		
			baseClass.waitForThreadToLoad(2);
			element = driver.findElement(addFilter);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(objField1);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(checkMark1);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(clkApply);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			baseClass.waitForThreadToLoad(2);
			element = driver.findElement(asMaritalStatus);		
			strMaritalStatus = baseClass.getTextFromWebElement(element);
			}catch(Exception e){
				System.out.println("Error in addGenericFilters() Method: "+e.getStackTrace());
		}

		return strGender + strMaritalStatus;
	}

	public void clickOnCloseButton() throws Exception{

		element = driver.findElement(clkClose);
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}

	public void clickOnWidget()throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(clkWidget);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
		
		element = driver.findElement(clkWidget1);		
		baseClass.waitForElementToBeClickable(element);
		baseClass.mouseHoverEventToElemnt(element);
		baseClass.clickOnWebElement(element);
		
		//baseClass.scrollIntoViewPageElementandClick(element);		
	}

	public void clickOnWidgetFilter() throws Exception{

		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(clkWidjetFilter);
		baseClass.scrollIntoViewPageElementandClick(element);
	}

	public void addWidgetLevelFilters(){
		
		try{		
			element = driver.findElement(addFilter);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(ObjField2);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(checkMark2);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(clkApply);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
		
			element = driver.findElement(addFilter);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(ObjField3);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(checkMark3);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);		
			element = driver.findElement(clkApply);
			baseClass.waitForElementToBeClickable(element);
			baseClass.clickOnWebElement(element);
			}catch(Exception e){
				System.out.println("Error in addWidgetLevelFilters() Method: "+e.getStackTrace());
		}		

	}

	public String dashboardTitle(){	
		element = driver.findElement(titleDB);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}	
	
	public void checkTextOnWidget(){		
	    
		element = driver.findElement(checkText);
		baseClass.waitForElementToBePresent(element);
		baseClass.scrollIntoViewPageElement(element);
	}
	
	public String getTextOnWidget(){
		
		element = driver.findElement(checkText);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}
	
	public void checkStateTextOnWidget(){		
	    
		element = driver.findElement(checkStateText);
		baseClass.waitForElementToBePresent(element);
		baseClass.scrollIntoViewPageElement(element);
	}
	
	public String getStateTextOnWidget(){
		
		element = driver.findElement(checkStateText);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}
	
	public void checkCountryTextOnWidget(){		
	    
		element = driver.findElement(checkCountryText);
		baseClass.waitForElementToBePresent(element);
		baseClass.scrollIntoViewPageElement(element);
	}
	
	public String getCountryTextOnWidget(){
		
		element = driver.findElement(checkCountryText);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}
	
	public String getTransformOfDashboard() throws Exception
	{
		String text = "";
		try{
			text =  driver.findElement(transformMap).getCssValue("color");
			}catch(Exception e){
				System.out.println("Error in getTransformOfDashboard() Method: "+e.getStackTrace());
				return text;
		}
		return text;
	}

	
	public void CheckTextonWidget1(){		
	 
		element = driver.findElement(checkText1);
		baseClass.waitForElementToBePresent(element);
		baseClass.scrollIntoViewPageElementandClick(element);
	}
	
	public String getTextOnWidget1(){
		
		element = driver.findElement(checkText1);
		return baseClass.getTextFromWebElement(element);		
	}
	
	public void clkAddChart(){
		
		element = driver.findElement(addChart);
		baseClass.waitForElementToBePresent(element);
		baseClass.clickOnWebElement(element);
	}
	
	public String getLabelText(){
		
		element = driver.findElement(listLabel);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}
	
	public void clkLabel(){
		
		element = driver.findElement(listLabel);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);		
	}
	
	public void clkCharts(String[] charts, int i) throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[text()='"+charts[i]+"']"));
		baseClass.clickOnWebElement(element);
	}
	
	public void clkLabelText(String labelText) throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[contains(text(),'"+labelText+"')]"));
		baseClass.scrollIntoPageElementandClick(element);
	}
	
	public void clkFullScreen(String labelText) throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[contains(text(),'"+labelText+"')]//following::div[@class='header-control full-screen-button']"));
		baseClass.mouseHoverEventToElemnt(element);
		baseClass.clickOnWebElement(element);		
	}
	
	public void clkControInfo() throws Exception{
		
		baseClass.waitForThreadToLoad(2);		
	    element = driver.findElement(By.xpath("//div[@class='leftPane']/div[1]/div[@style='display: block;']//following::div[@id='controlInfo']"));	    
	    baseClass.scrollIntoViewPageElementandClick(element);
	}
	
	public void sendTexttoInfo(String[] charts, int i) throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(inputInfo);
		baseClass.enterDataIntoEditbox(element, charts[i]);
	}
	
	public void clkSaveButton() throws Exception{
		
		baseClass.waitForThreadToLoad(2); 	    
	    element = driver.findElement(saveButton);
	    baseClass.clickOnWebElement(element);
	}
	
	public String getChartName() throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		//element = driver.findElement(By.xpath("//div[@class='fullScreenContainer']//following::div[@class='label-deactivated']"));
		element = driver.findElement(By.xpath("//div[@class='label-deactivated']"));
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}
	
	public void exitFullScreen(String[] charts, int i) throws Exception{		
 	    
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[text()='"+charts[i]+"']//following::div[@class='exitFullscreen']/i"));
		baseClass.clickOnWebElement(element);
	}
	
	public void clkSaveButton1(){
		
		element = driver.findElement(clkSave1);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);		
	}
	
	public void clkPublishButton() throws Exception{
		
		baseClass.waitForThreadToLoad(5); 
		element = driver.findElement(clkPublish);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public Boolean VisibiltyofDashbaord() throws Exception{		
		try{
			element = driver.findElement(iFrame);
			baseClass.waitForElementToBePresent(element);
			}catch(Exception e){
				System.out.println("Error in VisibiltyofDashbaord() Method: "+e.getStackTrace());
			return false;
		}
		return true;
	}
	
	public void clkOnExportButton() throws Exception{
		
		element = driver.findElement(clkExport);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void expPngFormat() throws Exception{
		
		element = driver.findElement(expPng);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void clkPngFormat() throws Exception{
		
		element = driver.findElement(clikPng);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);		
	}
	
	public void expPdfFormat() throws Exception{
		
		element = driver.findElement(expPdf);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);	    
	}
	
	public void headerText(String header) throws Exception{
		
		element = driver.findElement(txtHeader);
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, header);
	}
		
	public void footerText(String footer) throws Exception{
		
		element = driver.findElement(txtFooter);
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, footer);		
	}
	
	public void clkPdfForamt() throws Exception{
		
		element = driver.findElement(clikPdf);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);		
	}
		
	public void clkBackButton() throws Exception{
		
		element = driver.findElement(clkBack);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void expJsonFromat() throws Exception{
		
		element = driver.findElement(expJson);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void clkCloseButton() throws Exception{
		
		element = driver.findElement(clkClose1);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void expandWidget1() throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[contains(text(),'Amount vary by Age Group')]//preceding::div[@class='deactivated')]"));
		baseClass.mouseHoverEventToElemnt(element);
		baseClass.scrollIntoPageElementandClick(element);
		
		/*element = driver.findElement(By.xpath("//*[@class='deactivated')]"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.mouseHoverEventToElemnt(element);
		*/
		element = driver.findElement(By.xpath("//*[contains(text(),'Amount vary by Age Group')]//following::div[@class='header-control full-screen-button']"));
		baseClass.mouseHoverEventToElemnt(element);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);	
		
	}
	
	public void clkControlDownlaod() throws Exception{
		
		baseClass.waitForThreadToLoad(2);
	    element = driver.findElement(By.xpath("//div[@class='leftPane']/div[1]/div[@style='display: block;']//following::div[@id='controlDownload']"));	    
	    baseClass.scrollIntoViewPageElementandClick(element);
	}
	
	public void expXlsFormat() throws Exception{		
		 
		element = driver.findElement(expXls);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void exitFullScreenWidget() throws Exception{
		
		element = driver.findElement(exitFullScreen);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);		
		
	}	

}


