package com.aa.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aa.automation.BaseClass;
import com.aa.automation.UsersPage;

public class UsersTest {
	
	WebDriver driver;
	WebDriverWait wait;
	UsersPage usersPage;
	BaseClass baseClass;	

	public UsersTest(WebDriver driver,WebDriverWait wait) throws Exception
	{
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		this.wait = wait;
		baseClass = new BaseClass(driver,wait);
		usersPage = new  UsersPage(driver,wait);
	}

	//Method for Create New User(Dont Give any license when it is for Admin User)
		public void giveDetailsForCreatingUser(String UserName,String FirstName,String LastName,String Password,String Email,String License,String...RoleName) throws InterruptedException{
			baseClass.waitForThreadToLoad(2);
			usersPage.sendTextToUserNameInputBox(UserName);
			usersPage.sendTextToFirstNameInputBox(FirstName);
			usersPage.sendTextToLastNameInputBox(LastName);
			usersPage.sendTextToPasswordInputBox(Password);
			usersPage.sendTextToConfirmPasswordInputBox(Password);
			usersPage.sendTextToEmailInputBox(Email);
			usersPage.sendTextToConfirmEmailInputBox(Email);
			baseClass.scrollDownToMiddle(driver);
			usersPage.selectRoleTypeWithLicensePermission(License,RoleName);
			baseClass.waitForThreadToLoad(2);
			baseClass.scrollUpToPageStart(driver);
			
		}

	}


