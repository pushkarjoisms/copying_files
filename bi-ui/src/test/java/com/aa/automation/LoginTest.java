package com.aa.automation;

import com.aa.common.Utility;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class LoginTest {
	
	WebDriver driver;
	WebDriverWait wait;
	BaseClass baseClass;	
	LoginPage loginPage;
	UsersPage usersPage;
	private String URL;

	private void getBotInsightURL() {
	  Utility util = Utility.getInstance();
		URL = util.getHost() + "/botinsight";
	}

	public LoginTest(WebDriver driver, WebDriverWait wait) throws Exception
	{
	  this.getBotInsightURL();
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		this.wait = wait;
		baseClass = new BaseClass(driver, wait);
		loginPage = new LoginPage(driver, wait);
		usersPage = new UsersPage(driver, wait);
	}

	public void loginToBotInsight_ValidCredentials_VerifyLoginSuccessfully() throws Exception
	{
		//System.out.println("Executing Method No: 2");
		
		//Arrange	
		baseClass.enterURLIntoBrowser(URL);
		baseClass.waitForThreadToLoad(2);
		
		//Act
		usersPage.loginWithNewCredentials(BaseClass.GetPropertyName("Username"), BaseClass.GetPropertyName("Password"));
		//usersPage.giveSecurityDeatailsAfterLogin(BaseClass.GetPropertyName("PassWord"), BaseClass.GetPropertyName("NewPassword"), BaseClass.GetPropertyName("FirstQuestion"), BaseClass.GetPropertyName("FirstAnswer"), BaseClass.GetPropertyName("SecondQuestion"), BaseClass.GetPropertyName("SecondAnswer"), BaseClass.GetPropertyName("ThirdQuestion"), BaseClass.GetPropertyName("ThirdAnswer"));
		baseClass.waitForThreadToLoad(2);
		//baseClass.executeTaskFromAAEClient("LoginToBotCreator.atmx", "AAPlayer.exe");
		
		/*String URL = BaseClass.GetPropertyName("BIURL");
		baseClass.enterURLIntoBrowser(URL);
		baseClass.waitForThreadToLoad(5);
		baseClass.printMessageOnMessageBox("Entered BI URL");		
		String loginUserName = BaseClass.GetPropertyName("Username");		
		String loginPassword = BaseClass.GetPropertyName("Password");	
		loginPage.doLoginToCROrBotInsight(loginUserName, loginPassword);*/
		
		/*loginPage.enterUsername();
		loginPage.enterPassword();
		loginPage.checkKeepMeLoggedIn();

		//Act
		loginPage.login();*/

		//Assert
		String loginStatus = loginPage.getLoginStatus();
		Assert.assertEquals("img",loginStatus,"Login status is not Matching");
	}
	
	public void loginToBotInsight_ValidCredentials_VerifyLogin() throws Exception
	{
		//System.out.println("Executing Method No: 2");
		
		//Arrange
		baseClass.enterURLIntoBrowser(URL);
		baseClass.waitForThreadToLoad(2);
		
		//Act
		usersPage.loginWithNewCredentials(BaseClass.GetPropertyName("UserName1"), BaseClass.GetPropertyName("PassWord1"));
		usersPage.giveSecurityDeatailsAfterLogin(BaseClass.GetPropertyName("PassWord1"), BaseClass.GetPropertyName("NewPassword"), BaseClass.GetPropertyName("FirstQuestion"), BaseClass.GetPropertyName("FirstAnswer"), BaseClass.GetPropertyName("SecondQuestion"), BaseClass.GetPropertyName("SecondAnswer"), BaseClass.GetPropertyName("ThirdQuestion"), BaseClass.GetPropertyName("ThirdAnswer"));
		baseClass.waitForThreadToLoad(2);
	}
	
	public void loginToBotInsight_ValidCredentials_VerifyExpertLogin() throws Exception
	{
		//System.out.println("Executing Method No: 2");
		
		//Arrange
		baseClass.enterURLIntoBrowser(URL);
		baseClass.waitForThreadToLoad(2);
		
		//Act
		usersPage.loginWithNewCredentials(BaseClass.GetPropertyName("UserName2"), BaseClass.GetPropertyName("PassWord2"));
		usersPage.giveSecurityDeatailsAfterLogin(BaseClass.GetPropertyName("PassWord2"), BaseClass.GetPropertyName("NewPassword"), BaseClass.GetPropertyName("FirstQuestion"), BaseClass.GetPropertyName("FirstAnswer"), BaseClass.GetPropertyName("SecondQuestion"), BaseClass.GetPropertyName("SecondAnswer"), BaseClass.GetPropertyName("ThirdQuestion"), BaseClass.GetPropertyName("ThirdAnswer"));
		baseClass.waitForThreadToLoad(2);
	}
	
	public void loginToBotInsight_ValidCredentials_VerifyConsumerLogin() throws Exception
	{
		//System.out.println("Executing Method No: 2");
		
		//Arrange
		baseClass.enterURLIntoBrowser(URL);
		baseClass.waitForThreadToLoad(2);
		
		//Act
		usersPage.loginWithNewCredentials(BaseClass.GetPropertyName("UserName3"), BaseClass.GetPropertyName("PassWord3"));
		usersPage.giveSecurityDeatailsAfterLogin(BaseClass.GetPropertyName("PassWord3"), BaseClass.GetPropertyName("NewPassword"), BaseClass.GetPropertyName("FirstQuestion"), BaseClass.GetPropertyName("FirstAnswer"), BaseClass.GetPropertyName("SecondQuestion"), BaseClass.GetPropertyName("SecondAnswer"), BaseClass.GetPropertyName("ThirdQuestion"), BaseClass.GetPropertyName("ThirdAnswer"));
		baseClass.waitForThreadToLoad(2);
	}
	
	public void loginToBotInsight_ValidCredentials_VerifyRoleBasedAccess() throws Exception
	{
		//System.out.println("Executing Method No: 2");
		
		//Arrange
		baseClass.enterURLIntoBrowser(URL);
		baseClass.waitForThreadToLoad(2);
		
		//Act
		usersPage.loginWithNewCredentials(BaseClass.GetPropertyName("UserName4"), BaseClass.GetPropertyName("PassWord4"));
		usersPage.giveSecurityDeatailsAfterLogin(BaseClass.GetPropertyName("PassWord4"), BaseClass.GetPropertyName("NewPassword"), BaseClass.GetPropertyName("FirstQuestion"), BaseClass.GetPropertyName("FirstAnswer"), BaseClass.GetPropertyName("SecondQuestion"), BaseClass.GetPropertyName("SecondAnswer"), BaseClass.GetPropertyName("ThirdQuestion"), BaseClass.GetPropertyName("ThirdAnswer"));
		baseClass.waitForThreadToLoad(2);
	}
	
	public void loginToControlRoom_WithValidCredentials() throws Exception{	
		
		System.out.println("Executing Method No: 2");
		
		//Arrange
		String loginUserName = BaseClass.GetPropertyName("UserName_Admin");		
		String loginPassword = BaseClass.GetPropertyName("PassWord_Admin");

		//Act
		//baseClass.waitForThreadToLoad(2);
		loginPage.doLoginToControlRoom(loginUserName, loginPassword);
		
		//Assert
		//String loginStatus = loginPage.getLoginStatus();
		//Assert.assertEquals("img",loginStatus,"Login status is not matching");
		//Assert.assertEquals(loginPage.loggedInUserStatus(), true, "User not logged in successfully");
	}
	
public void loginToControlRoom_AdminCredentials() throws Exception{	
		
		//System.out.println("Executing Method No: 2");
		
		//Arrange		
		String URL = BaseClass.GetPropertyName("CRURL");
		baseClass.enterURLIntoBrowser(URL);
		baseClass.waitForThreadToLoad(2);
		String loginUserName = BaseClass.GetPropertyName("UserName_Admin");		
		String loginPassword = BaseClass.GetPropertyName("PassWord_Admin");

		//Act
		loginPage.doLoginToControlRoom(loginUserName, loginPassword);
		
		//Assert
		//String loginStatus = loginPage.getLoginStatus();
		//Assert.assertEquals("img",loginStatus,"Login status is not matching");
		//Assert.assertEquals(loginPage.loggedInUserStatus(), true, "User not logged in successfully");
	}
}
