package com.aa.automation;

import javax.swing.JOptionPane;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aa.automation.BaseClass;
import com.aa.automation.HomePage;
import com.aa.automation.LoginPage;
import com.aa.automation.RolesPage;
import com.aa.automation.SchedulePage;
import com.aa.automation.UsersPage;

public class RolesTest {
	
	WebDriver driver;
	WebDriverWait wait;
	RolesPage rolesPage;
	BaseClass baseClass;
	HomePage homePage;
	UsersPage usersPage;
	UsersTest usersTest;
	LoginPage loginPage;
	SchedulePage schedulePage;
	
	public RolesTest(WebDriver driver, WebDriverWait wait) throws Exception
	{
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		this.wait = wait;
		baseClass = new BaseClass(driver,wait);
		rolesPage = new  RolesPage(driver,wait);
		homePage = new HomePage(driver, wait);
		usersPage = new UsersPage(driver,wait);
		usersTest = new UsersTest(driver,wait);
		loginPage = new LoginPage(driver,wait);
		schedulePage = new SchedulePage(driver, wait);
	}
	
	 public void  CreateRoleAndUser_AssignedRoleToUser_ForBotInsight() throws Exception
	 {
		 
		 //Arrange
		homePage.accessAdministrationTab();
		homePage.clickOnUsersLink();
		String Username = BaseClass.GetPropertyName("UserName");
		if(usersPage.getColumnDataFromTable("Username",Username) == true){
			 usersPage.deleteUserFromDataTable(Username);
			 usersPage.clickAcceptDeleteRowButton();	
			 }	
		baseClass.waitForThreadToLoad(2);
		homePage.clickOnRolesLink();
		String Rolename = BaseClass.GetPropertyName("RoleName");
		 if(rolesPage.getColumnDataFromTable("Role name",Rolename) == true){
			 rolesPage.deleteRoleFromDataTable(Rolename);
			 rolesPage.clickAcceptDeleteRowButton();	
			 }		 
		 rolesPage.clickOnCreateRoleButton();
		 rolesPage.giveDetailsForCreatingRole(Rolename, "Description","View my scheduled bots","Schedule my bots to run", "Edit my scheduled activity","Delete my scheduled activity","Run my bots");
		 rolesPage.clickOnNextButton();	
		 rolesPage.clickOnMyTasksForSelectAll();	
		 rolesPage.clickOnNextButton();
		 //rolesPage.clickOnSelectAllDevicesCheckBox();		
		// rolesPage.clickOnMoveToNextTableButton();	 
		 //rolesPage.clickOnSaveChangesButton();
		 rolesPage.clickOnCreateRoleButton();
		 usersPage.clickOnOnCreateUserButton();		
		 usersTest.giveDetailsForCreatingUser(Username,BaseClass.GetPropertyName("FirstName"), BaseClass.GetPropertyName("LastName"), BaseClass.GetPropertyName("PassWord"), BaseClass.GetPropertyName("EmailId"),"Bot creator","AAE_Bot Insight Admin", Rolename);		
		 usersPage.clickOnCreateUserButton();	  
		 usersPage.clickOnHeaderBarTrayButton();
		 usersPage.clickOnLogOutButton();
		 baseClass.waitForThreadToLoad(2);
		 
		 //Act	
		 
		/* homePage.accessActivityTab();	
		 homePage.clickOnScheduledLink();
		 
		 //Assert
		 schedulePage.createOneTimeScheduleToExecuteTask("DemoTask", "TaskBots",2);	  
		 boolean flag = rolesPage.verifyRoleAccessPermission("DemoTask","icon aa-icon aa-icon-action-view icon--animate-none icon--block");	 
		 //JOptionPane.showMessageDialog(null, flag);
		 Assert.assertTrue(flag)*/;
	  
	 }
		   
	 public void  CreateRoleAndUser_AssignedRoleToUser_ForBotInsight1() throws Exception
	 {
		 
		 //Arrange
		 homePage.accessAdministrationTab();
			homePage.clickOnUsersLink();
			String Username = BaseClass.GetPropertyName("UserName1");
			if(usersPage.getColumnDataFromTable("Username",Username) == true){
				 usersPage.deleteUserFromDataTable(Username);
				 usersPage.clickAcceptDeleteRowButton();	
				 }	
			baseClass.waitForThreadToLoad(2);
			homePage.clickOnRolesLink();
			String Rolename = BaseClass.GetPropertyName("RoleName1");
			 if(rolesPage.getColumnDataFromTable("Role name",Rolename) == true){
				 rolesPage.deleteRoleFromDataTable(Rolename);
				 rolesPage.clickAcceptDeleteRowButton();	
				 }		 
			 rolesPage.clickOnCreateRoleButton();
		 rolesPage.giveDetailsForCreatingRole(Rolename, "Description","View my scheduled bots","Schedule my bots to run", "Edit my scheduled activity","Delete my scheduled activity","Run my bots");
		 rolesPage.clickOnNextButton();
		 schedulePage.expandMyTaskFolderFromTree();
		 schedulePage.accessSubFolderFromTree("resources");		 
		 rolesPage.clickOnresourcesForSelectAll();	
		 rolesPage.clickOnNextButton();
		 //rolesPage.clickOnSelectAllDevicesCheckBox();		
		 //rolesPage.clickOnMoveToNextTableButton();	 
		 rolesPage.clickOnCreateRoleButton();	 
		 usersPage.clickOnOnCreateUserButton();		
		 usersTest.giveDetailsForCreatingUser(Username,BaseClass.GetPropertyName("FirstName1"), BaseClass.GetPropertyName("LastName1"), BaseClass.GetPropertyName("PassWord1"), BaseClass.GetPropertyName("EmailId1"),"Bot runner","AAE_Bot Insight Admin", Rolename);		
		 usersPage.clickOnCreateUserButton();	  
		 //usersPage.clickOnHeaderBarTrayButton();
		// usersPage.clickOnLogOutButton();
		 //baseClass.waitForThreadToLoad(5);
		 
		
	  
	 }
	 
	 public void  CreateRoleAndUser_AssignedRoleToUser_ForRoleBasedAccess() throws Exception
	 {
		 
		 //Arrange
		homePage.accessAdministrationTab();		
		homePage.clickOnUsersLink();
		String Username = BaseClass.GetPropertyName("UserName4");
		if(usersPage.getColumnDataFromTable("Username",Username) == true){
			 usersPage.deleteUserFromDataTable(Username);
			 usersPage.clickAcceptDeleteRowButton();	
			 }	
		baseClass.waitForThreadToLoad(2);
		homePage.clickOnRolesLink();
		String Rolename = BaseClass.GetPropertyName("RoleName4");
		 if(rolesPage.getColumnDataFromTable("Role name",Rolename) == true){
			 rolesPage.deleteRoleFromDataTable(Rolename);
			 rolesPage.clickAcceptDeleteRowButton();	
			 }		 
		 rolesPage.clickOnCreateRoleButton();
		 rolesPage.giveDetailsForCreatingRole(Rolename, "Description","View my scheduled bots","Schedule my bots to run", "Edit my scheduled activity","Delete my scheduled activity","Run my bots");
		 rolesPage.clickOnNextButton();
		 schedulePage.expandMyTaskFolderFromTree();
		 schedulePage.accessSubFolderFromTree("Tasks");		 
		 rolesPage.clickOnTasksForSelectAll();	
		 rolesPage.clickOnNextButton();
		 //rolesPage.clickOnSelectAllDevicesCheckBox();		
		 //rolesPage.clickOnMoveToNextTableButton();	 
		 rolesPage.clickOnCreateRoleButton();	 
		 usersPage.clickOnOnCreateUserButton();		
		 usersTest.giveDetailsForCreatingUser(Username,BaseClass.GetPropertyName("FirstName4"), BaseClass.GetPropertyName("LastName4"), BaseClass.GetPropertyName("PassWord4"), BaseClass.GetPropertyName("EmailId4"),"Bot runner","AAE_Bot Insight Admin", Rolename);		
		 usersPage.clickOnCreateUserButton();	  
		 //usersPage.clickOnHeaderBarTrayButton();
		 // usersPage.clickOnLogOutButton();
		 //baseClass.waitForThreadToLoad(5);
		 
		
	  
	 }
	 
	 
	 
	 
	 public void CreateSchedule_RunBotInControlRoom() throws Exception{
		 
		 //Act	
		 
			 homePage.accessActivityTab();	
			 homePage.clickOnScheduledLink();
			 baseClass.refreshThePage();
			 baseClass.waitForThreadToLoad(5);
			 
			 //Assert
			 schedulePage.createAndExecuteTask("ProductionDashboardTask", "resources","botinsightrunuser");	  
			 //boolean flag = rolesPage.verifyRoleAccessPermission("DemoTask","icon aa-icon aa-icon-action-view icon--animate-none icon--block");	 
			 //JOptionPane.showMessageDialog(null, flag);
			 //Assert.assertTrue(flag);
	 }
	 
	 public void CreateSchedule_RunBotInControlRoom_RBA() throws Exception{
		 
		 //Act	
		 
			 homePage.accessActivityTab();	
			 homePage.clickOnScheduledLink();
			 baseClass.refreshThePage();
			 baseClass.waitForThreadToLoad(5);
			 
			 //Assert
			 schedulePage.createAndExecuteTask("ProductionDashboardTask_RBA", "Tasks","botinsightrbauser");	  
			 //boolean flag = rolesPage.verifyRoleAccessPermission("DemoTask","icon aa-icon aa-icon-action-view icon--animate-none icon--block");	 
			 //JOptionPane.showMessageDialog(null, flag);
			 //Assert.assertTrue(flag);
	 }
	 
	 public void  CreateExpertRoleAndUser_AssignedRoleToUser_ForBotInsight() throws Exception
	 {
		 
		 //Arrange
		homePage.accessAdministrationTab();		
		homePage.clickOnUsersLink();
		String Username = BaseClass.GetPropertyName("UserName2");
		if(usersPage.getColumnDataFromTable("Username",Username) == true){
			 usersPage.deleteUserFromDataTable(Username);
			 usersPage.clickAcceptDeleteRowButton();	
			 }	
		baseClass.waitForThreadToLoad(2);
		homePage.clickOnRolesLink();
		String Rolename = BaseClass.GetPropertyName("RoleName2");
		 if(rolesPage.getColumnDataFromTable("Role name",Rolename) == true){
			 rolesPage.deleteRoleFromDataTable(Rolename);
			 rolesPage.clickAcceptDeleteRowButton();	
			 }	 
		 rolesPage.clickOnCreateRoleButton();
		 rolesPage.giveDetailsForCreatingRole(Rolename, "Description","View my scheduled bots","Schedule my bots to run", "Edit my scheduled activity","Delete my scheduled activity","Run my bots");
		 rolesPage.clickOnNextButton();	
		 schedulePage.expandMyTaskFolderFromTree();
		 schedulePage.accessSubFolderFromTree("resources");		 
		 rolesPage.clickOnresourcesForSelectAll();		 
		 rolesPage.clickOnNextButton();
		 //rolesPage.clickOnSelectAllDevicesCheckBox();		
		 //rolesPage.clickOnMoveToNextTableButton();	 
		 rolesPage.clickOnCreateRoleButton();	 
		 usersPage.clickOnOnCreateUserButton();		
		 usersTest.giveDetailsForCreatingUser(Username,BaseClass.GetPropertyName("FirstName2"), BaseClass.GetPropertyName("LastName2"), BaseClass.GetPropertyName("PassWord2"), BaseClass.GetPropertyName("EmailId2"),"Bot creator","AAE_Bot Insight Expert", Rolename);		
		 usersPage.clickOnCreateUserButton();	  
		 usersPage.clickOnHeaderBarTrayButton();
		 usersPage.clickOnLogOutButton();
		 baseClass.waitForThreadToLoad(5);	 
	  
	 }
	 
	 public void  CreateConsumerRoleAndUser_AssignedRoleToUser_ForBotInsight() throws Exception
	 {
		 
		 //Arrange
		homePage.accessAdministrationTab();		
		homePage.clickOnUsersLink();
		String Username = BaseClass.GetPropertyName("UserName3");
		if(usersPage.getColumnDataFromTable("Username",Username) == true){
			 usersPage.deleteUserFromDataTable(Username);
			 usersPage.clickAcceptDeleteRowButton();	
			 }	
		baseClass.waitForThreadToLoad(2);
		homePage.clickOnRolesLink();
		String Rolename = BaseClass.GetPropertyName("RoleName3");
		 if(rolesPage.getColumnDataFromTable("Role name",Rolename) == true){
			 rolesPage.deleteRoleFromDataTable(Rolename);
			 rolesPage.clickAcceptDeleteRowButton();	
			 }	 
		 rolesPage.clickOnCreateRoleButton();
		 rolesPage.giveDetailsForCreatingRole(Rolename, "Description","View my scheduled bots","Schedule my bots to run", "Edit my scheduled activity","Delete my scheduled activity","Run my bots");
		 rolesPage.clickOnNextButton();	
		 schedulePage.expandMyTaskFolderFromTree();
		 schedulePage.accessSubFolderFromTree("resources");		 
		 rolesPage.clickOnresourcesForSelectAll();		 
		 rolesPage.clickOnNextButton();
		 //rolesPage.clickOnSelectAllDevicesCheckBox();		
		 //rolesPage.clickOnMoveToNextTableButton();	 
		 rolesPage.clickOnCreateRoleButton();	 
		 usersPage.clickOnOnCreateUserButton();		
		 usersTest.giveDetailsForCreatingUser(Username,BaseClass.GetPropertyName("FirstName3"), BaseClass.GetPropertyName("LastName3"), BaseClass.GetPropertyName("PassWord3"), BaseClass.GetPropertyName("EmailId3"),"Bot runner","AAE_Bot Insight Consumer", Rolename);		
		 usersPage.clickOnCreateUserButton();	  
		 usersPage.clickOnHeaderBarTrayButton();
		 usersPage.clickOnLogOutButton();
		 baseClass.waitForThreadToLoad(5);	 
		
	  
	 }
	 
    
	 public void  VerifyViewPermissionOnScheduledBots_SelectedDevicesWhenUserHaveSchedule_BasedOnRolePermission() throws Exception
	 {
		 System.out.println("Executing Method No: 59");
		 
		 //Arrange
		homePage.accessAdministrationTab();
		homePage.clickOnUsersLink();
		 if(usersPage.getColumnDataFromTable("USERNAME",BaseClass.GetPropertyName("UserName")) == true){
			 usersPage.deleteUserFromDataTable(BaseClass.GetPropertyName("UserName"));
			 usersPage.clickAcceptDeleteRowButton();	
			 }	
		baseClass.waitForThreadToLoad(2);
		homePage.clickOnRolesLink();	  
		 if(rolesPage.getColumnDataFromTable("ROLE NAME",BaseClass.GetPropertyName("RoleName")) == true){
			 rolesPage.deleteRoleFromDataTable(BaseClass.GetPropertyName("RoleName"));
			 rolesPage.clickAcceptDeleteRowButton();	
			 }		 
		 rolesPage.clickOnCreateRoleButton();
		 rolesPage.giveDetailsForCreatingRole("ScheduledMyBotsToRun", "Description","View my scheduled bots","Schedule my bots to run", "Edit my scheduled activity","Delete my scheduled activity","View my bots","Run my bots");
		 rolesPage.clickOnNextButton();	
		 rolesPage.clickOnMyTasksForSelectAll();	
		 rolesPage.clickOnNextButton();
		 rolesPage.clickOnSelectAllDevicesCheckBox();		
		 rolesPage.clickOnMoveToNextTableButton();	 
		 rolesPage.clickOnCreateRoleButton();	 
		 usersPage.clickOnOnCreateUserButton();		
		 usersTest.giveDetailsForCreatingUser(BaseClass.GetPropertyName("UserName"),BaseClass.GetPropertyName("FirstName"), BaseClass.GetPropertyName("LastName"), BaseClass.GetPropertyName("Password"), BaseClass.GetPropertyName("Email"),"None", "ScheduledMyBotsToRun");		
		 usersPage.clickOnCreateUserButton();	  
		 usersPage.clickOnHeaderBarTrayButton();
		 usersPage.clickOnLogOutButton();
		 
		 //Act		 
		 usersPage.loginWithNewCredentials(BaseClass.GetPropertyName("UserName"), BaseClass.GetPropertyName("Password"));
		 usersPage.giveSecurityDeatailsAfterLogin(BaseClass.GetPropertyName("Password"), BaseClass.GetPropertyName("NewPassword"), BaseClass.GetPropertyName("FirstQuestion"), BaseClass.GetPropertyName("FirstAnswer"), BaseClass.GetPropertyName("SecondQuestion"), BaseClass.GetPropertyName("SecondAnswer"), BaseClass.GetPropertyName("ThirdQuestion"), BaseClass.GetPropertyName("ThirdAnswer"));
		 homePage.accessActivityTab();	
		 homePage.clickOnScheduledLink();
		 
		 //Assert
		 schedulePage.createOneTimeScheduleToExecuteTask("DemoTask", "TaskBots",2);	  
		 boolean flag = rolesPage.verifyRoleAccessPermission("DemoTask","icon aa-icon aa-icon-action-view icon--animate-none icon--block");	 
		 //JOptionPane.showMessageDialog(null, flag);
		 Assert.assertTrue(flag);
	  
	 }
	 
	 public void  VerifyEditPermissionOnScheduledBots_SelectedDevicesWhenUserHaveSchedule_BasedOnRolePermission() throws Exception
	 {
		 System.out.println("Executing Method No: 59");
		 
		 //Arrange
		homePage.accessAdministrationTab();
		homePage.clickOnUsersLink();
		 if(usersPage.getColumnDataFromTable("USERNAME",BaseClass.GetPropertyName("UserName")) == true){
			 usersPage.deleteUserFromDataTable(BaseClass.GetPropertyName("UserName"));
			 usersPage.clickAcceptDeleteRowButton();	
			 }	
		baseClass.waitForThreadToLoad(2);
		homePage.clickOnRolesLink();	  
		 if(rolesPage.getColumnDataFromTable("ROLE NAME",BaseClass.GetPropertyName("RoleName")) == true){
			 rolesPage.deleteRoleFromDataTable(BaseClass.GetPropertyName("RoleName"));
			 rolesPage.clickAcceptDeleteRowButton();	
			 }		 
		 rolesPage.clickOnCreateRoleButton();
		 rolesPage.giveDetailsForCreatingRole("ScheduledMyBotsToRun", "Description","View my scheduled bots","Schedule my bots to run","Delete my scheduled activity","View and manage ALL scheduled activity","View my bots","Run my bots");
		 rolesPage.clickOnNextButton();	
		 rolesPage.clickOnMyTasksForSelectAll();	
		 rolesPage.clickOnNextButton();
		 rolesPage.clickOnSelectAllDevicesCheckBox();		
		 rolesPage.clickOnMoveToNextTableButton();	 
		 rolesPage.clickOnCreateRoleButton();	 
		 usersPage.clickOnOnCreateUserButton();		
		 usersTest.giveDetailsForCreatingUser(BaseClass.GetPropertyName("UserName"),BaseClass.GetPropertyName("FirstName"), BaseClass.GetPropertyName("LastName"), BaseClass.GetPropertyName("Password"), BaseClass.GetPropertyName("Email"),"None", "ScheduledMyBotsToRun");		
		 usersPage.clickOnCreateUserButton();	  
		 usersPage.clickOnHeaderBarTrayButton();
		 usersPage.clickOnLogOutButton();
		 
		 //Act		 
		 usersPage.loginWithNewCredentials(BaseClass.GetPropertyName("usrName_Admin1"), BaseClass.GetPropertyName("usrPassword_Admin1"));
		 //usersPage.giveSecurityDeatailsAfterLogin(BaseClass.GetPropertyName("Password"), BaseClass.GetPropertyName("NewPassword"), BaseClass.GetPropertyName("FirstQuestion"), BaseClass.GetPropertyName("FirstAnswer"), BaseClass.GetPropertyName("SecondQuestion"), BaseClass.GetPropertyName("SecondAnswer"), BaseClass.GetPropertyName("ThirdQuestion"), BaseClass.GetPropertyName("ThirdAnswer"));
		 homePage.accessActivityTab();	
		 homePage.clickOnScheduledLink();
		 
		 //Assert
		 schedulePage.createOneTimeScheduleToExecuteTask("DemoTask", "TaskBots",2);	  
		 boolean flag = rolesPage.verifyRoleAccessPermission("DemoTask","icon aa-icon aa-icon-action-view icon--animate-none icon--block");	 
		 //JOptionPane.showMessageDialog(null, flag);
		 Assert.assertTrue(flag);
	  
	 }


}
