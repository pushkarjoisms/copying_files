package com.aa.common;

public enum Role{
  CRADMIN  (0),
  Basic(2),
  BotInsight(12);

  private final int role;

  Role(int role) {
    this.role = role;
  }

  public int getRole() {
    return this.role;
  }
}
