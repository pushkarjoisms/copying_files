package com.aa.automation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class OperationAnalyticsPage {

	WebDriver driver;
	WebDriverWait wait;
	WebElement element;
	BaseClass baseClass;
	ArrayList<String> tabs;

	By cUsername = By.name("UserName");
	By cPassword = By.name("Password");
	By cLogin = By.xpath("//span[@id='loginButton-btnEl']");
	By cRoom = By.xpath("//label[text()='Control Room']");
	By opRoom = By.xpath("//span[text()='Operations Room']");	
	By opAnalytics = By.xpath("//span[@id='LinkButton-1442-btnInnerEl']");	
	By opIframe = By.xpath("//iframe[@id='ext-element-43']");
	By opDashboard = By.xpath("//span[text()='AUDIT TRAIL Dashboard']");
	By titleDB = By.xpath("//span[@class='dashboard-title']");
	By lbWidget = By.xpath("//*[@id='cid-c51']/div[3]/div[1]/div[1]/div[1]/div/span");
	By lbWidget1 = By.xpath("//*[@id='cid-c47']/div[3]/div[1]/div[1]/div[1]/div/span");
	By lbWidget2 = By.xpath("//*[@id='cid-c46']/div[3]/div[1]/div[1]/div[1]/div/span");	
	By opDropdown = By.xpath("//*[@id='simple-dropdown']");
	By opAudit = By.xpath("//*[@class='dropdown-menu']");	
	By userMenu = By.xpath("//*[@id='usermenu-1108-btnIconEl']");
	By cLogout = By.xpath("//*[@id='hhp_um_Logout-textEl']");

	public OperationAnalyticsPage(WebDriver driver,WebDriverWait wait) throws Exception{
		this.driver = driver;
		wait = new WebDriverWait(driver,90);
		this.wait = wait;
		baseClass = new BaseClass(driver, wait);
	}

	public void SwitchtoWindow() throws Exception{

		((JavascriptExecutor)driver).executeScript("window.open()");
		tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.get(BaseClass.GetPropertyName("crurl")+"controlroom");
	}

	public void LoginToControlRoom() throws Exception{

		driver.manage().window().maximize();
		element = driver.findElement(cUsername);
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, BaseClass.GetPropertyName("crusername"));		
		element = driver.findElement(cPassword);
		baseClass.enterDataIntoEditbox(element, BaseClass.GetPropertyName("crpassword"));
		element = driver.findElement(cLogin);
		baseClass.clickOnWebElement(element);
		//driver.findElement(cLogin).sendKeys(Keys.ENTER); 
	}

	public void ClickOnOperationAnalytics()throws Exception{

		element = driver.findElement(cRoom);
		baseClass.waitForElementToBePresent(element);
		driver.findElement(opRoom).sendKeys(Keys.ENTER);
		driver.findElement(opAnalytics).sendKeys(Keys.ENTER);	
	}

	public void SwitchToParentFrame() throws Exception{

		baseClass.waitForThreadToLoad(2);
		/*List<WebElement> iframes = driver.findElements(By.xpath("//iframe"));
        System.out.println(iframes.size());
        JOptionPane.showMessageDialog(null, iframes.size());*/       
		driver.switchTo().frame(0).switchTo().frame(0);		 
	}

	public void ClickDropdown1()throws Exception{

		element = driver.findElement(opDropdown);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
		element = driver.findElement(By.xpath("//*[@class='dropdown-menu']/li[1]"));
		baseClass.clickOnWebElement(element);
	}

	public void SwitchtoParentWindow() throws Exception{

		baseClass.waitForThreadToLoad(2);
		driver.switchTo().window(tabs.get(0));
	}

	public void close(){

		baseClass.closeBrowser();
	}	
	
	public void SwitchtoFrame(){
		
		baseClass.switchToChildFrame("dashboardContent");		
	}
	
	public String dashboardTitleText(){
		
		element = driver.findElement(titleDB);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);
	}
	
	public String getWidgetText(){
		
		element = driver.findElement(lbWidget);
		return baseClass.getTextFromWebElement(element);
	}
	
	public void SwitchtoParentFrame() throws InterruptedException{
		
		baseClass.waitForThreadToLoad(2);
		driver.switchTo().parentFrame();
	}

	public void ClickDropdown2(){
		
		element = driver.findElement(opDropdown);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
		element = driver.findElement(By.xpath("//*[@class='dropdown-menu']/li[2]"));
		baseClass.clickOnWebElement(element);
	}

	public String getWidgetText1(){
		
		element = driver.findElement(lbWidget1);
		return baseClass.getTextFromWebElement(element);
	}
	
	public void ClickDropdown3(){

		element = driver.findElement(opDropdown);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
		element = driver.findElement(By.xpath("//*[@class='dropdown-menu']/li[3]"));
		baseClass.clickOnWebElement(element);
	}
	
	public String getWidgetText2(){
		
		element = driver.findElement(lbWidget2);
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}
	
	public void SwitchtoDefault() throws InterruptedException{
		
		baseClass.waitForThreadToLoad(2);	 			 				
		driver.switchTo().defaultContent();
		
	}
	
	public void ControlRoomLogout() throws InterruptedException, IOException
	{
		baseClass.waitForThreadToLoad(2);
		driver.findElement(userMenu).sendKeys(Keys.ENTER); 		
		driver.findElement(cLogout).sendKeys(Keys.ENTER);
	}	

	public void clickToFullScreen(String labelText) throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[contains(text(),'"+labelText+"')]"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoViewPageElementandClick(element);
		baseClass.waitForThreadToLoad(2);
		baseClass.clickOnWebElement(element);
		element = driver.findElement(By.xpath("//*[contains(text(),'"+labelText+"')]//following::div[@class='header-control full-screen-button']"));
		baseClass.mouseHoverEventToElemnt(element);
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);		
	}
	
	public Boolean checkLeftPanelControls(String labelText){		
		
		baseClass.waitForThreadToLoad(2);
		boolean flag = false;
		flag = driver.findElement(By.xpath("//*[@class='leftPane']//following::div[@style='display: block;']")).isDisplayed();
		return flag;		
	}	
	
	public String getWidgetPrimaryLabelText(String labelText){
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[contains(text(),'"+labelText+"')]//following::div[@class='string-label primary']"));
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}
	
	public String getWidgetPrimaryLabelNumber(String labelText)throws Exception {
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[contains(text(),'"+labelText+"')]//following::div[@class='number-label primary']"));
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}
	
	public String getSubHeadNotificationText(String labelText)throws Exception {
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[contains(text(),'"+labelText+"')]//following::div[@class='subhead-notifications left']"));
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}
	
	public String getWidgetInnerContainserText(String labelText)throws Exception  {
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//*[contains(text(),'"+labelText+"')]//following::div[@class='innerContainer']"));
		baseClass.waitForElementToBePresent(element);
		return baseClass.getTextFromWebElement(element);		
	}
	
	public Boolean checkBottomWidgetLabels(String labelText){		
		
		baseClass.waitForThreadToLoad(2);
		boolean flag = false;
		flag = driver.findElement(By.xpath("//*[contains(text(),'"+labelText+"')]//following::div[@class='widgetLabel bottom']")).isDisplayed();
		return flag;		
	}
	
	public void exitFromFullScreen(String labelText) throws Exception{
		
		baseClass.waitForThreadToLoad(2);
		element = driver.findElement(By.xpath("//div[@class='exitFullscreen']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);		
	}

}


