package com.aa.automation;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class UsersPage {
	
	WebDriver driver;
	WebDriverWait wait;
	BaseClass baseClass;
	WebElement element;
	JavascriptExecutor js;	
	
	public UsersPage(WebDriver driver,WebDriverWait wait) throws Exception {
		this.driver = driver;
		wait = new WebDriverWait(driver,60);
		this.driver = driver;
		baseClass = new BaseClass(driver,wait);
		js = (JavascriptExecutor) driver;
	}
	
	//User Name Input Box        
	@FindBy(how=How.XPATH, using="//input[contains(@name,'username')]")
	private WebElement userNameInputBox;
	public void sendTextToUserNameInputBox(String Username){
		element = driver.findElement(By.xpath("//input[contains(@name,'username')]"));
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, Username);
	}

	//First Name Input Box
	@FindBy(how=How.XPATH, using="//input[@name='firstName']")
	private WebElement firstNameInputBox;

	public void sendTextToFirstNameInputBox(String firstName){
		element = driver.findElement(By.xpath("//input[@name='firstName']"));
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, firstName);
	}

	//Last Name Input Box
	@FindBy(how=How.XPATH, using="//input[@name='lastName']")
	private WebElement LastNameInputBox;

	public void sendTextToLastNameInputBox(String lastName){
		element = driver.findElement(By.xpath("//input[@name='lastName']"));
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, lastName);
	}
		

	//Password Input Box   
	@FindBy(how=How.XPATH, using="//input[@name='password']")
	private WebElement passwordInputBox;

	public void sendTextToPasswordInputBox(String password){
		element = driver.findElement(By.xpath("//input[@name='password']"));
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, password);		
	}

	//Confirm password Input Box    
	@FindBy(how=How.XPATH, using="//input[@name='passwordConfirm']")
	private WebElement  ConfirmPasswordInputBox;

	public void sendTextToConfirmPasswordInputBox(String passwordConfirm){
		
		element = driver.findElement(By.xpath("//input[@name='passwordConfirm']"));
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, passwordConfirm);
		
	}

	//Email Input Box
	@FindBy(how=How.XPATH, using="//input[@name='email']")
	private WebElement EmailInputBox;

	public void sendTextToEmailInputBox(String Email){
		
		element = driver.findElement(By.xpath("//input[@name='email']"));
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, Email);
	}

	//Email Confirm Input Box
	@FindBy(how=How.XPATH, using="//input[@name='emailConfirm']")
	private WebElement EmailConfirmInputBox;

	public void sendTextToConfirmEmailInputBox(String EmailConfirm ){
		
		element = driver.findElement(By.xpath("//input[@name='emailConfirm']"));
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, EmailConfirm);
		
	}

	//Move To Next Table Box          
	@FindBy(how = How.XPATH, using = "//div[@class='commandbutton commandbutton--theme-default commandbutton--fill commandbutton--not-recommended']//div[@class='commandbutton-button-label']")
	private WebElement MoveToNextTableBox ;

	public void ClickMoveToNextTableBox(){
		baseClass.waitForElementToBeClickable(MoveToNextTableBox);
		MoveToNextTableBox.click();
	} 

	//Bot Runner Button                  
	@FindBy(how=How.XPATH, using="//*[@name='licenseradio-0']/parent::div")
	private WebElement BotRunnerButton;

	public void ClickBotRunnerButton(String text){
		
		element = driver.findElement(By.xpath("//*[contains(@title,'"+text+"')]//div[@class='radioinput-control']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	} 

	//Bot Creator Button                  
	@FindBy(how=How.XPATH, using="//*[@name='licenseradio-1']/parent::div")
	private WebElement BotCreatorButton;

	public void ClickBotCreatorButton(String text){
		
		element = driver.findElement(By.xpath("//*[contains(@title,'"+text+"')]//div[@class='radioinput-control']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}
	
	public void selectRoleTypeWithLicensePermission(String License,String...RoleName){
		
		for(String role:RoleName){
			String AddRoleXpath="//*[text()='"+role+"']/preceding::div[3]//div[@class='checkboxinput-control']";  
			WebElement AddRoleCheckBox=driver.findElement(By.xpath(AddRoleXpath));
			baseClass.waitForElementToBeClickable(AddRoleCheckBox);
			baseClass.scrollIntoPageElementandClick(AddRoleCheckBox);		
		}
		
		WebElement MoveToNextTableBox = driver.findElement(By.xpath("//div[@class='commandbutton commandbutton--theme-default commandbutton--fill commandbutton--not-recommended']//div[@class='commandbutton-button-label']"));
		baseClass.waitForElementToBeClickable(MoveToNextTableBox);
		baseClass.scrollIntoPageElementandClick(MoveToNextTableBox);
		
		if(License=="Bot runner"){
			ClickBotRunnerButton(License);
		}else if(License=="Bot creator"){
			ClickBotCreatorButton(License);
		}else{
			System.out.println("Admin");
		}
	}

	// Get header And Its Column Data
		public boolean getColumnDataFromTable(String columnHeader, String row)throws InterruptedException {
			boolean TableRow = false;
			List<String> DataList = new ArrayList<String>();
			String startaXpath = "//*[@class='datatable-rows']/div[";
			String endaXpath = "/div[";
			int counta = 0;
			baseClass.waitForThreadToLoad(2);
			List<WebElement> allHeaders = driver.findElements(By.xpath("//*[@class='datatable-header']/div[1]/div"));
			String columnaStart = "//div[@class='datatable-header']/div[1]/div[";
			String columnaEnd = "]";
			int coulmnaCount = allHeaders.size();
			if (coulmnaCount > 1) {
				for (int i = 1; i < coulmnaCount - 1; i++) {
					String columnName = driver.findElement(By.xpath(columnaStart + i + columnaEnd)).getText();
					if (columnName.equalsIgnoreCase(columnHeader)) {
						List<WebElement> rows = driver.findElements(By.xpath("//*[@class='datatable-rows']/div"));
						int rowCount = rows.size();
						for (int j = 1; j <= rowCount; j++) {
							int a = counta + 1;
							String getData = driver.findElement(By.xpath(startaXpath + j + "]" + endaXpath + a + "]")).getText();
							DataList.add(getData);
						}
					} else {
						counta++;
					}
				}
			}
		//if (DataList.stream().filter(s -> s.equalsIgnoreCase(row)).findFirst().isPresent()){
			if (DataList.contains(row)) {
				return TableRow = true;
			} else {
				return TableRow = false;
			}

		}

		public void deleteUserFromDataTable(String userName) throws Exception
		{
			searchForUser(userName);
			mouseHoverEventToExpandActionMenu();
			element = driver.findElement(By.xpath("//div[@class='datatable-actions-content']//span[contains(@class,'icon aa-icon aa-icon-action-delete icon--animate-none icon--block')]"));
			baseClass.clickOnWebElement(element);
			baseClass.waitForThreadToLoad(2);
		}
		
		 public void mouseHoverEventToExpandActionMenu() throws InterruptedException
  		{
			 baseClass.waitForThreadToLoad(2);
  			WebElement mouseOverOnRow = driver.findElement(By.xpath("//div[contains(@class,'datatable-actions')]//div[1]"));
  			String strJavaScript = "var element = arguments[0];"
  					+ "var mouseEventObj = document.createEvent('MouseEvents');"
  					+ "mouseEventObj.initEvent( 'mouseover', true, true );"
  					+ "element.dispatchEvent(mouseEventObj);";
  			((JavascriptExecutor) driver).executeScript(strJavaScript,mouseOverOnRow);
  			baseClass.waitForThreadToLoad(2);
  		}
		
		private void searchForUser(String taskbot)
		{
			element = driver.findElement(By.xpath("//input[@placeholder='Search username']"));
			baseClass.enterDataIntoEditbox(element, taskbot);
			element.sendKeys(Keys.ENTER);
		}
		
		// Accept Delete Row
		@FindBy(how = How.XPATH, using = "//button[@name='accept']")
		private WebElement acceptDeleteRowButton;

		public void clickAcceptDeleteRowButton() {
			element = driver.findElement(By.xpath("//button[@name='accept']"));
			baseClass.waitForElementToBePresent(element);
			baseClass.moveToWebElementandClick(element);
		}
		
		// Clicking on create user
		@FindBy(how = How.XPATH, using = "//button[contains(., 'Create user')]")
		public WebElement creat_user;

		public void clickOnOnCreateUserButton() throws Exception {
			
			baseClass.waitForThreadToLoad(2);
			element = driver.findElement(By.xpath("//button[contains(., 'Create user')]"));
			baseClass.waitForElementToBeClickable(element);
			baseClass.scrollIntoPageElementandClick(element);
		}
		
		   
		//Create User Button
		    @FindBy(how=How.XPATH, using="//button[*]/div[text()='Create user']")
		    private WebElement CreateUser;
		    
		    public void clickOnCreateUserButton() throws InterruptedException{
		    element = driver.findElement(By.xpath("//button[*]/div[text()='Create user']"));
		    baseClass.waitForElementToBeClickable(element);
		    baseClass.scrollIntoPageElementandClick(element);		    
		    }
		    
		 // Header Bar Tray
			@FindBy(how = How.XPATH, using = "//button[@class= 'headerbar-tray-option-button headerbar-tray-option--labeled']")
			private WebElement HeaderBarTray;

			public void clickOnHeaderBarTrayButton() {
				baseClass.waitForThreadToLoad(2);
				element = driver.findElement(By.xpath("//button[@class= 'headerbar-tray-option-button headerbar-tray-option--labeled']"));
				baseClass.waitForElementToBeClickable(element);
				baseClass.scrollIntoPageElementandClick(element);
			}
	
			// LogOut Button
			@FindBy(how = How.XPATH, using = "//button[@class='commandbutton-button commandbutton-button--clickable']/div")
			private WebElement LogOutButton;

			public void clickOnLogOutButton() {
				element = driver.findElement(By.xpath("//button[@class='commandbutton-button commandbutton-button--clickable']/div"));
				baseClass.waitForElementToBeClickable(element);
				baseClass.scrollIntoPageElementandClick(element);
			}
			
			//Old Password Input box
			@FindBy(how=How.XPATH, using="//input[@name='oldPassword']")
		    private WebElement OldPasswordInputbox;
		    
		    public void SendtextOldPasswordInputboxInputbox(String Oldpassword){
		    element = driver.findElement(By.xpath("//input[@name='oldPassword']"));		    	
		    baseClass.waitForElementToBePresent(element);
		    baseClass.enterDataIntoEditbox(element, Oldpassword);
		    }
		    
		    //New Password Input box
			@FindBy(how=How.XPATH, using="//input[@name='password']")
		    private WebElement NewPasswordInputbox;
		    
		    public void SendtextNewPasswordInputbox(String Newpassword){
		    	element = driver.findElement(By.xpath("//input[@name='password']"));
			    baseClass.waitForElementToBePresent(element);
			    baseClass.enterDataIntoEditbox(element, Newpassword);
		    }
		    
		    //Confirm New Password Input box
			@FindBy(how=How.XPATH, using="//input[@name='passwordConfirm']")
		    private WebElement ConfirmNewPasswordInputbox;
		    
		    public void SendtextconfirmNewPasswordInputboxInputbox(String ConfirmNewpassword){
		    	
		    	element = driver.findElement(By.xpath("//input[@name='passwordConfirm']"));	
			    baseClass.waitForElementToBePresent(element);
			    baseClass.enterDataIntoEditbox(element, ConfirmNewpassword);			    
		    }	
		    
		    //Next Button
			@FindBy(how=How.XPATH, using="//button[@class='wizard-footer-button wizard-footer-button--clickable']")
		    private WebElement NextButton;
		    
		    public void ClickNextButton(){
		    	element = driver.findElement(By.xpath("//button[@class='wizard-footer-button wizard-footer-button--clickable']"));
		       	baseClass.waitForElementToBeClickable(element);	
		    	baseClass.clickOnWebElement(element);
		    }
		    
		    //First Question Input box
		    @FindBy(how = How.XPATH, using = "//div[@class='popup popup--theme-error popup--position-top popup--focus popup--hover']")
			private WebElement SecurityQustionsErrorMessage;
			
			public String SecurityQustionspageHelp(){
				element = driver.findElement(By.xpath("//div[@class='popup popup--theme-error popup--position-top popup--focus popup--hover']"));
				baseClass.waitForElementToBePresent(element);  
				return baseClass.getTextFromWebElement(element);
			    }
			
			public String ExpectedmessageForgotPassword(){
				String expected_message="Unable to continue since one or more questions are found as duplicate. To continue, please provide unique value for each question.";
				return expected_message; 
				}
		    
			//Error Message For First Question
		    @FindBy(how=How.XPATH, using="//input[@name='securityQuestions.question0']")
		    private WebElement FirstQuestionInputbox;
		    
		    public void SendtextFirstQuestionInputbox(String FirstQuestion){
		    	element = driver.findElement(By.xpath("//input[@name='securityQuestions.question0']"));
		    	baseClass.waitForElementToBePresent(element);	
		    	baseClass.enterDataIntoEditbox(element, FirstQuestion);
		    }
		    
		    
		    //First Answer Input box
			@FindBy(how=How.XPATH, using="//input[@name='securityAnswers.answer0']")
		    private WebElement FirstAnswerInputbox;
		    
		    public void SendtextFirstAnswerInputbox(String FirstAnswer){
		    	element = driver.findElement(By.xpath("//input[@name='securityAnswers.answer0']"));
		    	baseClass.waitForElementToBePresent(element);	
		    	baseClass.enterDataIntoEditbox(element, FirstAnswer);
		    }
		    
		    //Second Question Input box
			@FindBy(how=How.XPATH, using="//input[@name='securityQuestions.question1']")
		    private WebElement SecondQuestionInputbox;
		    
		    public void SendtextSecondQuestionInputbox(String SecondQuestion){
		    	
		    	element = driver.findElement(By.xpath("//input[@name='securityQuestions.question1']"));
		    	baseClass.waitForElementToBePresent(element);	
		    	baseClass.enterDataIntoEditbox(element, SecondQuestion);		    	
		    }		    
		    
		    //Second Answer Input box
			@FindBy(how=How.XPATH, using="//input[@name='securityAnswers.answer1']")
		    private WebElement SecondAnswerInputbox;
		    
		    public void SendtextSecondAnswerInputbox(String SecondAnswer){
		    	element = driver.findElement(By.xpath("//input[@name='securityAnswers.answer1']"));
		    	baseClass.waitForElementToBePresent(element);	
		    	baseClass.enterDataIntoEditbox(element, SecondAnswer);		    	
		    }
		    
		    //Third Question Input box
			@FindBy(how=How.XPATH, using="//input[@name='securityQuestions.question2']")
		    private WebElement ThirdQuestionInputbox;
		    
		    public void SendtextThirdQuestionInputbox(String ThirdQuestion){
		    	element = driver.findElement(By.xpath("//input[@name='securityQuestions.question2']"));
		    	baseClass.waitForElementToBePresent(element);	
		    	baseClass.enterDataIntoEditbox(element, ThirdQuestion);		    	
		    }
		    
		    //Third Answer Input box
			@FindBy(how=How.XPATH, using="//input[@name='securityAnswers.answer2']")
		    private WebElement ThirdAnswerInputbox;
		    
		    public void SendtextThirdAnswerInputbox(String ThirdAnswer){
		    	
		    	element = driver.findElement(By.xpath("//input[@name='securityAnswers.answer2']"));	
		    	baseClass.waitForThreadToLoad(2);
		    	baseClass.scrollIntoViewPageElementandClick(element);
		    	baseClass.enterDataIntoEditbox(element, ThirdAnswer);
		    }
		    
		    //Save And Log In Button
			@FindBy(how=How.XPATH, using="//button[@class='commandbutton-button commandbutton-button--clickable']")
		    private WebElement SaveAndLogInButton;
		    
		    public void ClickSaveAndLogInButton(){
		    element = driver.findElement(By.xpath("//button[@class='commandbutton-button commandbutton-button--clickable']"));
		    baseClass.scrollUpToPageStart(driver);
		    baseClass.waitForElementToBeClickable(element);	
		    baseClass.scrollIntoPageElementandClick(element);
		    }
			
			//new user Login method
			public void giveSecurityDeatailsAfterLogin(String OldPassword, String NewPassword,
					String FirstQuestion, String FirstAnswer, String SecondQuestion,
					String SecondAnswer, String ThirdQuestion, String ThirdAnswer) {
				SendtextOldPasswordInputboxInputbox(OldPassword);
				SendtextNewPasswordInputbox(NewPassword);
				SendtextconfirmNewPasswordInputboxInputbox(NewPassword);
				ClickNextButton();
				SendtextFirstQuestionInputbox(FirstQuestion);
				SendtextFirstAnswerInputbox(FirstAnswer);
				SendtextSecondQuestionInputbox(SecondQuestion);
				SendtextSecondAnswerInputbox(SecondAnswer);
				SendtextThirdQuestionInputbox(ThirdQuestion);
				SendtextThirdAnswerInputbox(ThirdAnswer);
				ClickSaveAndLogInButton();
					}
			
			@FindBy(how = How.NAME, using = "username")
			private WebElement username;

			@FindBy(how = How.NAME, using = "password")
			private WebElement userPassword;

			 @FindBy(how = How.XPATH, using = "//button[contains(., 'Log in')]")
			 private WebElement loginBtn;
			
			//New User Login  
			 public void  loginWithNewCredentials(String username, String password) throws Exception{
				String cst = BaseClass.GetPropertyName("CST");

				element = driver.findElement(By.name("username"));
			 	baseClass.enterDataIntoEditbox(element, username);
				element = driver.findElement(By.name("password"));
				baseClass.enterDataIntoEditbox(element, password);
				element = driver.findElement(By.xpath("//*[contains(text(), 'Remember my username')]"));
				baseClass.selectTheCheckbox(element);
				if (cst.equalsIgnoreCase("AWS")) {
					element = driver.findElement(By.xpath("//button[contains(., 'Log in')]"));
				}else{
				  element = driver.findElement(By.xpath("(//button[contains(., 'Log in')])[2]"));
				}
				baseClass.waitForElementToBeClickable(element);
				baseClass.clickOnWebElement(element);
				baseClass.waitForThreadToLoad(2);
			 }

}
