package com.aa.automation;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SchedulePage {
	
	WebDriver driver;
	WebDriverWait wait;
	BaseClass baseClass;
	RolesPage rolesPage ;
	WebElement element;
	JavascriptExecutor js;	
	
	public SchedulePage(WebDriver driver, WebDriverWait wait) throws Exception {
		this.driver = driver;
		wait = new WebDriverWait(driver,60);
		this.wait = wait;
		baseClass = new BaseClass(driver,wait);
		js = (JavascriptExecutor) driver;
	}
	

	@FindBy(how = How.XPATH, using = "//*[text()='Schedule bot...']")
	private WebElement ScheduleBots;
	
	public void clickOnScheduleBots() {
		element = driver.findElement(By.xpath("//*[text()='Schedule bot...']"));		
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void clickOnRunBots() {
		element = driver.findElement(By.xpath("//*[text()='Run bot now...']"));		
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	
	
	public void expandMyTaskFolderFromTree() throws InterruptedException{
		baseClass.waitForThreadToLoad(5); 
		JavascriptExecutor Executor = ((JavascriptExecutor)driver);
		element = driver.findElement(By.xpath("//*[text()='My Tasks']"));   
		baseClass.scrollIntoViewPageElementandClick(element);
		String MyTask_folderIcon=driver.findElement(By.xpath("//div[@class='folderlist-item folderlist-item--indent-0 folderlist-item--active']//parent::button[@class='folderlist-opener folderlist-clickable']/span")).getAttribute("class");
		if(!MyTask_folderIcon.contains("down")){
			driver.findElement(By.xpath("//div[@class='folderlist-item folderlist-item--indent-0 folderlist-item--active']//parent::button[@class='folderlist-opener folderlist-clickable']/span")).click();	
		}
			element = driver.findElement(By.xpath("//*[text()='resources']"));   
			baseClass.scrollIntoViewPageElementandClick(element);			
			String MyTask_folderIcon1=driver.findElement(By.xpath("//div[@class='folderlist-item folderlist-item--indent-1 folderlist-item--active']//parent::button[@class='folderlist-opener folderlist-clickable']/span")).getAttribute("class");
		if(!MyTask_folderIcon1.contains("down")){
				driver.findElement(By.xpath("//div[@class='folderlist-item folderlist-item--indent-1 folderlist-item--active']//parent::button[@class='folderlist-opener folderlist-clickable']/span")).click();
			}
		} 
	
	
	public void accessSubFolderFromTree(String foldername)
	{
		element = driver.findElement(By.xpath("//*[@title='"+foldername+"']"));
		baseClass.waitForThreadToLoad(2);
		baseClass.scrollIntoViewPageElementandClick(element);
	}
		
	
	public void selectBotWithDesiredProductionVersion(String taskbot) throws InterruptedException {
		baseClass.waitForThreadToLoad(5); 
		element = driver.findElement(By.xpath("//*[contains(text(),'"+taskbot+"')]//preceding::div[4]"));
		baseClass.scrollIntoViewPageElementandClick(element);
		//baseClass.selectRadioButton(element);	
		
	}
	

	@FindBy(how = How.XPATH, using = "//button[contains(.,'Select >')]")
	private WebElement Select;
	
	public void ClickOnSelect() {
		element = driver.findElement(By.xpath("//button[contains(.,'→')]"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}
	
	@FindBy(how = How.XPATH, using = "//div[text()='DEVICES']//following::div[1]")
	private WebElement ScheduleandDevicesBody;
	
	public void ClickOnScheduleandDevicesBodyusingjs()
	{
		JavascriptExecutor Executor = ((JavascriptExecutor)driver);
		Executor.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
		element = driver.findElement(By.xpath("//div[text()='SCHEDULE + DEVICES']//following::div[1]"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}
	
	public void ClickOnScheduleandDevices()
	{
		JavascriptExecutor Executor = ((JavascriptExecutor)driver);
		Executor.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
		element = driver.findElement(By.xpath("//div[text()='DEVICES']//following::div[1]"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.scrollIntoPageElementandClick(element);
	}
	
	
	
	
	

	@FindBy(how = How.XPATH, using = "//input[@name='scheduleStartTime']")
	private WebElement Schedule_StartTime;
	
	public String EnterStartTime(int Minute) throws Exception {
		element = driver.findElement(By.xpath("//input[@name='scheduleStartTime']"));				
		Date dNow = new Date( ); // Instantiate a Date object
		Calendar cal = Calendar.getInstance();
		cal.setTime(dNow);
		cal.add(Calendar.MINUTE, Minute);
		dNow = cal.getTime();
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		String  df1=df.format(dNow.getTime());
		String test = baseClass.convertDateFormat((df1.toString()));
		
		baseClass.waitForElementToBePresent(element);
		baseClass.enterDataIntoEditbox(element, test);
		
		cal.add(Calendar.MINUTE, 15);
		dNow = cal.getTime();
		SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");
		String  df3=df2.format(dNow.getTime());	
		return df3;
		
	}
	
	public void selectDevice(String Username) throws InterruptedException, Exception{
		js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
		String deviceName= InetAddress.getLocalHost().getHostName();
		String Status_column= driver.findElement(By.xpath("//*[contains(text(),'"+Username+"')]//preceding::span[2]")).getText();
		if(Status_column.equalsIgnoreCase("Connected"))
		{
			//JOptionPane.showMessageDialog(null, "Hi");
			driver.findElement(By.xpath("//*[contains(text(),'"+Username+"')]//preceding::div[8]")).click();
		}
	}
	
	@FindBy(how = How.XPATH, using = "//div[@class='datatableinput-controls']//button[@class='commandbutton-button commandbutton-button--clickable']")
	private WebElement dataTableInput_Controls;
	
	public void ClickOnDataTableInputControls() {
		element = driver.findElement(By.xpath("//div[@class='datatableinput-controls']//button[@class='commandbutton-button commandbutton-button--clickable']"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	@FindBy(how = How.XPATH, using = "//*[text()='Schedule bot']//parent::button")
	private WebElement ScheduleNow;
	public void ClickOnScheduleNow() {
		js.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
		element = driver.findElement(By.xpath("//*[text()='Schedule bot']//parent::button"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	public void ClickOnRunNow() {
		js.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
		element = driver.findElement(By.xpath("//*[text()='Run now']//parent::button"));
		baseClass.waitForElementToBeClickable(element);
		baseClass.clickOnWebElement(element);
	}
	
	
	
	
	public int IncrementSeconds(int seconds){
		Calendar c=Calendar.getInstance();
		c.setTime(new Date()); /* whatever*/
		int totalseconds=60;
		int currentSeconds=c.get(Calendar.SECOND);
		int remainingSeconds=totalseconds-currentSeconds;
		int waitseconds=remainingSeconds+seconds;
		return waitseconds;
	}

	public void waitForBotFires(int sec) throws InterruptedException{
		try{
			Thread.sleep(IncrementSeconds(sec)*1000);
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	public void createOneTimeScheduleToExecuteTask(String botName,String foldername, int seconds) throws Exception{
		clickOnScheduleBots();
		expandMyTaskFolderFromTree();
		accessSubFolderFromTree(foldername);
		selectBotWithDesiredProductionVersion(botName);
		ClickOnSelect();
		ClickOnScheduleandDevicesBodyusingjs();
		baseClass.waitForThreadToLoad(3);
		EnterStartTime(2);
		//selectDevice();
		ClickOnDataTableInputControls();
		ClickOnScheduleNow();
		waitForBotFires(seconds);
	}
	
	public void createAndExecuteTask(String botName,String foldername,String Username) throws Exception{
		clickOnRunBots();
		expandMyTaskFolderFromTree();
		accessSubFolderFromTree(foldername);
		selectBotWithDesiredProductionVersion(botName);
		ClickOnSelect();
		ClickOnScheduleandDevices();
		baseClass.waitForThreadToLoad(2);
		//EnterStartTime(2);
		selectDevice(Username);
		ClickOnDataTableInputControls();
		ClickOnRunNow();
		baseClass.waitForThreadToLoad(15);
		//waitForBotFires(seconds);
	}


}
