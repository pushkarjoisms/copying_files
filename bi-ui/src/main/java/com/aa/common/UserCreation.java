package com.aa.common;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Creates Control Room users; superadmin, botinsight admin and botrunner
 */
public class UserCreation {
  final static Logger log = Logger.getLogger(UserCreation.class);
  final String WONDERLAND = "W0nderland";
  String biURI = Utility.getInstance().getHost() + EndPoints.USERMANAGEMENT.getEndpoint() + "/users";
  String authURI = Utility.getInstance().getHost() + EndPoints.AUTHENTICATION.getEndpoint();
  private Utility utility = Utility.getInstance();
  private volatile static UserCreation instance;
  private String token;
  private List<String> licenses = new ArrayList<>();

  public String getAuthToken(String username, String password) {
    //String uri = utility.getHost() + EndPoints.AUTHENTICATION.getEndpoint();
    String authFilePath = utility.getFileLocation("json/users/authentication.json");
    Authentication auth = utility.getMapper(Authentication.class, authFilePath);
    auth.setUsername(username);
    auth.setPassword(password);

    String payload = utility.jsonObjectToString(auth);
    log.info("Authentication uri: " + this.biURI);
    log.info("payload: " + payload);
    Response response = HTTPUtility.post(this.authURI, payload);
    token = response.path("token");
    log.info("getAuthToken: " + token);
    return token;
  }

  public void changePassword(Botinsights botInsights) {
    this.getAuthToken(botInsights.getUsername(), botInsights.getPassword());

    String changePasswordFilePath = utility.getFileLocation("json/users/changepassword.json");
    ChangePassword changePassword = utility.getMapper(ChangePassword.class, changePasswordFilePath);
    changePassword.setPassword(WONDERLAND);
    String payload = utility.jsonObjectToString(changePassword);
    String uri = this.biURI + "/self/changePassword";
    log.info(botInsights.getUsername() + " change password uri: " + uri);
    log.info("payload: " + payload);
    Response response = HTTPUtility.post(uri, payload, token);
    assert (response.statusCode() == 200): "Failed in changing password for BotInsights admin user";
  }

  public void createNormaluser(Botinsights botInsights, String user, String license) {
    log.info("Creating: " + user);
    log.info(user + " uri: " + this.biURI);
    botInsights.setUsername(user);
    licenses.clear();
    licenses.add(license);
    botInsights.setLicenseFeatures(licenses);
    String payload = utility.jsonObjectToString(botInsights);
    log.info("payload: " + payload);
    Response response = HTTPUtility.post(this.biURI, payload, token);
    assert (response.statusCode() == 201): "Failed in creating BotInsights " + user;

    // change password for botInsight admin user
    this.changePassword(botInsights);
  }

  /**
   * Creates Control Room admin user
   * @param role - user role
   */
  public void createUser(int role) throws AutomationException {
    String ADMIN = "superadmin";
    String PASSWORD = "W0nderland";
    String EXPRESS = "Express";

    ControlRoomUser controlRoomUser = new ControlRoomUser();
    log.info("Creating control-room user");

    if (role != Role.CRADMIN.getRole()) {
      throw new AutomationException("Failed in creating Control-Room admin -- incorrect role id");
    }

    ControlRoomUtility crutil = ControlRoomUtility.getInstance();
    if (! Utility.getInstance().isUserCreated()) {
      try {
        log.info("Copying License data from Artifacts: ");
        crutil.copyLicenseFilesFromArtifacts();

        log.info("Control-room will be configured and users will be created");
        controlRoomUser.performFirstLoadActions("DATABASE", ADMIN, PASSWORD, EXPRESS);
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else {
      throw new AutomationException("CR/BI users has already been created.. skipping user creation");
    }
  }

  /**
   * Create user other than control room admin user depending on the role and license
   * @param user - user name
   * @param role - user role
   * @param license - license is either develoment or runtime
   */
  public void createUser(String user, int role, String license) {
   //String uri = utility.getHost() + EndPoints.USERMANAGEMENT.getEndpoint();
   String biFilePath = utility.getFileLocation("json/users/noncruser.json");
   Botinsights botInsights = utility.getMapper(Botinsights.class, biFilePath);
   token = this.getAuthToken("superadmin", WONDERLAND);
   String payload;

   if (license.equalsIgnoreCase("DEVELOPMENT")) {
      if (role == Role.BotInsight.getRole()) {
        log.info("Creating: " + user);
        List<RoleID> roleList = new ArrayList<>();
        RoleID biRole = new RoleID();
        biRole.setId(Role.BotInsight.getRole());
        RoleID basicRole = new RoleID();
        basicRole.setId(Role.Basic.getRole());
        roleList.add(biRole); // add botInsight admin role to Botinsight admin user
        roleList.add(basicRole); // add basic role to Botinsight admin user
        botInsights.setRoles(roleList);

        // create botInsight user
        botInsights.setUsername(user);
        payload = utility.jsonObjectToString(botInsights);
        log.info(user + " uri: " + this.biURI);
        log.info("payload: " + payload);
        Response response = HTTPUtility.post(this.biURI, payload, token);
        assert (response.statusCode() == 201): "Failed in creating BotInsights admin user";

        // change password for botInsight admin user
        this.changePassword(botInsights);

      } else { // bot creator user
        this.createNormaluser(botInsights, user, "DEVELOPMENT");
      }
    } else { // bot runner user
      System.out.println("Creating: " + user);
      this.createNormaluser(botInsights, user, "RUNTIME");
    }
  }

  public static void main(String[] args) {
    Role role = Role.CRADMIN;
    System.out.println(role.getRole());
  }
}
