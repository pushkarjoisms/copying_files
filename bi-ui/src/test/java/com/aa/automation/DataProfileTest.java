package com.aa.automation;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.File;
import java.util.Scanner;

public class DataProfileTest{

	WebDriver driver;
	WebDriverWait wait;

	DataProfilePage dataProfilePage;
	DashboardPage dashboardPage;
	DashboardTest dashboardTest;
	BaseClass baseClass;
	String projectRepo = System.getProperty("user.dir")+"\\src\\test\\resources\\Tasks";

	public DataProfileTest(WebDriver driver, WebDriverWait wait) throws Exception
	{
		this.driver= driver;
		wait = new WebDriverWait(driver,60);
		this.wait = wait;
		dataProfilePage = new DataProfilePage(driver,wait);
		dashboardPage = new DashboardPage(driver, wait);
		dashboardTest = new DashboardTest(driver, wait);
		baseClass = new BaseClass(driver,wait);
		
	}	

	public void check_InformationMessage_ToSearchTaskToLoadDataProfile() throws Exception
	{
		System.out.println("Executing Method No: 20");
		
		//Arrange
		dataProfilePage.clickOnAnalyze();
		baseClass.refreshThePage();
		dataProfilePage.clickOnConfigure();	

		//Act
		dataProfilePage.selectDataProfile();
		
		//Assert
		String result = dataProfilePage.getInfoMessageAskingToSearchTask();
		Assert.assertTrue(result.equals("Please search and choose a task to start your configuration!"));
	}

	public void editAndGenerateNewDashboardLinks_OnDataProfile_IsAvailableTrue() throws Exception
	{
		System.out.println("Executing Method No: 19");
		
		//Arrange	
		dataProfilePage.searchAndSelectTask(BaseClass.GetPropertyName("Task"));		

		//Act
		dataProfilePage.selectDataProfile();
		String result = dataProfilePage.getEditAndGenerateNewDashbordLinks();

		//Assert
		//Assert.assertTrue(result.contains("rgba(0, 174, 223, 1)"));
		//Assert.assertTrue(result.contains("rgba(176, 176, 176, 1)"));
		Assert.assertTrue(result.contains("rgba(102, 102, 102, 1)"));
		
	}

	public void clickEdit_OnDataProfile_MakeDataProfileEditable() throws Exception
	{
		System.out.println("Executing Method No: 16");
		
		//Arrange
		dataProfilePage.searchAndSelectTask(BaseClass.GetPropertyName("Task"));		

		//Act
		dataProfilePage.selectDataProfile();
		dataProfilePage.clickOnEditLink();

		//Assert
		boolean result = dataProfilePage.getClassNameOfEditableFields();
		dataProfilePage.clickOnCancelButton();
		Assert.assertTrue(result);		
	}	

	public void clickSave_ForUpdatedDataProfileTable_RetainsChanges() throws Exception
	{
		System.out.println("Executing Method No: 14");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("17022017.atmx","AAPlayer.exe");	
		dataProfilePage.searchAndSelectTask(BaseClass.GetPropertyName("Task"));
		dataProfilePage.selectDataProfile();
		baseClass.waitForThreadToLoad(2);
		dataProfilePage.clickOnEditLink();

		//Act
		dataProfilePage.modifyDataTableFields(3);
		dataProfilePage.clickOnSaveAndGenerateNewDashboard();
		baseClass.waitForThreadToLoad(2);
		dataProfilePage.selectDataProfile();

		//Assert
		String result = dataProfilePage.getDataProfileRowValues();
		boolean inclusionCheckBoxState = dataProfilePage.getInclusionCheckboxState();
		Assert.assertTrue(result.contains("Numeric"));
		Assert.assertTrue(result.contains("String"));
		Assert.assertFalse(inclusionCheckBoxState);
	}

	public void clickGenerateNewDashboardLink_LoadsDashboard_WithModifiedDataProfile() throws Exception
	{
		System.out.println("Executing Method No: 45");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("TelecomOrderEntry.atmx", "AAPlayer.exe");
		baseClass.logTextToFile(projectRepo+"\\uploadtest.txt", "taskToSelect=TelecomOrderEntry.atmx");
		baseClass.logTextToFile(projectRepo+"\\selectParentFolder.txt", "selectParentFolder=My Tasks");
		baseClass.logTextToFile(projectRepo+"\\selectChildFolder.txt", "selectChildFolder=resources");
		baseClass.logTextToFile(projectRepo+"\\selectSubChildFolder.txt", "selectSubChildFolder=Tasks");
		baseClass.executeTaskFromAAEClient("UploadTask_VCS_OFF.atmx", "AAPlayer.exe");
		baseClass.waitForThreadToLoad(2);
		dataProfilePage.clickOnConfigure();
		dataProfilePage.selectDataProfile();
		dataProfilePage.searchAndSelectTask("TelecomOrderEntry");
		dataProfilePage.clickOnEditLink();

		//Act
		dataProfilePage.modifyDataTableFields(5);
		int row[] = {6, 11};
		String dataType[] ={"Country Code","State Code"}; 
		for(int i=0; i<row.length; i++) {
			dataProfilePage.setCountryAndStateCode(row[i] ,dataType[i]);
		}
		dataProfilePage.clickOnSaveAndGenerateNewDashboard();
		baseClass.waitForThreadToLoad(10);
		//dataProfilePage.clickGenerateNewDashbord();
		baseClass.switchToChildFrame("dashboardContent");

		//Assert
		Boolean result = dashboardPage.dashboardWidgetsInIframe();
		baseClass.switchToParentFrame();
		Assert.assertFalse(result);
	}

	public void clickAnalyzeButtonFromAAEClientEditor_ForUpdatedDataProfileTable_CleansUpChangesBackToOriginal() throws Exception
	{
		System.out.println("Executing Method No: 32");
		
		//Arrange
		dataProfilePage.selectDataProfile();
		dataProfilePage.searchAndSelectTask(BaseClass.GetPropertyName("Task"));
		dataProfilePage.clickOnEditLink();

		//Act
		dataProfilePage.modifyDataType();
		dataProfilePage.clickOnSaveAndGenerateNewDashboard();
		baseClass.waitForThreadToLoad(5);
		dataProfilePage.selectDataProfile();
		baseClass.executeTaskFromAAEClient("17022017.atmx", "AAPlayer.exe");
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(5);
		String result = dataProfilePage.getDataProfileTable();
		String referenceString = "Preview data"+
				"\nTask Name: 17022017"+
				"\nVariable Name Display Name Datatype Inclusion Minimum Maximum Average Sum Distinct Count"+
				"\nA1 A1 Numeric"+
				"\n2,351.00"+
				"\n2,355.00"+
				"\n2,353.33"+
				"\n14,120.00"+
				"\n5"+
				"\nA2 A2 String"+
				"\nRank"+
				"\n5"+
				"\nA3 A3 Numeric"+
				"\n20,171.00"+
				"\n20,175.00"+
				"\n20,173.33"+
				"\n121,040.00"+
				"\n5"+
				"\nA4 A4 String"+
				"\nRank"+
				"\n5";

		//Assert
		//System.out.println(result);
		Assert.assertEquals(result, referenceString);
		
	}

	public void clickAnalyzeButton_ForOnlyStringVariableTask_VerifyDashboardIsCreated() throws Exception
	{
		System.out.println("Executing Method No: 33");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("OnlyStringDataType.atmx", "AAPlayer.exe");
		dataProfilePage.selectDataProfile();		
		dataProfilePage.searchAndSelectTask("OnlyStringDataType");
		dashboardPage.selectDashboard();

		//Act
		String result = "";
		
		/*baseClass.executeTaskFromAAEClient("OnlyStringDataType.atmx", "AATaskEditor.exe");		
		String browser = BaseClass.GetPropertyName("Browser");		
		if (browser.equalsIgnoreCase("Chrome")) {
			baseClass.executeTaskFromAAEClient("GetDashboardText_Chrome.atmx", "AAPlayer.exe");
			result = dashboardPage.getDashbaordFrameText();			
			//result = baseClass.getClipboardValue();
		} else if (browser.equalsIgnoreCase("IE")) {
			baseClass.executeTaskFromAAEClient("GetDashboardText_IE.atmx","AAPlayer.exe");
			result = dashboardPage.dashboardTitle();
		}*/			
		
		baseClass.switchToChildFrame("dashboardContent");
		result = dashboardPage.dashboardTitle();
		baseClass.switchToParentFrame();
		
		//Assert
		Assert.assertEquals(result, "OnlyStringDataType");
	}

	public void clickAnalyzeButton_ForOnlyNumericVariableTask_VerifyDashboardCreated() throws Exception
	{
		System.out.println("Executing Method No: 34");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("OnlyNumericDataType.atmx", "AAPlayer.exe");
		dataProfilePage.selectDataProfile();
		dataProfilePage.searchAndSelectTask("OnlyNumericDataType");

		//Act
		//baseClass.executeTaskFromAAEClient("OnlyNumericDataType.atmx", "AATaskEditor.exe");
		//baseClass.executeTaskFromAAEClient("GetDashboardText.atmx", "AAPlayer.exe");

		//Assert
		//String result = baseClass.getClipboardValue();
		String result = dataProfilePage.getDataProfileText();
		Assert.assertNotEquals(result,"Dashboard couldn't be created for OnlyStringDataType!");
		//String originalWindow = baseClass.switchToChildWindow();
		//baseClass.switchToWindow(originalWindow);
	}

	public void listItems_InDataTypeDropDown_IsPresent() throws Exception
	{
		System.out.println("Executing Method No: 17");
		
		//Arrange	
		dataProfilePage.searchAndSelectTask(BaseClass.GetPropertyName("Task"));		

		//Act
		dataProfilePage.selectDataProfile();
		dataProfilePage.clickOnEditLink();

		//Assert
		boolean result = dataProfilePage.getDataTypeListItems();
		Assert.assertTrue(result);
	}

	public void fields_InDataProfileTable_IsNotEditable() throws Exception
	{
		System.out.println("Executing Method No: 15");
		
		//Arrange
		dataProfilePage.searchAndSelectTask(BaseClass.GetPropertyName("Task"));
		dataProfilePage.selectDataProfile();

		//Act
		dataProfilePage.clickOnEditLink();
		dataProfilePage.modifyDataTableFields(3);
		dataProfilePage.clickOnCancelButton();

		//Assert
		boolean result = dataProfilePage.getClassNameOfEditableFields();
		Assert.assertFalse(result);
	}

	public void executedTask_GenerateDataProfileTable_WithCorrectValues() throws Exception
	{
		System.out.println("Executing Method No: 12");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("DataProfile_ValueVerification.atmx", "AAPlayer.exe");		

		//Act
		dataProfilePage.searchAndSelectTask("DataProfile_ValueVerification");
		dataProfilePage.selectDataProfile();
		baseClass.waitForThreadToLoad(2);
		String result = dataProfilePage.getDataProfileRowValues();

		//Assert
		String verification = "Variable Name Display Name Datatype Inclusion Minimum Maximum Average Sum Distinct Countnumericvariable Numericvariable Numeric"
				+ "\n3.00"
				+ "\n5,489,222.00"
				+ "\n1,622,057.00"
				+ "\n6,488,228.00"
				+ "\n3Stringvariable Stringvariable String"
				+ "\nRank"
				+ "\n3Timestampvariable Timestampvariable Timestamp"
				+ "\n1970-01-01 00:00"
				+ "\n1970-01-01 00:00"
				+ "\n3";
		//System.out.println(result);
		Assert.assertTrue(StringUtils.equalsIgnoreCase(result, verification));
	}

	public void dataProfileTable_ShowsCorrectValues_AfterTraversingFromAnalyzeToDataProfile() throws Exception
	{
		System.out.println("Executing Method No: 43");
		
		//Arrange
		//dataProfilePage.clickOnAnalyze();
		dataProfilePage.clickOnConfigure();
		dataProfilePage.selectDataProfile();

		//Act				
		dataProfilePage.searchAndSelectTask("DataProfile_ValueVerification");		

		//Assert
		String result = dataProfilePage.getDataProfileRowValues();
		String verification = "Variable Name Display Name Datatype Inclusion Minimum Maximum Average Sum Distinct Countnumericvariable Numericvariable Numeric"
				+ "\n3.00"
				+ "\n5,489,222.00"
				+ "\n1,622,057.00"
				+ "\n6,488,228.00"
				+ "\n3Stringvariable Stringvariable String"				
				+ "\nRank"
				+ "\n3Timestampvariable Timestampvariable Timestamp"
				+ "\n1970-01-01 00:00"
				+ "\n1970-01-01 00:00"
				+ "\n3";
		
		System.out.println("Diff---dataProfileTable_ShowsCorrectValues_AfterTraversingFromAnalyzeToDataProfile"+StringUtils.difference(result, verification));
		//System.out.println(result);
		Assert.assertTrue(StringUtils.equalsIgnoreCase(result, verification));
	}

	public void dataProfile_PreviewDataButton_IsPresent() throws Exception
	{
		System.out.println("Executing Method No: 18");
		
		//Arrange		
		baseClass.executeTaskFromAAEClient("TelecomOrderEntry.atmx", "AAPlayer.exe");
		dataProfilePage.searchAndSelectTask("TelecomOrderEntry");		

		//Act
		dataProfilePage.selectDataProfile();
		String result = dataProfilePage.isPreviewDataButtonPresent();

		//Assert
		Assert.assertTrue(result.contains("rgba(255, 255, 255, 1)"));
		Assert.assertTrue(result.contains("16px"));
	}

	//Preview Screen Verification
	public void clickPreviewData_ForExistingDataProfile_DisplayCorrectValuesInNewlyOpenedTab() throws Exception
	{
		System.out.println("Executing Method No: 11");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("TelecomOrderEntry.atmx", "AAPlayer.exe");
		baseClass.waitForThreadToLoad(2);
		dataProfilePage.searchAndSelectTask("TelecomOrderEntry");
		dataProfilePage.selectDataProfile();
		baseClass.waitForThreadToLoad(2);

		//Act
		dataProfilePage.clickOnPreviewData();
		@SuppressWarnings("resource")
		String refrenceTableData = new Scanner(new File(System.getProperty("user.dir")+ "\\src\\test\\resources\\TelecomOrderEntry.txt")).useDelimiter("\\Z").next();
		String originalWindow = baseClass.switchToChildWindow();
		baseClass.waitForThreadToLoad(5);
		String informationMessage = dataProfilePage.getRowCountMessage();
		String tableData = dataProfilePage.getPreviewDataTable();
		String logo = dataProfilePage.getBotInsightText();
		baseClass.closeBrowser();
		baseClass.switchToWindow(originalWindow);
		
		//Assert
		//LogTextToFile l = new LogTextToFile();
		//l.test("D:\\WSDL\\tabledata.txt", tableData);
		//l.test("D:\\WSDL\\refrencedata.txt", refrenceTableData);
		Assert.assertTrue(informationMessage.contains("Showing sample 1000 rows from the data processed by the automation task"));
		Assert.assertTrue(logo.equals("Bot Insight"));
		//System.out.println("Diff---clickPreviewData_ForExistingDataProfile_DisplayCorrectValuesInNewlyOpenedTab"+StringUtils.difference(tableData, refrenceTableData));
		//Assert.assertTrue(StringUtils.equalsIgnoreCase(tableData, refrenceTableData));
		/*System.out.println(tableData);
		System.out.println("-----------------");
		System.out.println(refrenceTableData);*/
	}

	public void taskNameLable_OnExistingDataProfile_IsVisible() throws Exception 
	{
		System.out.println("Executing Method No: 3");
		
		//Arrange
		String task = BaseClass.GetPropertyName("Task");
		baseClass.copyTheFileFromSourceToDestination("17022017.atmx", "17022017.atmx");
		baseClass.waitForThreadToLoad(2);
		baseClass.executeTaskFromAAEClient("17022017.atmx","AAPlayer.exe");
		baseClass.waitForThreadToLoad(2);
		//baseClass.executeTaskFromAAEClient("UploadTask.atmx","AAPlayer.exe");	
		baseClass.logTextToFile(projectRepo+"\\uploadtest.txt", "taskToSelect=17022017.atmx");
		baseClass.logTextToFile(projectRepo+"\\selectParentFolder.txt", "selectParentFolder=My Tasks");
		baseClass.logTextToFile(projectRepo+"\\selectChildFolder.txt", "selectChildFolder=resources");
		baseClass.logTextToFile(projectRepo+"\\selectSubChildFolder.txt", "selectSubChildFolder=Tasks");
		baseClass.executeTaskFromAAEClient("UploadTask_VCS_OFF.atmx", "AAPlayer.exe");

		//Act
		dataProfilePage.clickOnConfigure();	
		baseClass.waitForThreadToLoad(2);
		dataProfilePage.searchAndSelectTask(task);
		dataProfilePage.selectDataProfile();

		//Assert
		baseClass.waitForThreadToLoad(2);
		String taskName = dataProfilePage.getDataProfileTable();
		Assert.assertTrue(taskName.contains(task));
	}

	public void taskNameLableForDepedencyTask_OnExistingDataProfile_IsVisible() throws Exception
	{
		System.out.println("Executing Method No: 6");
		
		//Arrange
		String parenttask = BaseClass.GetPropertyName("ParentTask");	
		baseClass.waitForThreadToLoad(2);
		baseClass.executeTaskFromAAEClient("ParentTask.atmx", "AAPlayer.exe");
		baseClass.logTextToFile(projectRepo+"\\uploadtest.txt", "taskToSelect=ParentTask.atmx");
		baseClass.logTextToFile(projectRepo+"\\selectParentFolder.txt", "selectParentFolder=My Tasks");
		baseClass.logTextToFile(projectRepo+"\\selectChildFolder.txt", "selectChildFolder=resources");
		baseClass.logTextToFile(projectRepo+"\\selectSubChildFolder.txt", "selectSubChildFolder=Tasks");
		baseClass.executeTaskFromAAEClient("UploadTask_VCS_OFF.atmx", "AAPlayer.exe");
		dataProfilePage.clickOnConfigure();

		//Act
		dataProfilePage.searchAndSelectTask(parenttask);
		dataProfilePage.selectDataProfile();
		baseClass.waitForThreadToLoad(2);

		//Assert
		String taskName = dataProfilePage.getParentChildTaskNames(parenttask);
		Assert.assertTrue(taskName.contains(parenttask));
		Assert.assertTrue(taskName.contains("ChildTask"));
	}
	
	//Execute new task
	public void verifyNewlyExecutedTask() throws Exception
	{
		System.out.println("Executing Method No: 9");		
		
		baseClass.copyTheFileFromSourceToDestination("AN7_1.atmx", "AN7_1.atmx");
		baseClass.waitForThreadToLoad(2);
		baseClass.executeTaskFromAAEClient("AN7_1.atmx", "AAPlayer.exe");			
		dataProfilePage.clickOnConfigure();
		dataProfilePage.selectDataProfile();
		
	}

	public void reExecutedTask_AfterRenamingAnalyticsVariable_ReflectsChangesOnDataProfile() throws Exception
	{
		System.out.println("Executing Method No: 10");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("EditSaveUpload.atmx", "AAPlayer.exe");
		baseClass.waitForThreadToLoad(2);
		baseClass.executeTaskFromAAEClient("AN7_1.atmx", "AAPlayer.exe");		
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(5);		

		//Act		
		dataProfilePage.selectDataProfile();
		baseClass.waitForThreadToLoad(2);

		//Assert
		dataProfilePage.searchAndSelectTask("AN7_1");
		String newVariableText = dataProfilePage.getInfoOnNewVariable();
		String renamedVariableText = dataProfilePage.getInfoOnRenamedVariable();
		Assert.assertEquals("New", newVariableText);
		Assert.assertEquals("Renamed", renamedVariableText);
	}

	public void executedTask_LogginNullValueForAnalyticVariable_IsNotSeenOnDataProfile() throws Exception
	{
		System.out.println("Executing Method No: 13");
		
		//Arrange
		dataProfilePage.clickOnConfigure();		
		baseClass.executeTaskFromAAEClient("Blank_Variables.atmx", "AAPlayer.exe");

		//Act
		dataProfilePage.searchAndSelectTask("Blank_Variables");
		dataProfilePage.selectDataProfile();
		baseClass.waitForThreadToLoad(2);
		String result = dataProfilePage.getNullValuesForVariables();

		//Assert
		String verification = null;
		Assert.assertNotEquals(result,verification);
	}

	public void executedTask_ReadFromTextWithHeaderCommand_LogsAllVariablesWithStringDataType() throws Exception
	{
		System.out.println("Executing Method No: 26");
		
		//Arrange
		dataProfilePage.clickOnConfigure();
		dataProfilePage.selectDataProfile();		

		//Act
		baseClass.executeTaskFromAAEClient("ReadFromText_with_Header.atmx", "AAPlayer.exe");
		dataProfilePage.searchAndSelectTask("ReadFromText_with_Header");		

		//Assert
		String verification = "Numeric";
		String result = dataProfilePage.getDataTypeForAllVariables();
		Assert.assertFalse(result.contains(verification));
	}

	public void executedTask_ExitsLoopAtCounter10_LogsAllVariablesWithCorrectData() throws Exception
	{
		System.out.println("Executing Method No: 27");
		
		//Arrange
		String task = "ExitLoop_at_Counter10";
		dataProfilePage.clickOnConfigure();				
		baseClass.executeTaskFromAAEClient("ExitLoop_at_Counter10.atmx", "AAPlayer.exe");

		//Act
		Boolean result1 = null;
		dataProfilePage.searchAndSelectTask(task);
		result1 = baseClass.waitForFrameToBeAvailableAndSwitchToIt("dashboardContent");
		baseClass.waitForThreadToLoad(2);
		baseClass.switchToParentFrame();
		dataProfilePage.selectDataProfile();
		String result = dataProfilePage.getDataProfileTable();		

		//Assert
		Assert.assertTrue(result.contains(task));
		Assert.assertTrue(result.contains("7"));
		Assert.assertTrue(result1);

	}

	public void saveDataProfile_bySettingAllVariableDataTypeAsString_GiveInformationMessage() throws Exception
	{
		System.out.println("Executing Method No: 28");
		
		//Arrange
		dataProfilePage.clickOnConfigure();		
		baseClass.executeTaskFromAAEClient("OnlyStringDataType.atmx", "AAPlayer.exe");

		//Act
		dataProfilePage.searchAndSelectTask("OnlyStringDataType");
		dataProfilePage.selectDataProfile();
		dataProfilePage.clickOnEditLink();
		dataProfilePage.selectDataType("String");
		dataProfilePage.clickOnSaveAndGenerateNewDashboard();

		//Assert
		Boolean result = dataProfilePage.atLeastOneNumericDataTypeMessage();
		Assert.assertTrue(result);
	}

	public void dataProfile_OnlyNumericDataType_ShowsMinMaxAvgSumValues() throws Exception
	{
		System.out.println("Executing Method No: 29");
		
		//Arrange
		dataProfilePage.clickOnConfigure();
		baseClass.executeTaskFromAAEClient("DataProfile_ValueVerification.atmx", "AAPlayer.exe");		

		//Act		
		dataProfilePage.searchAndSelectTask("DataProfile_ValueVerification");
		dataProfilePage.selectDataProfile();

		//Assert
		String result = dataProfilePage.getDataProfileRowValues();
		result =  result != null ? result.trim() : "";
		String refrenceString = "Variable Name Display Name Datatype Inclusion Minimum Maximum Average Sum Distinct Countnumericvariable Numericvariable Numeric"
				+ "\n3.00"
				+ "\n5,489,222.00"
				+ "\n1,622,057.00"
				+ "\n6,488,228.00"
				+ "\n3Stringvariable Stringvariable String"
				+ "\nRank"
				+ "\n3Timestampvariable Timestampvariable Timestamp"
				+ "\n1970-01-01 00:00"
				+ "\n1970-01-01 00:00"
				+ "\n3";
		//System.out.println("expected: "+ result);
		Assert.assertTrue(StringUtils.equalsIgnoreCase(result, refrenceString));
	}

	public void fontColor_OfStandardDashboard_IsOrange() throws Exception
	{
		System.out.println("Executing Method No: 30");
		
		//Arrange
		dataProfilePage.clickOnConfigure();		

		//Act
		dataProfilePage.searchAndSelectTask(BaseClass.GetPropertyName("Task"));
		dataProfilePage.selectDataProfile();

		//Assert
		String result = dataProfilePage.getFontColorOfDashboard();
		String refrenceString = "rgba(246, 166, 36, 1)";
		Assert.assertEquals(result,refrenceString);
	}

	public void fontColor_OfCustomDashboard_IsBlack() throws Exception
	{
		System.out.println("Executing Method No: 31");
		
		//Arrange
		dataProfilePage.clickOnConfigure();
		dataProfilePage.selectDataProfile();

		//Act
		dataProfilePage.searchAndSelectTask(BaseClass.GetPropertyName("Dashboard"));

		//Assert
		String result = dataProfilePage.getFontColorOfDashboard();
		String refrenceString = "rgba(51, 51, 51, 1)";
		Assert.assertEquals(result,refrenceString);
	}

	public void clickDefaultDashboardLink_FromSavedAsDashboard_LoadsDashboard() throws Exception
	{
		System.out.println("Executing Method No: 48");
		
		//Arrange
		String dashboard = BaseClass.GetPropertyName("Dashboard");
		dashboardTest.deleteSavedAsDashboard(dashboard);
		baseClass.executeTaskFromAAEClient("17022017.atmx", "AAPlayer.exe");
		dataProfilePage.searchAndSelectTask(BaseClass.GetPropertyName("Task"));

		//Act
		dashboardPage.clickSaveAsButton();
		dashboardPage.enterDashboardName(dashboard);
		dashboardPage.clickOkayLink();
		baseClass.waitForThreadToLoad(5);
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(10);
		dataProfilePage.selectDataProfile();       	   	
		dataProfilePage.clickOnDefaultDbLink(); 
		String ParentWindow = baseClass.switchToChildWindow();
		baseClass.waitForThreadToLoad(5);
		
		//Assert  
		String taskName = "";		
		taskName = dataProfilePage.getFrameTextFromDashboard(); 
		baseClass.closeBrowser();
		baseClass.switchToWindow(ParentWindow);
		Assert.assertEquals(BaseClass.GetPropertyName("Task"), taskName);
	}


	public void clickRankButton_ForParenChildTask_VerifyRankTableLoaded() throws Exception
	{
		System.out.println("Executing Method No: 49");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("ParentTask.atmx", "AAPlayer.exe");
		baseClass.waitForThreadToLoad(2);
		dashboardPage.searchAndSelectDashboard(BaseClass.GetPropertyName("ParentTask"));

		//Act
		dataProfilePage.selectDataProfile();
		dataProfilePage.clickOnRankButton();
		String tblText = dataProfilePage.getTextFromTable();
		String referString = "Rank Value Count"+
				"\n1 Samsung Phone Case 3"+
				"\n2 iPAD Case 3"+
				"\n3 iPhone 6 2"+
				"\n4 iPhone 6S 2"+
				"\n5 iPAD Air 32 GB 2";	   
		
		dataProfilePage.selectDropdownForRecordCount(1);
		String tblText1 = dataProfilePage.getTextFromTable();	    
		String referString1 = "Rank Value Count"+
				"\n1 iPAD Case 3"+
				"\n2 Samsung Phone Case 3"+
				"\n3 iPAD Special Docker 2"+
				"\n4 iPhone 6 2"+
				"\n5 iPhone 6S 2"+
				"\n6 iPhone Case 2"+
				"\n7 Samsung Battery Pack 2"+
				"\n8 Samsung Galaxy S6 2"+
				"\n9 Samsung Mini-TV 2"+
				"\n10 iPAD Air 32 GB 2";	    

		dataProfilePage.selectDropdownForCurrentSort(1);
		dataProfilePage.selectDropdownForRecordCount(0);
		String tblText2 = dataProfilePage.getTextFromTable();	    
		String referString2 = "Rank Value Count"+
				"\n1 iPAD Mini 1"+
				"\n2 Samsung Tablet 1"+
				"\n3 iPAD Air 32 GB 2"+
				"\n4 iPhone 6S 2"+
				"\n5 iPhone 6 2";	    

		dataProfilePage.selectDropdownForRecordCount(1);
		String tblText3 = dataProfilePage.getTextFromTable();	    
		String referString3 = "Rank Value Count"+
				"\n1 iPAD Mini 1"+
				"\n2 Samsung Tablet 1"+
				"\n3 Samsung Mini-TV 2"+
				"\n4 Samsung Galaxy S6 2"+
				"\n5 Samsung Battery Pack 2"+
				"\n6 iPhone Case 2"+
				"\n7 iPhone 6S 2"+
				"\n8 iPhone 6 2"+
				"\n9 iPAD Special Docker 2"+
				"\n10 iPAD Air 32 GB 2";
		
		//Assert		
		baseClass.waitForThreadToLoad(2);
		baseClass.refreshThePage();
		Assert.assertEquals(tblText,referString);
		Assert.assertEquals(tblText1,referString1);
		Assert.assertEquals(tblText2,referString2);	
		Assert.assertEquals(tblText3,referString3);
		
		//System.out.println("Diff---clickRankButton_ForParenChildTask_VerifyRankTableLoaded"+StringUtils.difference(tblText2, referString2));
		//Assert.assertTrue(StringUtils.equalsIgnoreCase(tblText2, referString2));
		
	}
	
	public void VarifyDataProfile_ForParentChildTask_IfChildTaskHasFails() throws Exception{
		
		System.out.println("Executing Method No: 57");
		
		//Arrange
		baseClass.executeTaskFromAAEClient("ParentChild_DataProfileCase.atmx", "AAPlayer.exe");
		baseClass.refreshThePage();
		baseClass.waitForThreadToLoad(2);
		dashboardPage.clickOnConfigure();
		dashboardPage.selectDashboard();
		dashboardPage.searchAndSelectDashboard("ParentChild_DataProfileCase");
		dataProfilePage.selectDataProfile();
		baseClass.waitForThreadToLoad(2);		
		String text = dataProfilePage.getDataProfileText();
		Assert.assertTrue(text.contains("ParentChild_DataProfileCase"));
		
	}


}
