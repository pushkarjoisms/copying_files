package com.aa.common;

import io.restassured.builder.MultiPartSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Properties;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertTrue;

public class ControlRoomUser {
	final static Logger log = Logger.getLogger(ControlRoomUser.class);
	Response res;
	private String firstNameRetrived;
	private String userNameRetrived;
	private Response response;
	private static RequestSpecification spec;
	private static String workSpacePath = System.getProperty("user.dir");
	private String host;
	private String licenseFilePath;
	private static String domain;
	private static String authenticationType = ""; // "DATABASE" or "ACTIVE_DIRECTORY"
	private String credentialVaultMode = "Express"; //"express" OR "manual"
	private Boolean isFirstTimeLoad = true;
	private Boolean isLicenseUpdated = false;
	private String token = null;

	public void performFirstLoadActions(String authenticationType, String userName, String password,
			String credentialVaultMode) throws Exception {

		String licenseFile = "licenses/" +
		   "AAECR_Purchase_All_True_150D_300T_50AttendedRunTime_IQBots_50_Analytics_BOTFARM_ON.license";
		host = Utility.getInstance().getHost();
		//licenseFilePath = ControlRoomUser.class.getClassLoader().getResource(licenseFile).getFile();

		String lfile = "AAECR_Purchase_All_True_150D_300T_50AttendedRunTime_IQBots_50_Analytics_BOTFARM_ON.license";
		licenseFilePath = System.getProperty("user.dir") + "\\src\\test\\resources\\licenses\\" + lfile;


		log.info("Control-Room first admin settings");
		setFirstAdminSettings();
		setCVPublicPrivateKeys(credentialVaultMode);
		log.info("Control-Room set public/private keys");
		setAuthenticationType(authenticationType);
		log.info("Control-Room set authenticationtype");
		createFirstAdmin(userName, password, authenticationType);
		log.info("Control-Room creating admin: " + userName + ":" + password);

		login(userName, password);
		log.info("Control-Room installing license");
		installLicenseIfExpired();
		logOut(token);
	}

	public String login(String userName, String password) throws Exception {
		String body = "{\"username\": \"%s\", \"password\": \"%s\"}";

		if (authenticationType.equalsIgnoreCase("ACTIVE_DIRECTORY")) {
			body = String.format(body, domain + "\\\\" + userName, password);
		} else {
			body = String.format(body, userName, password);
		}

		response = postRequest(host + "/v1/authentication", body, "");
		if(response.statusCode()!= 200) {
			if(isFirstTimeLoad && response.statusCode() == 400 && response.jsonPath().get("message")
				   .equals("Authentication Type not set")) {
				ControlRoomUser actions = new ControlRoomUser();
				actions.performFirstLoadActions(authenticationType, userName, password, credentialVaultMode);
				isFirstTimeLoad = false;
				response = postRequest(host + "/v1/authentication", body, "");
			}
		}
		token = response.path("token");
		if(!isLicenseUpdated) {
			Response res = getRequest(host + "/v1/license/status", token);
			if(res.statusCode() == 402 && res.path("message").equals("Product license expired.")) {
				File file = new File(licenseFilePath);
				res = postRequestForUploadFile(host + "/v1/license/install", file, "licensefile",
"application/octet-stream", token);
				//assertTrue(res.statusCode() == 200);
			}
			else {
				Response resAvail = getRequest(host + "/v1/license/availability", token);
				int AvailLicense = 0;
				String license = "{\"data\": " + resAvail.getBody().asString() + "}";
				JSONObject newObj = new JSONObject(license);
				JSONArray items = newObj.getJSONArray("data");
				for (int it = 0; it < items.length(); it++) {
					JSONObject contactItem = items.getJSONObject(it);
					if(contactItem.get("name").equals("ControlRoom")) {
						JSONArray item = contactItem.getJSONArray("featureAvailabilitys");
						for (int i = 0; i < item.length(); i++) {
							if(item.getJSONObject(i).getString("name").equals("Development")) {
								AvailLicense = item.getJSONObject(i).getInt("available");
								//System.out.println(AvailLicense);
							}
						}
					}
				}
				if(AvailLicense <= 5) {
					File file = new File(licenseFilePath);
					res = postRequestForUploadFile(host + "/v1/license/install", file, "licensefile",
					"application/octet-stream", token);
				}
				isLicenseUpdated = true;
			}
		}
		return token;
	}

	public void logOut(String authToken) {
		response = postRequest(host + "/v1/authentication/logout", "", authToken);
	}

	public Response getRequest(String uri, String token) {
		response =
			given().
				//spec(spec).
				header("X-Authorization", token).
				contentType("application/json").
				when().
				get(uri).
				then().extract().response();

		return response;
	}

	public Response postRequest(String uri, String reqBody, String token) {
		System.out.println(reqBody);
		response =
			given().
				//spec(spec).
				header("X-Authorization", token).
				header("X-Version", "v1").
				contentType("application/json").
				body(reqBody).
				when().
				post(uri).
				then().
				extract().
				response();

		return response;
	}

	public Response putRequest(String uri, String reqBody, String token) {
		response =
			given().
				//spec(spec).
				header("X-Authorization", token).
				contentType("application/json").
				body(reqBody).
				when().
				put(uri).
				then().extract().response();

		return response;
	}

	public Response postRequestForUploadFile(String uri, File file, String controlName, String contentType,
      String token) throws IOException {
		FileInputStream fileInputStream = new FileInputStream(file);
		byte fileContent[] = new byte[(int)file.length()];
		fileInputStream.read(fileContent);
		fileInputStream.close();

		response =
			(Response) given().
				//spec(spec).
				header("X-Authorization", token).
				header("X-Version", "v1").
					multiPart(new MultiPartSpecBuilder(fileContent).fileName(file.getName())
					.controlName(controlName)
					.mimeType(contentType)
					.build()).
					when().
					post(uri).
					then().extract().response();

		return response;
	}

	public static void createFolder(String path) {
		try {
			File dir = new File(path);
			dir.mkdir();
		} catch(Exception e) {}
	}

	private String getAuthTypePayLoad(String authType) throws IOException {
		String body = "";
		switch (authType.toUpperCase()) {
			case "DATABASE":
				body = "{\"authType\":\"%s\"}";
				body = String.format(body, authType);
				break;
			case "ACTIVE_DIRECTORY":
				FileReader reader=new FileReader(workSpacePath + "\\src\\test\\resources\\activeDirectory.properties");
			    Properties properties =new Properties();  
			    properties.load(reader);
			    
				body = "{\n" + 
						"  \"authType\": \"%s\",\n" + 
						"  \"adSettings\": {\n" + 
						"    \"url\": \"%s\",\n" + 
						"    \"principal\": \"%s\",\n" + 
						"    \"credentials\": \"%s\"\n" + 
						"  }\n" + 
						"}\n" + 
						"";
				body = String.format(body, authType, properties.getProperty("url"), properties.getProperty("principal"),
				    properties.getProperty("password"));
				break;
		default:
			System.out.println("UNDEFINED AUTHENTICATION TYPE - DATABASE or ACTIVE_DIRECTORY");
			break;
		}
		return body;
	}

	public void installLicenseIfExpired() throws IOException {
		res = getRequest(host + "/v1/license/status", token);
		if(res.statusCode() == 402 && res.path("message").equals("Product license expired.")) {
			File file = new File(licenseFilePath);
			res = postRequestForUploadFile(host + "/v1/license/install", file, "licensefile",
          "application/octet-stream", token);
			assertTrue(res.statusCode() == 200);
		}
		res = getRequest(host + "/v1/license/status", token);
		assertTrue(res.statusCode() == 200);
	}

	private void setFirstAdminSettings() {
		res = getRequest(host + "/v1/settings/firstAdminSettings", "");
		assertTrue(res.statusCode() == 200);
		
		//String repoPath = res.path("repositoryPath").toString().replaceAll("\\\\", "\\\\\\\\");
		String repoPath = "C:\\ProgramData\\AutomationAnywhere\\Server Files";
		this.createFolder(repoPath);
		repoPath = repoPath.replaceAll("\\\\", "\\\\\\\\");
		String body = "{\n" +
				"	\"repositoryPath\": \"" + repoPath + "\",\n" +
				//"	\"installationType\": \"custom\",\n" + 
				"	\"accessUrl\": \"" + host  + "\"\n" +
				"}";
		log.info("Running setFirstAdmingSettings: " + host + "/v1/settings/firstAdminSettings");
		log.info("payload : " + body);
		res = postRequest(host + "/v1/settings/firstAdminSettings", body, "");
		assertTrue(res.statusCode() == 200);
	}
	
	private void setCVPublicPrivateKeys(String cvMode) {
		
		try {
			res = postRequest(host + "/v1/credentialvault/keys", "", "");
			assertTrue(res.statusCode() == 200);
			String publicKey = res.jsonPath().get("publicKey");
			String privateKey = res.jsonPath().get("privateKey");
			
			res = putRequest(host + "/v1/credentialvault/keys", credentialVaultKeys(publicKey, privateKey,
			   	cvMode), "");
			assertTrue(res.statusCode() == 200);
		} catch (Exception e) {
			System.out.println("Master key is already stored");
			e.printStackTrace();
		}
	}
	
	private void setAuthenticationType(String authType) throws IOException {
		
		res = getRequest(host + "/v1/authentication/type", "");
		assertTrue(res.statusCode() == 200);
		String currentAuthType = res.jsonPath().get("authType");
		System.out.println(currentAuthType);
		if(currentAuthType.equals("NONE")) {
			res = postRequest(host + "/v1/authentication/type", getAuthTypePayLoad(authType), "");
			assertTrue(res.statusCode() == 200);
		}
	}
	
	private void createFirstAdmin(String username, String password, String authType) {
		
		switch (authType.toUpperCase()) {
		case "DATABASE":
			res = getRequest(host + "/v1/usermanagement/firstAdmin", "");
			assertTrue(res.statusCode() == 200);
			Boolean isCreated = res.jsonPath().get("created");
			if (!isCreated) {
				res = postRequest(host + "/v1/usermanagement/firstAdmin",firstAdminPayload(username,
				    password, authType), "");
				assertTrue(res.statusCode() == 201);
			}
			break;
		case "ACTIVE_DIRECTORY":
			res = getRequest(host + "/v1/usermanagement/activeDirectory", "null");
			assertTrue(res.statusCode() == 200);
			
			String responseOutput = res.jsonPath().getString("[0]");
			
			System.out.println("Method name createFirstAdmin - responseOutput:"+responseOutput);
			System.out.println("Method name createFirstAdmin - domain:"+domain);
			if(responseOutput.contains(domain)) {
				System.out.println("Method name createFirstAdmin --- Domain name and response output are same");
				res = postRequest(host + "/v1/usermanagement/firstAdmin",firstAdminPayload(username, password,
				    authType), "");
				assertTrue(res.statusCode() == 201);
			}
			break;
		default:
			System.out.println("UNDEFINED AUTHENTICATION TYPE - DATABASE or ACTIVE_DIRECTORY");
			break;
		}
	}
	
	private String firstAdminPayload(String username, String password, String authType) {
		
		String body = "";
		switch (authType.toUpperCase()) {
			case "DATABASE":
				body = "{\r\n" + 
				"	\"questionsAnswers\":" +
				"		[\r\n" +
				"			{\r\n" +
				"				\"question\":\"Who is Freddie Cougar?\",\r\n" +
				"				\"answer\":\"your monster\"\r\n" +
				"			},\r\n" +
				"			{ \r\n" +
				"				\"question\":\"Who is Roshan?\",\r\n" +
				"				\"answer\":\"he is a legend\"\r\n" +
				"			},\r\n" +
				"			{\r\n" +
				"				\"question\":\"Who is the IQbots\",\r\n" +
				"				\"answer\":\"he is a fine man\"\r\n" +
				"			}\r\n" +
				"		],\r\n" + 
				"	\"user\":\r\n" +
				"		{\r\n" +
				"			\"email\":\"p@p.com\",\r\n" +
				"			\"firstName\":\"admin\",\r\n" +
				"			\"lastName\":\"admin\",\r\n" +
				"			\"password\":\"%s\",\r\n" +
				"			\"username\":\"%s\"}\r\n" + 
				"		}";
				body = String.format(body, password, username);
				break;
			case "ACTIVE_DIRECTORY":
				String uname = username;
				
				System.out.println("Method name firstAdminPayload"+domain);
				res = getRequest(host + "/v1/usermanagement/firstAdmin/activeDirectory/" + domain + "/" + uname, "");
				
				firstNameRetrived = res.jsonPath().getString("firstName");
				userNameRetrived = res.jsonPath().getString("username");
				if(res.statusCode() == 200) {
					body =  "	{\r\n" + 
							"	\"user\":\r\n" +
							"		{\r\n" +
							"			\"email\":\"p@p.com\",\r\n" +
							"			\"firstName\":\""+firstNameRetrived+"\",\r\n" +
							"			\"lastName\":\"TestAdminLastName\",\r\n" +
							"			\"username\":\""+userNameRetrived+"\",\r\n" +
							"			\"description\":\"Test\",\r\n" +
							"			\"domain\":\""+domain+"\"}\r\n" +
							"	}";
							body = String.format(body);
							
					return body;
				}
				
				body = String.format(body);
				break;
			default:
				System.out.println("UNDEFINED AUTHENTICATION TYPE - DATABASE or ACTIVE_DIRECTORY");
				break;
		}
		return body;
	}
	
	
	private String credentialVaultKeys(String publicKey, String privateKey, String credVaultMode) {
		String body = "{\r\n" + 
				"	\"publicKey\":\"%s\",\r\n" + 
				"	\"privateKey\":\"%s\",\r\n" + 
				"	\"mode\":\"%s\"\r\n" + 
				"}";
		return String.format(body, publicKey, privateKey, credVaultMode);
	}
}
