package com.aa.zephyrintegration;

import java.net.URI;

import com.aa.automation.BaseClass;
import com.thed.zephyr.cloud.rest.ZFJCloudRestClient;
import com.thed.zephyr.cloud.rest.client.JwtGenerator;

public class JwtTokenGenerator {

	public String generateTokenToGetCycleId(String Projectid, String Versionid) throws Exception
	{
		
		
		String zephyrBaseUrl = BaseClass.GetPropertyName("zephyrBaseUrl");
		String accessKey = BaseClass.GetPropertyName("accessKey");
		String secretKey = BaseClass.GetPropertyName("secretKey");
		String userName= BaseClass.GetPropertyName("userName");
		ZFJCloudRestClient client = ZFJCloudRestClient.restBuilder(zephyrBaseUrl, accessKey, secretKey, userName).build();
		
		JwtGenerator jwtGenerator = client.getJwtGenerator();
		
		String createCycleUri = zephyrBaseUrl + "/public/rest/api/1.0/cycles/search?versionId="+Versionid+"&projectId="+Projectid;
		URI uri = new URI(createCycleUri);		
		int expirationInSec = 136000;
		String jwt = null;
		jwt = jwtGenerator.generateJWT("GET", uri, expirationInSec);
		return jwt;
	}

	public String generateTokenToGetIssueId(String Projectid,String Versionid, String cycleId, int offset, int size) throws Exception
	{
		String zephyrBaseUrl = BaseClass.GetPropertyName("zephyrBaseUrl");
		String accessKey = BaseClass.GetPropertyName("accessKey");
		String secretKey = BaseClass.GetPropertyName("secretKey");
		String userName= BaseClass.GetPropertyName("userName");
		ZFJCloudRestClient client = ZFJCloudRestClient.restBuilder(zephyrBaseUrl, accessKey, secretKey, userName).build();
		JwtGenerator jwtGenerator = client.getJwtGenerator();
//		for (int i=1; i<=3; i++)
//		{
		String createCycleUri = zephyrBaseUrl + "/public/rest/api/1.0/executions/search/cycle/"+cycleId+"?projectId="+Projectid+"&versionId="+Versionid+"&offset=" + offset +"&size=" + size;
		URI uri = new URI(createCycleUri);		
		int expirationInSec = 136000;
		String jwt = jwtGenerator.generateJWT("GET", uri, expirationInSec);
	//	}
		return jwt;
	}

	public String generateTokenToGetExecutionId(String Projectid,String Versionid, String cycleId, int offset, int size) throws Exception
	{
		try
		{
			String zephyrBaseUrl = BaseClass.GetPropertyName("zephyrBaseUrl");
			String accessKey = BaseClass.GetPropertyName("accessKey");
			String secretKey = BaseClass.GetPropertyName("secretKey");
			String userName= BaseClass.GetPropertyName("userName");
			ZFJCloudRestClient client = ZFJCloudRestClient.restBuilder(zephyrBaseUrl, accessKey, secretKey, userName).build();
			JwtGenerator jwtGenerator = client.getJwtGenerator();
			String createCycleUri = zephyrBaseUrl + "/public/rest/api/1.0/executions/search/cycle/"+cycleId+"?versionId="+Versionid+"&projectId="+Projectid+"&offset=" + offset +"&size=" + size;
			URI uri = new URI(createCycleUri);	

			int expirationInSec = 13600000;
			String jwt = jwtGenerator.generateJWT("GET", uri, expirationInSec);
			return jwt;
		}
		catch (Exception e)
		{

			System.out.println(e);
		}
		return "";
	}

	public String generateTokenToUpdateExecutionStatus(long issueId, String executionId, String Projectid) throws Exception
	{
		String zephyrBaseUrl = BaseClass.GetPropertyName("zephyrBaseUrl");
		String accessKey = BaseClass.GetPropertyName("accessKey");
		String secretKey = BaseClass.GetPropertyName("secretKey");
		String userName= BaseClass.GetPropertyName("userName");
		ZFJCloudRestClient client = ZFJCloudRestClient.restBuilder(zephyrBaseUrl, accessKey, secretKey, userName).build();
		JwtGenerator jwtGenerator = client.getJwtGenerator();
		String createCycleUri = zephyrBaseUrl + "/public/rest/api/1.0/execution/"+executionId+"?projectId="+Projectid+"&issueId="+issueId+"";

		URI uri = new URI(createCycleUri);		
		int expirationInSec = 360;
		String jwt = jwtGenerator.generateJWT("PUT", uri, expirationInSec);
		return jwt;
	}
}
