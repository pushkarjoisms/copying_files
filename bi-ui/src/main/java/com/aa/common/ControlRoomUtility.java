package com.aa.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Properties;

public class ControlRoomUtility {
  final static Logger log = Logger.getLogger(ControlRoomUtility.class);
  static File parentDir = new File(System.getProperty("user.dir")).getParentFile();
  static File sourceDir = new File(parentDir + "\\artifacts\\licenses\\");
  static File destDir = new File((System.getProperty("user.dir") + "\\src\\test\\resources\\licenses"));

  private volatile static ControlRoomUtility instance;

  public static ControlRoomUtility getInstance() {
    if (instance == null) {
      synchronized(ControlRoomUtility.class) {
        if (instance == null) {
          instance = new ControlRoomUtility();
        }
      }
    }
    return instance;
  }

  public void copyLicenseFilesFromArtifacts() {
    this.licenseCopy(sourceDir, destDir);
  }

  public void copyFile(File sourceDir, File destDir) throws IOException {
    InputStream in = null;
    OutputStream out = null;
    try {
      in = new FileInputStream(sourceDir);
      out = new FileOutputStream(destDir);
      byte[] buffer = new byte[1024];
      int length;
      while ((length = in.read(buffer)) > 0) {
        out.write(buffer, 0, length);
      }
    } catch(IOException e){
      log.info("Something went wrong in copying License files in bi-ui " + e.getMessage());
    }
    finally {
      in.close();
      out.close();
    }

  }

  public void licenseCopy(File sourceDir, File destDir) {
    try {
      log.info("This is the Artifacts dir.: " + sourceDir);
      log.info("This is the Destination dir. in bi-ui: " + destDir);

      String srcFiles[] = sourceDir.list();
      for (String file:srcFiles)
      {
        log.info("The license file to copy is "+file);
        File srcFile= new File(sourceDir,file);
        File destFile= new File(destDir,file);
        this.copyFile(srcFile, destFile);
      }
    }
    catch (IOException e) {
      log.info("Something went wrong in copying Licenses Dir. from artifacts in bi-ui"+e.getMessage());
    }
  }

  public static void main(String[] args) {
    ControlRoomUtility util = ControlRoomUtility.getInstance();
  }
}
