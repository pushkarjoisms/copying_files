package com.aa.common;

import java.util.List;

public class Botinsights {
  private List<RoleID> roles;
  private String email;
  private Boolean enableAutoLogin ;
  private String username;
  private String description;
  private String firstName;
  private String lastName;
  private Boolean disabled;
  private String password;
  private List<String> licenseFeatures;

  public List<RoleID> getRoles() {
    return roles;
  }

  public void setRoles(List<RoleID> roles) {
    this.roles = roles;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Boolean getEnableAutoLogin() {
    return enableAutoLogin;
  }

  public void setEnableAutoLogin(Boolean enableAutoLogin) {
    this.enableAutoLogin = enableAutoLogin;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Boolean getDisabled() {
    return disabled;
  }

  public void setDisabled(Boolean disabled) {
    this.disabled = disabled;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<String> getLicenseFeatures() {
    return licenseFeatures;
  }

  public void setLicenseFeatures(List<String> licenseFeatures) {
    this.licenseFeatures = licenseFeatures;
  }
}
